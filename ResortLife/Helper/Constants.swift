//
//  Constants.swift
//  EStudentApp
//
//  Created by Apple retina on 07/10/19.
//  Copyright © 2019 Apple retina. All rights reserved.
//

import Foundation
import UIKit

// Story board...
let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)

//Notifications

let kProfileNotify = Notification.Name("update_profile")
let CTG_ISONotify = "iosCountry_list"
let kGuestTblReload = Notification.Name("reloadGuestTbl")
let kQuestionsTblReload = Notification.Name("reloadQuestionTbl")
let kToggleSideMenu = Notification.Name("toggle_sideMenu")
let kMenuClicked = Notification.Name("menu_clicked")

// MARK:- Constants

let googleApiKey = "AIzaSyBAYwH-09-3KOVGsADJO7Usg4ib45y6C0k"
let kCurrency = "$"
let kUser_Profile = "login_user"
let kFIRToken = "fcm_token"
let kUserType = "b2b"
let placeHolder = UIImage.init(named: "place_holder")//splash_screen.png
let userPlaceHolder = UIImage.init(named: "default_user")
let hotelPlaceHolder = UIImage.init(named: "place_holder")

// MARK:- Global functions

func getDeviceToken() -> String {
    
    let token = UserDefaults.standard.object(forKey: kFIRToken)
    if let token_str = token as? String {
        return token_str
    }
    return ""
}

func getAppDelegate() -> AppDelegate {
    let app_delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    return app_delegate
}

func getWindow() -> UIWindow {
    let app_delegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    return app_delegate.window!
}

struct Constants {
    static let kUserDefaults: UserDefaults = UserDefaults.standard
    static var isLoaded:Bool = false
}

struct UserData{
    
    static var registrationDetails:[String:String] = [:]
    static var userId:String = ""
    static var auth_key:String = ""
}

struct AppColors {
    static let green = UIColor.hex(hex: "#416C46", alpha: 1.0)
}


