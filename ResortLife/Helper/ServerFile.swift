//
//  ServerFile.swift
//  BaseProject
//
//  Created by PTBLR-1206 on 13/01/20.
//  Copyright © 2020 PTBLR-1206. All rights reserved.
//

import Foundation

// ***** Test Server *****
// MARK:-
struct Server {
   //http://provabextranet.com/resort_life/mobile_webservices/mobile/index.php/agent_user/verify
    
    // variables...
    static let sandbox_url = "http://provabextranet.com/resort_life/mobile_webservices/mobile/index.php/"
    static let production_url = "http://provabextranet.com/resort_life/mobile_webservices/mobile/index.php/"
    
    static let image_url = "http://provabextranet.com"
    static let image_url2 = "http://provabextranet.com/resort_life/mobile_webservices/"


    // environment type
    enum Environment {
        case Sandbox
        case Production
    }
    
    // server type
    static func getServer(type: Environment) -> String {
        
        if type == .Sandbox {
            return sandbox_url
        } else {
            return production_url
        }
    }
    
}

//API Files

let Promo_Image_URL = "https://"

//MARK:- HOME
let kCountryList = "flight/country_list"
let kHomePageResponse = "general/mobile_top_destination_list"
let Get_AllPromo = "general/all_promo"

//MARK:- USER
let kUserLogin = "agent_user/verify"
let kRegister = "auth/register_on_light_box_mobile"
let kforgotPassword = "general/forgot_password_mobile"
let kChangePassword = "general/mobile_change_password"
let kUpdateProfile = "general/update_profile_mobile"
let kGetProfileDetails = "general/get_profile_details"
let kGetBalance = "general/agent_balance"

//MARK:- CMS
let kAboutUs = "general/about_us"
let kPrivacyPolicy = "general/cms_mobile/privacy"
let kTermsAndConditions = "general/cms_mobile/terms"

//MARK:- Hotel
let kHotelSearch = "general/pre_hotel_search_mobile"
let kHotelDetails = "hotel/mobile_hotel_details"
let kGetHotelRoomList = "hotel/get_hotel_room_list"
let kGetRoomPrice = "hotel/get_room_price"
let kHotelRoomBlock = "hotel/mobile_booking"
let kHotelPreBooking = "hotel/mobile_pre_booking_crs"

//MARK:- Bookings
let kHotelBookings = "hotel/booking_history"

//MARK:- Notifications
let kGetNofications = "general/notification_list"
