//
//  HandleNavigations.swift
//  EStudentApp
//
//  Created by Apple retina on 07/10/19.
//  Copyright © 2019 Apple retina. All rights reserved.
//

import Foundation
import UIKit

class HandleNavigationScreens{    
    
    class func showHomeScreen(){
   
        let homePageView = HomeContainerVC.instantiateFromStoryBoard() as! HomeContainerVC

        let navCntl = UINavigationController(rootViewController: homePageView)
        navCntl.isNavigationBarHidden = true
        appDelegate.window?.rootViewController = navCntl
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    class func showSplashScreen(){
        let loginCntl = MainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as! SplashScreenVC
        
        let navCntl = UINavigationController(rootViewController: loginCntl)
        navCntl.isNavigationBarHidden = true
        appDelegate.window?.rootViewController = navCntl
        appDelegate.window?.makeKeyAndVisible()
    }
    
    class func showLoginScreen(){
        let loginCntl = MainStoryboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        
        let navCntl = UINavigationController(rootViewController: loginCntl)
        navCntl.isNavigationBarHidden = true
        appDelegate.window?.rootViewController = navCntl
        appDelegate.window?.makeKeyAndVisible()
    }
    
    class func logOut() {

        let controller = MainStoryboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        let navCntl = UINavigationController(rootViewController: controller)
        navCntl.isNavigationBarHidden = true

        appDelegate.window?.rootViewController = navCntl
        appDelegate.window?.makeKeyAndVisible()
        
        UserViewModel.userData = nil
        
        let dictionary = UserDefaults.standard.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            UserDefaults.standard.removeObject(forKey: key)
        }
            
    }
 
}

