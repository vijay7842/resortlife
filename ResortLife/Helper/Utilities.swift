//
//  Utilities.swift
//  ElamantTravel
//
//  Created by Apple retina on 30/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation
import UIKit
import DGActivityIndicatorView

class DialougeUtils {
    
    static let activityView = UIActivityIndicatorView(style: .whiteLarge)
    
    static func removeActivityView(view : UIView) {
        //activityView.stopAnimating()
        view.isUserInteractionEnabled = true
        let window = UIApplication.shared.keyWindow!
        window.viewWithTag(111)?.removeFromSuperview()
    }
    
    static func addActivityView(view : UIView) {
        
        let window = UIApplication.shared.keyWindow!
        
        window.viewWithTag(111)?.removeFromSuperview()
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: window.frame.size.width , height: window.frame.size.height + 80 )
     //   loadingView.backgroundColor =  UIColor(displayP3Red: 0.99, green: 0.99, blue: 0.99, alpha: 0.5)
        loadingView.backgroundColor =  UIColor.white.withAlphaComponent(0.6)

        loadingView.clipsToBounds = true
        loadingView.tag = 111
        
        let holdingView: UIView = UIView()
        holdingView.frame = CGRect(x: 0, y: 0, width: 100 , height: 100 )
        holdingView.backgroundColor =  UIColor.clear
        holdingView.layer.cornerRadius = 10
        holdingView.clipsToBounds = true
        loadingView.addSubview(holdingView)
        
        
        let activityIndicator = DGActivityIndicatorView(type: .triplePulse, tintColor: AppColors.green, size: 80)
        activityIndicator?.frame = CGRect(x: loadingView.frame.size.width/2 , y: loadingView.frame.size.height / 2, width: 70, height: 70)
        activityIndicator?.center = view.center
        holdingView.center = view.center
        
        view.isUserInteractionEnabled = false
        activityIndicator?.startAnimating()
        loadingView.addSubview(activityIndicator!)
        window.addSubview(loadingView)
    }
    
}

class CommonUtilities {
    
    static func animateTheView(view:UIView, xValue:CGFloat){
        UIView.animate(withDuration: 0.3,
                       animations: {
                        view.transform = CGAffineTransform(scaleX: xValue, y: xValue)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.3) {
                            view.transform = CGAffineTransform.identity
                        }
        })
    }
    
    
    static func convertHtmlToString(htmlString:String) -> String{
        return htmlString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    static func convertHtmlStringToAttributedString(htmlString:String) -> NSAttributedString{
        
        var attString = NSAttributedString.init(string: "")
        
        do {
            let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            attString = try NSAttributedString(data: htmlString.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: options, documentAttributes: nil)
            return attString
        } catch {
            print(error)
        }
        
        return attString
        
    }
    
    class func validateEmail(with email: String) -> Bool {
        let stricterFilter = false
        let stricterFilterString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let laxString = ".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*"
        let emailRegex: String = stricterFilter ? stricterFilterString : laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return !emailTest.evaluate(with: email)
    }
    class func validateMobile(value: String) -> Bool {
        //  let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        // let PHONE_REGEX = "^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$"
        let PHONE_REGEX = "[0-9]{10}"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    class func validatePANCardNumber(_ PANNumber : String) -> Bool{
        let regularExpression = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
        let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
        return panCardValidation.evaluate(with: PANNumber)
    }
    
    class func convertObjectToJsonString(objects:AnyObject) -> String {
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: objects, options:JSONSerialization.WritingOptions.prettyPrinted)
            if let theJSONText = String(data: jsonData,
                                        encoding: .utf8){
                return theJSONText
            }
            return ""
        }
        catch  {
            print(error.localizedDescription)
            return ""
        }
        
    }
    class func convertObjectToData(objects:AnyObject) -> Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: objects, options: [])
            
            return jsonData as Data
        }
        catch  {
            print(error.localizedDescription)
            let jsonData = Data()
            return jsonData
        }
    }
    class func validateZipCode(postalCode:String)->Bool{
        // let postalcodeRegex = "^[0-9]{5}(-[0-9]{4})?$"  // For US
        let postalcodeRegex = "^[0-9]{6}$" // For India
        
        let pinPredicate = NSPredicate(format: "SELF MATCHES %@", postalcodeRegex)
        let bool = pinPredicate.evaluate(with: postalCode) as Bool
        return !bool
    }
    
    class func renderResizedImage (prevImage:UIImage,newWidth: CGFloat) -> UIImage {
        let scale = newWidth / prevImage.size.width
        let newHeight = prevImage.size.height * scale
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        let renderer = UIGraphicsImageRenderer(size: newSize)
        
        let image = renderer.image { (context) in
            prevImage.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        }
        return image
    }
    
    class func showAlerView(_ viewcontroller:UIViewController, alertText:String){
        
        let optionMenu = UIAlertController(title: "Alert", message: alertText, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            optionMenu.removeFromParent()
        })
        
        optionMenu.addAction(okAction)
        
        viewcontroller.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
}

