//
//  Enumerations.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 16/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation


enum SelectOption{
    case CheckIn
    case CheckOut
    case MealType
}
