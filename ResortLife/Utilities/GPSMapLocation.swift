//
//  GPSMapLocation.swift
//  TaxiSwiftApp
//
//  Created by Kondaiah V on 2/13/18.
//  Copyright © 2018 Provab Technosoft Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreLocation

// define micros...
let GPlaceKey = "AIzaSyAxyJICj2WgwUlQ6PLo5e_EasOKAEWuSLU" // testing
//let GPlaceKey = "AIzaSyDQfDFKLm_jMc9f5xMKRWM049Ega4v6SV4" // Production


let kGeoUpdateLocation = Notification.Name("GeoUpdateLocation")

// Blocks (Closures) declaration...
typealias SearchResult = (_ addressArr: [Any], _ success: Bool, _ message: String) -> Void
typealias LatiLongResult = (_ geometryDict: [AnyHashable: Any], _ success: Bool, _ message: String) -> Void
typealias AddressResult = (_ addressDict: [AnyHashable: Any], _ success: Bool, _ message: String) -> Void
typealias PolylineResult = (_ polylinesArr: [Any], _ success: Bool, _ message: String) -> Void


// MARK:- Interface
class GPSMapLocation: NSObject, CLLocationManagerDelegate {

    // MARK:- Variables
    public var locationManager: CLLocationManager!
    public var locationCoordinate2D: CLLocationCoordinate2D!
	public var locationsBearingDictionary: [String: Any] = [:]
    
    private var oldLocation: CLLocation!
    private var newLocation: CLLocation!
    
    
    // Singleton Instance
    static let shared = GPSMapLocation()
	
	// MARK:-
	private override init() {}
	
	
    // location permissions...
    func GetLocationPermission() {
        
        locationManager = CLLocationManager ()
        locationManager.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone // 10 meters
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = .otherNavigation //.automotiveNavigation
      
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.pausesLocationUpdatesAutomatically = true
        //locationManager.allowsBackgroundLocationUpdates = true
    }
    
    func LocationStartUpdate() {
        if locationManager != nil {
            locationManager.startUpdatingLocation()
        }
    }
    
    func LocationStopUpdate() {
        if locationManager != nil {
            locationManager.stopUpdatingLocation()
            locationManager = nil
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // setup old location and new location...
        let currentLocation = locations.last
        if oldLocation == nil {
            oldLocation = currentLocation
        }
        else if locations.count >= 2 {
            oldLocation = locations[locations.count-2]
        }
        else {
            oldLocation = newLocation
        }
        
        // new location and bearing angles...
        newLocation = currentLocation
        let bearAngle: Double = getBearingAngleBetweenLocations(sourceLocation: oldLocation, destination: newLocation)
        
        locationCoordinate2D = newLocation.coordinate
        locationsBearingDictionary = ["new_location": newLocation, "old_location": oldLocation, "bearing": bearAngle]
        //print("Final dictionary : \(locationsBearingDictionary)")
		NotificationCenter.default.post(name: kGeoUpdateLocation, object: nil, userInfo: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .denied || status == .restricted {
            self.perform(#selector(deniedAlertFuncation), with: nil, afterDelay: 2.0)
            return
        }
        else if status == .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
            print("Location Permission :- authorizedWhenInUse")
        }
        else if status == .authorizedAlways {
            locationManager.requestAlwaysAuthorization()
            print("Location Permission :- authorizedAlways")
        }
        else {}
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location fails :\(error.localizedDescription) : \(error._code)")
    }
    
    @objc func deniedAlertFuncation() -> Void {
        
        let alertCtrl = UIAlertController.init(title: "Location Services Disabled", message: "Please enable location service from your phone's settings", preferredStyle: .alert)
		
        let canAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action:UIAlertAction) in })
        let enbleAction = UIAlertAction.init(title: "Enable", style: .default, handler: { (action:UIAlertAction) in
			
			if #available(iOS 10.0, *) {
				UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options:[:], completionHandler: nil)
			}
			else {
				// Fallback on earlier versions
				UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
			}
        })
        alertCtrl.addAction(canAction)
        alertCtrl.addAction(enbleAction)
        (UIApplication.shared.delegate?.window??.rootViewController)!.present(alertCtrl, animated: true, completion: nil)
    }
}

extension GPSMapLocation {
    
    // MARK: - Bearing Angle
    func getBearingAngleBetweenLocations(sourceLocation: CLLocation, destination: CLLocation) -> Double {
        
        // getting to and from latitude and longitude angle...
        let lat1: Double = DegreesToRadians(degrees: sourceLocation.coordinate.latitude)
        let lon1: Double = DegreesToRadians(degrees: sourceLocation.coordinate.longitude)
        
        let lat2: Double = DegreesToRadians(degrees: destination.coordinate.latitude)
        let lon2: Double = DegreesToRadians(degrees: destination.coordinate.longitude)
        
        // calculating angle between points...
        let dLon: Double = lon2 - lon1
        let y: Double = sin(dLon) * cos(lat2)
        let x: Double = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        
        var radiansBearing: Double = atan2(y, x)
        if radiansBearing < 0.0 {
            radiansBearing += (2 * .pi)
        }
        return RadiansToDegrees(radians: radiansBearing)
    }
    
    func DegreesToRadians(degrees: Double) -> Double {
        return degrees * .pi / 180.0
    }
    
    func RadiansToDegrees(radians: Double) -> Double {
        return radians * 180.0 / .pi
    }
}

extension GPSMapLocation {
    
    // MARK: - Locations Service
    // getting search location using search string, if you want filter(restricted) based on country wide...
    func GetSearchingAddressFromGoogle(searchText: String, countryId: String, ResultBlock: @escaping SearchResult) -> Void {
        
        // url string creations...
        var urlString: String!
        if countryId.count > 0 && countryId.count < 3 {
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchText)&components=country:\(countryId)&types=geocode&language=en-US&key=\(GPlaceKey)"
        }
        else {
            urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchText)&types=geocode&language=en-US&key=\(GPlaceKey)"
        }
		urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
		print("URL : \(String(describing: urlString))")
        
        // session creations...
        let defaultConfiure = URLSessionConfiguration.default
        let defaultSession = URLSession.init(configuration: defaultConfiure, delegate: nil, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: URL.init(string: urlString)!) { (data, response, error) in
            
            // final response getting...
            if error != nil {
                ResultBlock([Any](), false, (error?.localizedDescription)!)
            }
            else {
                do {
                    // if its no results
                    let jsonObj = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    guard let jsonDict = jsonObj as? [AnyHashable : Any] else {
                        ResultBlock([Any](), false, "NO Result")
                        return
                    }
                    
                    // if data avalaibles
                    if jsonDict["status"] as! String == "OK" {
                        ResultBlock(jsonDict["predictions"] as! [Any] , true , "Success")
                    }
                    else {
                        ResultBlock([Any]() , false, "NO Result")
                    }
                }
                catch {
                    ResultBlock([Any]() , false, error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    // getting latitude & longtitude values using place id...
    func GetLatitudeAndLongitudeFromGoogle(placeId: String, ResultBlock: @escaping LatiLongResult) -> Void {
        
        // url string...
        var urlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&language=en-US&key=\(GPlaceKey)"
		urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
		
        // session creations...
        let defaultConfiure = URLSessionConfiguration.default
        let defaultSession = URLSession.init(configuration: defaultConfiure, delegate: nil, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: URL.init(string: urlString)!) { (data, response, error) in
            
            // final response getting...
            if error != nil {
                ResultBlock ([AnyHashable: Any](), false, (error?.localizedDescription)!)
            }
            else {
                do {
                    // if its no results...
                    let jsonObj = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    guard let jsonDict = jsonObj as? [AnyHashable : Any] else {
                        ResultBlock ([AnyHashable: Any](), false, "Not getting latitude and longtitude.")
                        return
                    }
                    
                    // if data avalaibles
                    if jsonDict["status"] as! String == "OK" {
                        ResultBlock(((jsonDict["result"] as! [AnyHashable: Any])["geometry"] as! [AnyHashable: Any])["location"] as! [AnyHashable: Any] , true , "Success")
                    }
                    else {
                        ResultBlock ([AnyHashable: Any](), false, "Not getting latitude and longtitude.")
                    }
                }
                catch {
                    ResultBlock ([AnyHashable: Any](), false, error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    // getting address using latitude & longtitude("12.912765,77.610538")...
    func GetAdderssFromGoogle(latAndlog: String, resultBlock: @escaping AddressResult) -> Void {
        
        // url string...
        var urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latAndlog)&language=en-US&sensor=true&key=\(GPlaceKey)"
		urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
		
        // session creations...
        let defaultConfiure = URLSessionConfiguration.default
        let defaultSession = URLSession.init(configuration: defaultConfiure, delegate: nil, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: URL.init(string: urlString)!) { (data, response, error) in
            
            // final response getting...
            if error != nil {
                resultBlock([AnyHashable: Any](), false, (error?.localizedDescription)!)
            }
            else {
                
                do {
                    
                    // if its no results
                    let jsonObj = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
					print("Google API :\(jsonObj)")
                    guard let jsonDict = jsonObj as? [AnyHashable : Any] else {
                        resultBlock([AnyHashable: Any](), false, "Not getting address.")
                        return
                    }
                    // success status getting...
                    if jsonDict["status"] as! String == "OK" {
                    
                        var address = "" // Main Address
                        var street_number = "" // Street number
                        var route = "" // Road name
                        var neighborhood = "" // Nearest street
                        
                        var sublocality1 = "" // Neres area names..
                        var sublocality2 = ""
                        var sublocality3 = ""
            
                        var city = "" // city name
                        var district = "" // district name
                        var state = "" // state name
                        var country = "" // country name...
                        var zipcode = "" // Postal number
                        var latitude = ""
                        var longtitude = ""
                        
                        
                        // finding place components...
                        if let resultArray = jsonDict["results"] as? [Any] {
                            if let resultDict = resultArray[0] as? [AnyHashable : Any] {
                                
                                // main address...
                                if let addressMain = resultDict["formatted_address"] as? String {
                                    address = addressMain
                                }
                                
                                // latitude and longtitude...
                                if let geoMetryDict = resultDict["geometry"] as? [AnyHashable : Any] {
                                    if let locationDict = geoMetryDict["location"] as? [AnyHashable : Any] {
                                        
										if let latitude_str = locationDict["lat"] {
											latitude = "\(latitude_str)"
										}
										if let longtitude_str = locationDict["lng"] {
											longtitude = "\(longtitude_str)"
										}
                                    }
                                }
                                
                                
                                // sublocality components...
                                if let addressCompArray = resultDict["address_components"] as? [Any] {
                                    
                                    for i in 0 ..< addressCompArray.count {
                                        let addressDict = addressCompArray[i] as! [AnyHashable : Any]
                                        let addressKeysArray = addressDict["types"] as! [String]
                                        //print("Address keys :\(addressKeysArray)")
                                        
                                        // Route / Street Number / Neighborhood
                                        if addressKeysArray.contains("street_number") {
                                            street_number = addressDict["long_name"] as! String
                                        }
                                        else if addressKeysArray.contains("route") {
                                            route = addressDict["long_name"] as! String
                                        }
                                        else if addressKeysArray.contains("neighborhood") {
                                            neighborhood = addressDict["long_name"] as! String
                                        }
                                        // sublocality - Nearest Area Name
                                        else if addressKeysArray.contains("sublocality_level_3") {
                                            sublocality3 = addressDict["long_name"] as! String
                                        }
                                        else if addressKeysArray.contains("sublocality_level_2") {
                                            sublocality2 = addressDict["long_name"] as! String
                                        }
                                        else if addressKeysArray.contains("sublocality_level_1") {
                                            sublocality1 = addressDict["long_name"] as! String
                                        }
                                        // locality - City Name
                                        else if addressKeysArray.contains("locality") {
                                            city = addressDict["long_name"] as! String
                                        }
                                        // administrative - District / State / Country Name
                                        else if addressKeysArray.contains("administrative_area_level_2") {
                                            district = addressDict["long_name"] as! String
                                        }
                                        else if addressKeysArray.contains("administrative_area_level_1") {
                                            state = addressDict["long_name"] as! String
                                        }
                                        else if addressKeysArray.contains("country") {
                                            country = addressDict["long_name"] as! String
                                        }
                                        // postal_code / Zipcode
                                        else if addressKeysArray.contains("postal_code") {
                                            zipcode = addressDict["long_name"] as! String
                                        }
                                    }
                                }
                            }
                        }
                        
                        // sending final address details...
                        let addressDict = ["address":address, "street_number":street_number, "route":route, "neighborhood":neighborhood, "sublocality_3":sublocality3, "sublocality_2":sublocality2, "sublocality_1":sublocality1, "city":city, "state":state, "district":district, "country":country, "zipcode":zipcode, "latitude":latitude, "longitude":longtitude]
                        print("Address : \(addressDict)")
                        resultBlock(addressDict , true, "Success")
                    }
                    else {
                        resultBlock([AnyHashable: Any](), false, "Not getting address.")
                    }
                }
                catch {
                    resultBlock([AnyHashable: Any](), false, error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    //  origin, destination address or latlong("12.912765,77.610538")
    func GetPolylinesList(originLocation: String, destinationLocation: String, resultBlock: @escaping PolylineResult) -> Void {
        
        // url string...
        var urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(originLocation)&destination=\(destinationLocation)&mode=driving&language=en-US&key=\(GPlaceKey)&alternatives=true&optimistic=true"
		urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""
        
        // session creations...
        let defaultConfiure = URLSessionConfiguration.default
        let defaultSession = URLSession.init(configuration: defaultConfiure, delegate: nil, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: URL.init(string: urlString)!) { (data, response, error) in
            
            // final response getting...
            if error != nil {
                resultBlock([Any](), false, (error?.localizedDescription)!)
            }
            else {
                
                do {
                    
                    // if its no results
                    let jsonObj = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    guard let jsonDict = jsonObj as? [AnyHashable : Any] else {
                        resultBlock([Any](), false, "No routes.")
                        return
                    }
                    // success status getting...
                    if jsonDict["status"] as! String == "OK" {
                        
                        if let resultArray = jsonDict["routes"] as? [Any] {
                            resultBlock(resultArray, true, "No routes.")
                        }
                        else {
                            resultBlock([Any](), false, "No routes.")
                        }
                    }
                    else {
                        resultBlock([Any](), false, "No routes.")
                    }
                }
                catch {
                    resultBlock([Any](), false, error.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
}
