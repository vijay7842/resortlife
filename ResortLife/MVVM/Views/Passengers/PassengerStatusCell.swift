//
//  PassengerStatusCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 12/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class PassengerStatusCell: UITableViewCell {

    @IBOutlet weak var lbl_name:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK:- Configure cell
    func configureCell(index:Int){
        
        if let customer = BookingViewModel.hotelBookings[BookingViewModel.selectedIndex].customers?[index]{
        lbl_name.text = "\(customer.title). \(customer.first_name) \(customer.last_name)"
        }
        
    }
    
}
