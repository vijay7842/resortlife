//
//  PassengersCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 06/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class PassengersCell: UITableViewCell {

    @IBOutlet weak var lbl_heading:UILabel!
    @IBOutlet weak var txf_firstName:UITextField!
    @IBOutlet weak var txf_lastName:UITextField!
    @IBOutlet weak var txf_dateOfBirth:UITextField!
    @IBOutlet weak var txf_passportNumber:UITextField!
    @IBOutlet weak var txf_passportExpDate:UITextField!
    @IBOutlet weak var txf_passportCountry:UITextField!
    @IBOutlet weak var btn_passportCountry:UIButton!
    
    @IBOutlet var img_radioBtn:[UIImageView]!
    @IBOutlet var lbl_passengerTitle:[UILabel]!

    @IBOutlet weak var hei_passportDetails:NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Setup cell with data
    
    func loadCellData(data:PassengersModel){
        
        btn_passportCountry.tag = self.tag
      
        lbl_heading.text = "Enter the details of \(data.displayTitle)"
        
        txf_firstName.text = data.firstName
        txf_lastName.text = data.lastName
        
        for imageView in img_radioBtn{
            let tag = "\(imageView.tag)"
            imageView.image = (data.title == tag) ? #imageLiteral(resourceName: "radio_on") : #imageLiteral(resourceName: "radio_off")
        }
        for label in lbl_passengerTitle{
            let tag = "\(label.tag)"
            label.textColor = (data.title == tag) ? AppColors.green : UIColor.black
        }
        
            hei_passportDetails.constant = 0
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(selectDateOfBirthFromDatePicker(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        datePicker.tag = self.tag
        txf_dateOfBirth.inputView = datePicker
        
        if data.passportExpDate.count != 0{
            txf_passportExpDate.text = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: data.passportExpDate, toFormat: "dd MMM yyyy")
        }
        if data.dateOfBirth.count != 0{
            txf_dateOfBirth.text = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: data.dateOfBirth, toFormat: "dd MMM yyyy")
        }
    }
    
}

extension PassengersCell{
   
    //MARK:- IBActions
    @IBAction func passengerTypeClicked(_ sender:UIButton){
        
        if sender.tag == 1{
            PassengersViewModel.passengersList[self.tag].title = "1"
        }
        else if sender.tag == 2{
            PassengersViewModel.passengersList[self.tag].title = "2"
        }
        else if sender.tag == 3{
            PassengersViewModel.passengersList[self.tag].title = "3"
        }
        
        for imageView in img_radioBtn{
            imageView.image = (sender.tag == imageView.tag) ? #imageLiteral(resourceName: "radio_on") : #imageLiteral(resourceName: "radio_off")
        }
        for label in lbl_passengerTitle{
            label.textColor = (sender.tag == label.tag) ? AppColors.green : UIColor.black
        }
        
    }
    
    @IBAction func firstNameTextChanged(_ sender:UITextField){
        if sender.text?.count != 0{
            PassengersViewModel.passengersList[self.tag].firstName = sender.text ?? ""
        }
    }
    
    @IBAction func lastNameTextChanged(_ sender:UITextField){
        if sender.text?.count != 0{
            PassengersViewModel.passengersList[self.tag].lastName = sender.text ?? ""
        }
    }
    
    //MARK:- Select Date of Birth
    @objc func selectDateOfBirthFromDatePicker(_ sender:UIDatePicker){
        
        txf_dateOfBirth.text = DateFormatter.getDateString(formate: "dd MMM yyyy", date: sender.date)
        let selected_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: sender.date)
        
        PassengersViewModel.passengersList[self.tag].dateOfBirth = selected_date
    }
   
    
}
