//
//  PassengerDetailsCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 06/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class PassengerDetailsCell: UITableViewCell {

    @IBOutlet weak var lbl_passengerType:UILabel!
    @IBOutlet weak var lbl_name:UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Setup cell with data
    func loadCellData(data:PassengersModel){
        var title = ""
        if data.title == "1"{
            title = "Mr."
        }else if data.title == "2"{
            title = "Mrs."
        }else if data.title == "3"{
            title = "Ms."
        }else{}
        
        lbl_name.text = "\(title) \(data.firstName)"
        
        var passengerType = ""

        if data.passengerType == "1"{
            passengerType = "Adult \(self.tag+1)"
        }else if data.passengerType == "2"{
            passengerType = "Children \(self.tag+1)"
        }else if data.passengerType == "3"{
            passengerType = "Infant \(self.tag+1)"
        }else{}
        
        lbl_passengerType.text = passengerType
        
    }
    
}
