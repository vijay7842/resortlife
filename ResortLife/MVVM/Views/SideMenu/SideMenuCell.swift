//
//  SideMenuCell.swift
//  ResortLife
//
//  Created by Apple retina on 20/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var img_title:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- Configure cell
    func configureCell(index:Int){
        
        lbl_title.text = HomeViewModel.menuList[index].0
        img_title.image = UIImage.init(named: HomeViewModel.menuList[index].1)
    }
    
}
