//
//  TravellerSelectorCell.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class TravellerSelectorCell: UICollectionViewCell {

    @IBOutlet weak var lbl_number:UILabel!
    @IBOutlet weak var containerView:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    //MARK:- Setup cell data
    func setupCellData(index:Int,number:String,adultOrChild:Int,cellTag:Int){
       
        lbl_number.text = number
        
            let model =  HotelSearchViewModel.guestRoomsList[cellTag]
            
            if adultOrChild == 1{
                lbl_number.backgroundColor = (model.adultSelIndex == index) ? AppColors.green : UIColor.white
                lbl_number.textColor = (model.adultSelIndex == index) ?  UIColor.white : AppColors.green

            }
            else if adultOrChild == 2{
                lbl_number.backgroundColor = (model.childSelIndex == index) ? AppColors.green : UIColor.white
                lbl_number.textColor = (model.childSelIndex == index) ?  UIColor.white : AppColors.green
            }
        
    }
    
}
