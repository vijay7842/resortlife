//
//  ChildAgeListingCell.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class ChildAgeListingCell: UITableViewCell {

    @IBOutlet weak var coll_ages:UICollectionView!
    @IBOutlet weak var lbl_title:UILabel!

    let agesList = ["2","3","4","5","6","7","8","9","10","11"]

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.registerCells()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Register Cells
    func registerCells(){
        coll_ages.register(UINib.init(nibName: "ChildAgeSelectorCell", bundle: nil), forCellWithReuseIdentifier:
            "ChildAgeSelectorCell")
    }
    
    //MARK:- Set up cell data
    func setUpCellData(index:Int){
        self.lbl_title.text = "Age of Child\(index+1)"
        self.coll_ages.reloadData()
    }
    
}


extension ChildAgeListingCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return agesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChildAgeSelectorCell", for: indexPath) as! ChildAgeSelectorCell
        cell.tag = indexPath.row
        
        cell.setUpCellData(index: indexPath.row, age: agesList[indexPath.row],cellTag:self.tag,collectionView:collectionView)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: 50, height: 50)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        updateData(index: indexPath.row)
        
        collectionView.reloadData()
    }
    
    //MARK:- Updated data
    func updateData(index:Int){
        guard let cell = coll_ages.superview?.superview?.superview?.superview?.superview?.superview?.superview?.superview as? SelectGuestRoomsCell else {
            return
        }
        
        var model = HotelSearchViewModel.guestRoomsList[cell.tag].childsList[self.tag]
        model.selectedIndex = index
        model.age = agesList[index]
    HotelSearchViewModel.guestRoomsList[cell.tag].childsList[self.tag] = model
    }
    
}


