//
//  SelectGuestRoomsCell.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class SelectGuestRoomsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var btn_delete:UIButton!

    @IBOutlet weak var coll_adults:UICollectionView!
    @IBOutlet weak var coll_children:UICollectionView!
    @IBOutlet weak var tbl_childrenAge:UITableView!
    
    @IBOutlet weak var hei_tblAge:NSLayoutConstraint!
    
    let adultsList:[String] = ["1","2","3","4"]
    let childrenList:[String] = ["0","1","2"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.registerCells()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- Register Cells
    func registerCells(){
        coll_adults.register(UINib.init(nibName: "TravellerSelectorCell", bundle: nil), forCellWithReuseIdentifier:
            "TravellerSelectorCell")
        coll_children.register(UINib.init(nibName: "TravellerSelectorCell", bundle: nil), forCellWithReuseIdentifier:
            "TravellerSelectorCell")
        tbl_childrenAge.register(UINib.init(nibName: "ChildAgeListingCell", bundle: nil), forCellReuseIdentifier: "ChildAgeListingCell")
        
    }
    
    //MARK:- Setup cell data
    func setUpCellData(){
        btn_delete.tag = self.tag
        btn_delete.isHidden = (self.tag == 0) ? true : false
        
        lbl_title.text = "Room \(self.tag+1)"
      
        tbl_childrenAge.reloadData()

        hei_tblAge.constant = CGFloat(HotelSearchViewModel.guestRoomsList[self.tag].childsList.count*110)

    }
    
    
    func reloadAndUpdateTblHeight(){
        
        tbl_childrenAge.reloadData()
        
        NotificationCenter.default.post(name: kGuestTblReload, object: nil)
    }
    
}

extension SelectGuestRoomsCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == coll_adults{
            return adultsList.count
        }
        else{
            return childrenList.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == coll_adults{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TravellerSelectorCell", for: indexPath) as! TravellerSelectorCell
            
            cell.setupCellData(index: indexPath.row, number:adultsList[indexPath.row],adultOrChild:1,cellTag:self.tag)
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TravellerSelectorCell", for: indexPath) as! TravellerSelectorCell
            
            cell.setupCellData(index: indexPath.row, number:childrenList[indexPath.row],adultOrChild:2,cellTag:self.tag)
            
            return cell
        }
       
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: 60, height: collectionView.frame.height)
        return size
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == coll_adults{
        HotelSearchViewModel.guestRoomsList[self.tag].adultsCount = indexPath.row+1
        HotelSearchViewModel.guestRoomsList[self.tag].adultSelIndex = indexPath.row
           
            collectionView.reloadData()

        }else{
        HotelSearchViewModel.guestRoomsList[self.tag].childrenCount = indexPath.row
        HotelSearchViewModel.guestRoomsList[self.tag].childSelIndex = indexPath.row
            
            collectionView.reloadData()

            HotelSearchViewModel.updateModel(index: indexPath.row,roomIndex:self.tag)
            
            self.reloadAndUpdateTblHeight()
            
        }
       
    }
   
}

extension SelectGuestRoomsCell:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HotelSearchViewModel.guestRoomsList[self.tag].childsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildAgeListingCell", for: indexPath) as! ChildAgeListingCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
       
        cell.setUpCellData(index:indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
