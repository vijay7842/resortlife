//
//  ChildAgeSelectorCell.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class ChildAgeSelectorCell: UICollectionViewCell {

    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var lbl_age:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func draw(_ rect: CGRect) {
        containerView.cornerRadius = containerView.frame.width/2
    }
    
    //MARK:- Set up cell data
    func setUpCellData(index:Int,age:String,cellTag:Int,collectionView:UICollectionView){
       
        lbl_age.text = age
        
        guard let cell = collectionView.superview?.superview?.superview?.superview?.superview?.superview?.superview?.superview as? SelectGuestRoomsCell else {
            return
        }
        
       let model = HotelSearchViewModel.guestRoomsList[cell.tag].childsList[cellTag]
        
        print("cell.tag \(cell.tag) , selectedIndex\(model.selectedIndex), cellTag\(cellTag), index\(index)")

        containerView.backgroundColor = (model.selectedIndex == index) ? AppColors.green : UIColor.white
        lbl_age.textColor = (model.selectedIndex == index) ?  UIColor.white : AppColors.green

    }

}
