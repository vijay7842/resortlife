//
//  FlightDetailsCell.swift
//  ResortLife
//
//  Created by Apple retina on 25/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class FlightDetailsCell: UITableViewCell {

    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var txf_flightName:UITextField!
    @IBOutlet weak var txf_flightNumber:UITextField!
    @IBOutlet weak var txf_date:UITextField!
    @IBOutlet weak var txf_time:UITextField!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Load cell data
    func loadCellData(index:Int){
        self.tag = index
        
        lbl_title.text = (index==0) ? "Arrival" : "Departure"
       
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(selectDateFromDatePicker(_:)), for: .valueChanged)
        datePicker.minimumDate = Date()
        datePicker.tag = self.tag
        txf_date.inputView = datePicker
        
        let timePicker = UIDatePicker()
        timePicker.datePickerMode = .time
        timePicker.addTarget(self, action: #selector(selectTimeFromDatePicker(_:)), for: .valueChanged)
        timePicker.tag = self.tag
        txf_time.inputView = timePicker
        
        if index == 0{
            txf_flightName.text = HotelReviewViewModel.flight_arrival.flight_name
            txf_flightNumber.text = HotelReviewViewModel.flight_arrival.flight_number
            if HotelReviewViewModel.flight_arrival.flight_date.count != 0{
                txf_date.text = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: HotelReviewViewModel.flight_arrival.flight_date, toFormat: "dd MMM yyyy")
            }
            if HotelReviewViewModel.flight_arrival.flight_time.count != 0{
                txf_time.text = DateFormatter.changeDateFormatString(fromFormat: "HH:mm", date: HotelReviewViewModel.flight_arrival.flight_time, toFormat: "hh:mm a")
                
            }
            
        }else{
            txf_flightName.text = HotelReviewViewModel.flight_departure.flight_name
            txf_flightNumber.text = HotelReviewViewModel.flight_departure.flight_number
            if HotelReviewViewModel.flight_departure.flight_date.count != 0{
                txf_date.text = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: HotelReviewViewModel.flight_departure.flight_date, toFormat: "dd MMM yyyy")

            }
            if HotelReviewViewModel.flight_departure.flight_time.count != 0{
                txf_time.text = DateFormatter.changeDateFormatString(fromFormat: "HH:mm", date: HotelReviewViewModel.flight_departure.flight_time, toFormat: "hh:mm a")
            }
            
        }
        
    }
    
    
}

extension FlightDetailsCell{
    //MARK:- IBActions
    
    @IBAction func flightNameTextChanged(_ sender:UITextField){
        if sender.text?.count != 0{
            if self.tag == 0{
                HotelReviewViewModel.flight_arrival.flight_name = sender.text ?? ""
            }else{
                HotelReviewViewModel.flight_departure.flight_name = sender.text ?? ""
            }
            
        }
    }
    
    @IBAction func flightNumberTextChanged(_ sender:UITextField){
        if sender.text?.count != 0{
            if self.tag == 0{
                HotelReviewViewModel.flight_arrival.flight_number = sender.text ?? ""
            }else{
                HotelReviewViewModel.flight_departure.flight_number = sender.text ?? ""
            }
        }
    }
    
    //MARK:- Select Date
    @objc func selectDateFromDatePicker(_ sender:UIDatePicker){
        
        txf_date.text = DateFormatter.getDateString(formate: "dd MMM yyyy", date: sender.date)
        let selected_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: sender.date)
        if sender.tag == 0{
            HotelReviewViewModel.flight_arrival.flight_date = selected_date
        }else{
            HotelReviewViewModel.flight_departure.flight_date = selected_date
        }

    }
    
    //MARK:- Select Time
    @objc func selectTimeFromDatePicker(_ sender:UIDatePicker){
        
        txf_time.text = DateFormatter.getDateString(formate: "hh:mm a", date: sender.date)
          let selected_time = DateFormatter.getDateString(formate: "HH:mm", date: sender.date)
        if sender.tag == 0{
            HotelReviewViewModel.flight_arrival.flight_time = selected_time
        }else{
            HotelReviewViewModel.flight_departure.flight_time = selected_time
        }
        
    }
}
