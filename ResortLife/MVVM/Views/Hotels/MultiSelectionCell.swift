//
//  MultiSelectionCell.swift
//  ElamantTravel
//
//  Created by Apple retina on 03/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class MultiSelectionCell: UITableViewCell {

    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var img_checkBox:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
