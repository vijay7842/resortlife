//
//  RoomOptionsCell.swift
//  ResortLife
//
//  Created by Apple retina on 22/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class RoomOptionsCell: UITableViewCell {

    @IBOutlet weak var lbl_title:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Configure cell
    func configureCellWithData(index:Int){
        self.tag = index
        
        lbl_title.textColor = UIColor.black
        self.isUserInteractionEnabled = true
        
        let data = HotelDetailsViewModel.hotelRoomsList[HotelDetailsViewModel.selectedRoomIndex]
        
        switch HotelDetailsViewModel.selectedOption {
            
        case .CheckIn:
            lbl_title.text = data.datesList[index]
            
            if index < HotelDetailsViewModel.lastSelectedIndex{
                lbl_title.textColor = UIColor.lightGray
                self.isUserInteractionEnabled = false
            }
            
        case .CheckOut:
            lbl_title.text = data.datesList[index]
            
            if index < HotelDetailsViewModel.lastSelectedIndex{
                lbl_title.textColor = UIColor.lightGray
                self.isUserInteractionEnabled = false
            }
            
        case .MealType:
            lbl_title.text = data.mealPlans[index].meal_plan
        }
        
    }
    
}
