//
//  ImagesCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 05/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class ImagesCell: UICollectionViewCell {

    @IBOutlet weak var img_hotel:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- Configure cell
    func configureCell(index:Int){
        if var url = HotelDetailsViewModel.hotelDetails?.images[index]{
            
            url = url.replacingOccurrences(of: " ", with: "%20")
            let imageUrl = "\(Server.image_url)\(url)"
            
            img_hotel.sd_setShowActivityIndicatorView(true)
            img_hotel.sd_setIndicatorStyle(.gray)
            img_hotel.sd_setImage(with: URL.init(string: imageUrl)) { (image, error, cache, urls) in
                self.img_hotel.image = (error != nil) ? hotelPlaceHolder : image
            }
        }
        
    }
    
}
