//
//  DescriptionCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 12/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet weak var lbl_description:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
