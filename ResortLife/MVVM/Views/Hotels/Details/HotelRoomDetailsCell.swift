//
//  HotelRoomDetailsCell.swift
//  ElamantTravel
//
//  Created by Apple retina on 08/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class HotelRoomDetailsCell: UITableViewCell {

    @IBOutlet weak var lbl_roomName:UILabel!
    @IBOutlet weak var lbl_amenities:UILabel!
    @IBOutlet weak var lbl_refundStatus:UILabel!
    @IBOutlet weak var lbl_noOfNights:UILabel!
    @IBOutlet weak var lbl_roomPrice:UILabel!
    @IBOutlet weak var lbl_select:UILabel!
    @IBOutlet weak var img_select:UIImageView!
    @IBOutlet weak var view_select:UIView!
    @IBOutlet weak var lbl_checkInDate:UILabel!
    @IBOutlet weak var lbl_checkOutDate:UILabel!
    @IBOutlet weak var lbl_foodType:UILabel!

    @IBOutlet weak var btn_select:UIButton!
    @IBOutlet weak var btn_policy:UIButton!
    @IBOutlet weak var btn_checkInDate:UIButton!
    @IBOutlet weak var btn_checkOutDate:UIButton!
    @IBOutlet weak var btn_foodType:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Load cell data
    
    func loadCellData(index:Int){
        
        self.tag = index
        btn_select.tag = index
        btn_policy.tag = index
        btn_checkInDate.tag = index
        btn_checkOutDate.tag = index
        btn_foodType.tag = index
        
        lbl_checkInDate.text = "Check In Date"
        lbl_checkOutDate.text = "Check Out Date"
        lbl_foodType.text = "Breakfast"

        let data = HotelDetailsViewModel.hotelRoomsList[index]
        lbl_roomName.text = data.roomName
        lbl_roomPrice.text = String(format:"%@ %.2f",data.currency_symbol,data.roomPrice)
        
        if data.selected_check_in.count != 0{
            lbl_checkInDate.text = data.selected_check_in
        }
        if data.selected_check_out.count != 0{
            lbl_checkOutDate.text = data.selected_check_out
        }
        if data.selected_meal_plan.count != 0{
            lbl_foodType.text = data.selected_meal_plan
        }
        let amenities = data.amenities.joined(separator: ", ")
        lbl_amenities.text = amenities
        
        var refundStatus = ""
        
        if data.cancellation_policy != ""{
            if data.cancellation_policy.contains("non-refundable"){
                refundStatus = "No-Refund"
            }else{
                refundStatus = "Refundable"
            }
        }
        
        lbl_refundStatus.text = refundStatus
        lbl_noOfNights.text = "(\(HotelViewModel.details.noOf_nights) Nights)"
        
        var selectStatus = ""
        
        // if HotelDetailsViewModel.selectedRoomIndex == index{
        if HotelDetailsViewModel.selectedRoomIndexes.contains(index){
            
            selectStatus = "Selected"
            img_select.image = UIImage.init(named: "round_tick")
            lbl_select.textColor = UIColor.white
            view_select.backgroundColor = AppColors.green
        }else{
            selectStatus = "Select"
            img_select.image = UIImage.init(named: "")
            lbl_select.textColor = UIColor.black
            view_select.backgroundColor = UIColor.white
        }
        lbl_select.text = selectStatus
        
    }
    
}
