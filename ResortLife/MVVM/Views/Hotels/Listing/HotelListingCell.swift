//
//  HotelListingCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 05/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class HotelListingCell: UITableViewCell {

    @IBOutlet weak var lbl_hotelName:UILabel!
    @IBOutlet weak var lbl_location:UILabel!
    @IBOutlet weak var lbl_currency:UILabel!
    @IBOutlet weak var lbl_price:UILabel!

    @IBOutlet var img_stars:[UIImageView]!
    @IBOutlet var img_amenities:[UIImageView]!

    @IBOutlet weak var coll_images:UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coll_images.register(UINib.init(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK:- Setup cell data
    func setUpCellData(index:Int){
        
        let data = HotelSearchViewModel.filter_searchResults[index]
        lbl_hotelName.text = data.hotel_name
        lbl_location.text = data.address
        lbl_currency.text = kCurrency
        lbl_price.text = String(format: "%.2f", data.room_price)
        
        coll_images.reloadData()
        
        let rating = data.star_rating
        
        for imageView in img_stars{
            let tag = "\(imageView.tag)"
            imageView.image = #imageLiteral(resourceName: "starSelected")
            if tag == rating{
                break
            }
        }
        
        let amenities = data.hotel_amenitites.joined(separator: ",")
        
        for imageView in img_amenities{
            
            imageView.alpha = 0.2

            if imageView.tag == 1 && amenities.localizedCaseInsensitiveContains("parking"){
                imageView.alpha = 1.0
            }
            if imageView.tag == 2 && amenities.localizedCaseInsensitiveContains("Wi-fi"){
                imageView.alpha = 1.0
            }
            if imageView.tag == 3 && amenities.localizedCaseInsensitiveContains("breakfast"){
                imageView.alpha = 1.0
            }
            if imageView.tag == 4 && amenities.localizedCaseInsensitiveContains("pool"){
                imageView.alpha = 1.0
            }
        }
    }
    
}
extension HotelListingCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
        
        let imageUrl = "\(Server.image_url)\(HotelSearchViewModel.filter_searchResults[self.tag].hotel_picture)"
        
        cell.img_hotel.sd_setShowActivityIndicatorView(true)
        cell.img_hotel.sd_setIndicatorStyle(.gray)
        cell.img_hotel.sd_setImage(with: URL.init(string: imageUrl)) { (image, error, cache, urls) in
            cell.img_hotel.image = (error != nil) ? hotelPlaceHolder : image
        }
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      //  let size = CGSize.init(width: (collectionView.frame.width/2)+50, height: collectionView.frame.height)
        let size = CGSize.init(width: collectionView.frame.width, height: collectionView.frame.height)

        return size
    }
    
}
