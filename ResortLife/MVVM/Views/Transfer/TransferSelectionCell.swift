//
//  TransferSelectionCell.swift
//  ResortLife
//
//  Created by Apple retina on 21/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class TransferSelectionCell: UITableViewCell {

    @IBOutlet weak var img_checkBox:UIImageView!
    @IBOutlet weak var lbl_name:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Configure cell
    func configureCellWithData(index:Int){
        self.tag = index
        
        let data = HotelDetailsViewModel.hotelTransfers[index]
       
        let transferName = "\(data.transfer_name) (Adult: \(kCurrency)\(data.trans_adult), Child: \(kCurrency)\(data.trans_child), Infant: \(kCurrency)\(data.trans_infant))"
        lbl_name.text = transferName
        
        if HotelDetailsViewModel.selectedTransfers.contains(data.id){
           img_checkBox.image = #imageLiteral(resourceName: "checked")
        }else{
            img_checkBox.image = #imageLiteral(resourceName: "unchecked")
        }
        
    }
    
}
