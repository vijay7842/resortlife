//
//  CountrySelectionView.swift
//  ResortLife
//
//  Created by Apple retina on 28/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

protocol SelectCountryDelegate {
    func selectedCountryInfo(data:CountryModel)
    func removeView()

}


class CountrySelectionView: UIView {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tbl_list:UITableView!
    
    //MARK:- Variables
    var delegate:SelectCountryDelegate?
    
    //MARK:- View life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        registerCells()
    }
   

}
extension CountrySelectionView{
  
    //MARK:- IBActions
    @IBAction func closeViewClicked(_ sender:UIButton){
        delegate?.removeView()
    }
    
}
extension CountrySelectionView:UITableViewDelegate,UITableViewDataSource{
    
    func registerCells(){
       tbl_list.register(UINib.init(nibName: "CountryListCell", bundle: nil), forCellReuseIdentifier: "CountryListCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HotelCitiesModel.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryListCell", for: indexPath) as! CountryListCell
        cell.selectionStyle = .none
        cell.configureCell(index:indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedCountryInfo(data:  HotelCitiesModel.countryList[indexPath.row])
        delegate?.removeView()

    }
    
}
