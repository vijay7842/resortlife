//
//  CountryListCell.swift
//  ResortLife
//
//  Created by Apple retina on 28/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class CountryListCell: UITableViewCell {

    @IBOutlet weak var lbl_name:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Configure cell
    func configureCell(index:Int){
        let data = HotelCitiesModel.countryList[index]
        lbl_name.text = "\(data.country_name) \(data.dial_code)"
    }
    
}


