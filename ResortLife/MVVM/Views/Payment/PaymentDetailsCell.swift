//
//  PaymentDetailsCell.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 12/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class PaymentDetailsCell: UITableViewCell {

    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_amount:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Show payment details
    func showPaymentDetails(index:Int){
        
        let model = BookingViewModel.hotelBookings[BookingViewModel.selectedIndex]
        let baseFare = (model.grand_total ?? 0)-(model.tax_total ?? 0)
        
        if index == 0{
            lbl_title.text = "Base fare"
            lbl_amount.text = String.init(format: "%@ %.02f",kCurrency, baseFare)
        }else{
            lbl_title.text = "Taxes and fees"
            lbl_amount.text = String.init(format: "%@ %.02f",kCurrency, model.tax_total ?? 0)
        }
        
        
    }
    
    //MARK:- Show hotel price details
    func showHotelPriceDetails(index:Int){
        
            lbl_title.text = HotelReviewViewModel.getPriceDetails().0[index]
            if index==0{
                lbl_amount.text = String.init(format: "%@%.02f",kCurrency, HotelReviewViewModel.getPriceDetails().1[index])
            }else{
                lbl_amount.text = String.init(format: "+ %@%.02f",kCurrency, HotelReviewViewModel.getPriceDetails().1[index])
            }
        
    }
   
}
