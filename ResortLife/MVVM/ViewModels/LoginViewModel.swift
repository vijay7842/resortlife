//
//  LoginViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 18/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

// typealias closure
typealias UniqueHandler = () -> Void
typealias UniqueMsgHandler = (_ message: String) -> Void
typealias OtpHandler = (_ otp: String) -> Void

struct LoginViewModel {

    
}


extension LoginViewModel {
    
    // MARK:- API's
    
    //Login
   static func loginAPICall(params: [String: String], handler: @escaping UniqueMsgHandler) -> Void {
        
       // SwiftLoader.show(animated: true)
        DialougeUtils.addActivityView(view: appDelegate.window!)

        // calling api...
        VKAPIs.shared.getRequestXwwwform(params: params, file: kUserLogin, httpMethod: .POST)
        { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Login response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    if let status = result["status"] as? Bool,status == true{
                        
                        if let user_dict = result["data"] as? [String: Any] {
                            
                        UserViewModel.saveUserDetails(user_dict: user_dict)
                        handler(result["message"] as? String ?? "")
                            
                        }
                        
                    } else {
                        
                        // error message...
                        appDelegate.window?.makeToast(message: result["message"] as? String ?? "")
                    }
                }
            }
            else {
                
                print("Login Personal/Professional error : \(String(describing: error?.localizedDescription))")
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
           // SwiftLoader.hide()
            
            DialougeUtils.removeActivityView(view: appDelegate.window!)

        }
        
  }
    
    //Forgot password
   static func forgotPasswordAPICall(params: [String: String], handler: @escaping UniqueMsgHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)

        // calling api...
        VKAPIs.shared.getRequestXwwwform(params: params, file: kforgotPassword, httpMethod: .POST)
        { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Forgot password response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    if let status = result["status"] as? Bool,status == true{
                        
                       handler(result["message"] as? String ?? "")
                        
                    } else {
                        
                        // error message...
                        appDelegate.window?.makeToast(message: result["message"] as? String ?? "")
                    }
                }
          
            }
            else {
                
                print("Login Personal/Professional error : \(String(describing: error?.localizedDescription))")
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
        }
        
    }
    
    //Change password
   static func changePasswordAPICall(params: [String: String], handler: @escaping UniqueMsgHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)

        // calling api...
        VKAPIs.shared.getRequestXwwwform(params: params, file: kChangePassword, httpMethod: .POST)
        { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Change password response: \(String(describing: resultObj))")
                
                // response information...
                if let result = resultObj as? [String: Any] {
                    
                    if let status = result["status"] as? Bool,status == true{
                        
                        handler(result["message"] as? String ?? "")
                        
                    } else {
                        
                        // error message...
                        appDelegate.window?.makeToast(message: result["message"] as? String ?? "")
                    }
                }
                
            }
            else {
                
                print("Login Personal/Professional error : \(String(describing: error?.localizedDescription))")
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
        }
        
    }

}
