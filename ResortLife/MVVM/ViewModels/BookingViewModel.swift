//
//  BookingViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

class BookingViewModel{
    
    //MARK:- Variables
    static var hotelBookings = [HotelBookingsModel]()
    static var selectedIndex:Int = -1

    //MARK:-  clear Data
    static func clearData() {
        hotelBookings.removeAll()
    }
    
    //MARK:- Create booking Models
    static func createBookingModels(result:[String:Any]){
        
        if let dataDict = result["data"] as? [String:Any],let hotels = dataDict["booking_details"] as? [[String:Any]], hotels.count != 0{
                self.hotelBookings = HotelBookingsModel.createModels(array: hotels)
            }
      
    }
}

extension BookingViewModel{
    //MARK:- API's
    
    static func getBookingsApiCall(handler: @escaping UniqueHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        self.clearData()
       
        var params = [String: String] ()
        params["user_type"] = UserViewModel.userData?.user_type
        params["user_id"] = UserViewModel.userData?.user_id
        
        // calling api...
        VKAPIs.shared.getRequestXwwwform(params: params, file: kHotelBookings, httpMethod: .POST)
        { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Bookings success: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        
                        print("result \(result)")
                        self.createBookingModels(result: result)
                        
                    }else{
                      //  appDelegate.window?.makeToast(message: model.message)
                    }
                    
                    handler()
                    
                } else {
                    print("homePageAds formate : \(String(describing: resultObj))")
                }
            }
            else {
                // error message...
                print("error message : \(error?.localizedDescription ?? ""))")
                
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            
            DialougeUtils.removeActivityView(view: appDelegate.window!)
        }
    }
    
}
