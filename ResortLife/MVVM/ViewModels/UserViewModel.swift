//
//  UserViewModel.swift
//  BaseProject
//
//  Created by PTBLR-1206 on 31/01/20.
//  Copyright © 2020 PTBLR-1206. All rights reserved.
//

import Foundation
import UIKit

class UserViewModel {
    
    //MARK:- Variables
    static var userData: UserModel?
    static let titlesArray:[(String,String)] = [("Mr","1"),("Ms","2"),("Miss","3"),("Master","4"),("Mrs","5"),("Mstr","6")]
    
    //MARK:- Save user details
    static func saveUserDetails(user_dict: [String: Any]) {
        
        if let user_data = try? JSONSerialization.data(withJSONObject: user_dict, options: []){
            UserDefaults.standard.set(user_data, forKey: kUser_Profile)
        }
        
        self.loadUserDataFromUserDefaults()
        
        print("userData.user_id \(userData?.user_id ?? "")")
        
    }
    
    //MARK:- Load details from User defaults
    
    static func loadUserDataFromUserDefaults() {
        
        let userDict = getUserDictionary()
        userData = UserModel.init(details: userDict)
        
    }
    
    //MARK:- Check titles
    static func getTitleFromId(title_id:String) -> String{
        var title = ""
        
        switch title_id {
        case "1":
            title = "Mr"
        case "2":
            title = "Ms"
        case "3":
            title = "Miss"
        case "4":
            title = "Master"
        case "5":
            title = "Mrs"
        case "6":
            title = "Mstr"
        default:
            title = "Mr"
        }
        return title
    }
    
}
extension UserViewModel{
    
    //MARK:- API calls
    static func getProfileDetails(params: [String: String], handler: @escaping UniqueHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        // calling api...
        VKAPIs.shared.getRequestXwwwform(params: params, file: kGetProfileDetails, httpMethod: .POST)
        { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Profile Details response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    if let status = result["status"] as? Bool,status == true{
                        
                        if let user_dict = result["data"] as? [String: Any] {
                            
                            UserViewModel.saveUserDetails(user_dict: user_dict)
                            handler()
                        }
                        
                    } else {
                        
                        // error message...
                        appDelegate.window?.makeToast(message: result["message"] as? String ?? "")
                    }
                }
                
            }
            else {
                
                print("Login Personal/Professional error : \(String(describing: error?.localizedDescription))")
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
        }
        
    }
    
    static func updateUserProfile(params: [String: String],imageParms:[String:UIImage], handler: @escaping UniqueMsgHandler)->Void{
        
        DialougeUtils.addActivityView(view: appDelegate.window!)

        VKAPIs.shared.getRequestFormdata(params: params, images: imageParms, file: kUpdateProfile, httpMethod: .POST, handler:  { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Update profile response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        
                        handler(result["message"] as? String ?? "")
                        
                    }else{
                        appDelegate.window?.makeToast(message: model.message)
                    }
                   
                }
                
            }
            else {
                
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
            
        })
    }
    
}

// MARK:- Updates
extension UserViewModel {
    
    static func getUserDictionary() -> [String: Any] {
        
        var user_info: [String: Any] = [:]
        
        // convert login user data to model...
        let data = UserDefaults.standard.data(forKey: kUser_Profile)
        do {
            
            // getting user info...
            let json_obj = try JSONSerialization.jsonObject(with: data ?? Data(), options: [])
            if let user_dict = json_obj as? [String: Any] {
                user_info = user_dict
            }
        } catch let error as NSError {
            print("User to load: \(error.localizedDescription)")
        }
        return user_info
    }
    
   static func saveUserDictionaryFromModel() {
      
        if let user_data = try? JSONEncoder().encode(UserViewModel.userData) {
            UserDefaults.standard.set(user_data, forKey: kUser_Profile)
        }
    }
    
}
