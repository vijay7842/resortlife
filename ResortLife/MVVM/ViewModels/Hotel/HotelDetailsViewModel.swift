//
//  HotelDetailsViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 09/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HotelDetailsViewModel {
    
    //MARK:- Variables
    static var hotelDetails:HotelDetailsModel?
    static var hotelTransfers: [HotelTransfersModel] = []

    static var hotelRoomsList: [HotelRoomsModel] = []

    static var token:String = ""
    static var token_key:String = ""
    static var trace_id:String = ""
    static var search_hash:String = ""

    static var selectedTransfers: [String] = []

    static var selectedRoomIndex:Int = 0
    static var selectedRoomIndexes:[Int] = []

    static var selectedOption:SelectOption = .CheckIn
    static var lastSelectedIndex:Int = 0
    
    
    //MARK:- Clear rooms data
    static func clearRoomsData(){
        selectedOption = .CheckIn
        selectedRoomIndex = 0
        lastSelectedIndex = 0
        selectedRoomIndexes.removeAll()
        selectedTransfers.removeAll()
    }
    
    //MARK:- Create details Model
    static func createDetailsModel(result_dict:[String:Any]){
        
        if let dataDict = result_dict["data"] as? [String:Any]{
            
            self.search_hash = "\(dataDict["search_hash"] ?? "")"

            if let hotel_info = dataDict["HotelInfoResult"] as?  [String:Any]{
                
                self.trace_id = "\(hotel_info["TraceId"] ?? "")"
                
                if let custom = hotel_info["Custom"] as?  [String:Any]{
                    self.token = "\(custom["token"] ?? "")"
                    self.token_key = "\(custom["token_key"] ?? "")"
                }
                
                if let details = hotel_info["HotelDetails"] as?  [String:Any]{
                    let data = HotelDetailsModel.init(data: details)
                    HotelDetailsViewModel.hotelDetails = data
                }
                
            }
            
            if let array = dataDict["transfer_details"] as? [[String:Any]],array.count != 0{
                HotelDetailsViewModel.hotelTransfers = HotelTransfersModel.createModels(array: array)
            }
        }
        
    }
    
    //MARK:- Create Rooms Model
    static func createRoomsModel(result_dict:[String:Any]){
        
        if let dataArray = result_dict["data"] as? [[String:Any]],dataArray.count != 0{
            HotelDetailsViewModel.hotelRoomsList = HotelRoomsModel.createModels(array: dataArray)
        }
        
    }
    
    //MARK:- Select/Deselect transfer
    static func selectTransfers(index:Int){
        
        let data = HotelDetailsViewModel.hotelTransfers[index]
       
        if HotelDetailsViewModel.selectedTransfers.contains(data.id){
            if let index = HotelDetailsViewModel.selectedTransfers.firstIndex(of: data.id){
                HotelDetailsViewModel.selectedTransfers.remove(at: index)
            }
        }else{
            HotelDetailsViewModel.selectedTransfers.append(data.id)
        }
        
    }
    
    //MARK:- Get Room options count
    static func getRoomOptionsCount() -> Int{
        var count:Int = 0
        
        let data = self.hotelRoomsList[self.selectedRoomIndex]
        
        switch self.selectedOption {
        case .CheckIn:
            count = data.datesList.count
        case .CheckOut:
            count = data.datesList.count
        case .MealType:
            count = data.mealPlans.count
        }
        return count
    }
    
    //MARK:- Update Room data
    static func updateRoomData(result:[String:Any]){
        
        if let dict = result["data"] as? [String:Any]{
            let model = RoomPriceModel.init(dict: dict)
            var data = self.hotelRoomsList[self.selectedRoomIndex]
            data.token_key = model.token_key
            data.token = model.token
            data.roomPrice = model.room_price
            self.hotelRoomsList[self.selectedRoomIndex] = data
        }
        
    }
    
    //MARK:- Update room selection data
    static func updateRoomSelectionData(index:Int){
        
        var data = self.hotelRoomsList[self.selectedRoomIndex]
        
        switch self.selectedOption {
        case .CheckIn:
            let datesList = data.datesList
            
            if datesList.count != 0 && datesList.count > index{
                data.selected_check_in = datesList[index]
            }
        case .CheckOut:
            let datesList = data.datesList
            
            if datesList.count != 0 && datesList.count > index{
                data.selected_check_out = datesList[index]
            }
        case .MealType:
            let mealPlans = data.mealPlans
            
            if mealPlans.count != 0 && mealPlans.count > index{
                data.selected_meal_plan_id = mealPlans[index].origin
                data.selected_meal_plan = mealPlans[index].meal_plan

            }
        }
        
        self.hotelRoomsList[self.selectedRoomIndex] = data
        
    }
    
    //MARK:- Reset rooms data
    static func resetRoomsData(){
        self.clearRoomsData()
        
        for i in 0..<self.hotelRoomsList.count{
            var model = self.hotelRoomsList[i]
            model.selected_check_in = ""
            model.selected_check_out = ""
            model.selected_meal_plan = ""
            self.hotelRoomsList[i] = model
        }
        
    }
    
    //MARK:- Validate Rooms data
   static func validateRooms() -> Bool{
        
        if HotelDetailsViewModel.selectedRoomIndexes.count == 0{
            return false
        }else{
            for index in HotelDetailsViewModel.selectedRoomIndexes{
                
                let data = HotelDetailsViewModel.hotelRoomsList[index]
                
                if data.selected_check_in.count == 0 || data.selected_check_out.count == 0 {
                    return false
                }
                
            }
        }
        
        return true
    }
    
    //MARK:- Validate details for room block
    static func validateDetailsForRoomBlock()-> Bool{
       
        if !self.validateRooms(){
            appDelegate.window?.makeToast(message: "Please select your room dates")
            return false
        }
        if HotelDetailsViewModel.hotelRoomsList.count != 0{
            let data = HotelDetailsViewModel.hotelRoomsList[0]
            
            if lastSelectedIndex != data.datesList.count-1{
                appDelegate.window?.makeToast(message: "Please select all dates")
                return false
            }
        }
       
        if self.selectedTransfers.count == 0{
            appDelegate.window?.makeToast(message: "Please select Transfers")
            return false
        }
        
        return true
    }
    
    //MARK:- Validate date
    static func validateDate(index:Int) -> Bool{
        
        if selectedOption == .CheckIn{
         
            if self.lastSelectedIndex == index{
                self.lastSelectedIndex = index
                return true
            }else{
                appDelegate.window?.makeToast(message: "Please select date in order")
                return false
            }
            
        }
        
        if selectedOption == .CheckOut{
            
            if self.lastSelectedIndex <= index{
                self.lastSelectedIndex = index
                
                let data = self.hotelRoomsList[self.selectedRoomIndex]
                
                let sel_date = data.datesList[index]
                
                if data.selected_check_in != sel_date{
                    return true
                }else{
                    appDelegate.window?.makeToast(message: "Check In and Check Out dates should not be same")
                    return false
                }
                
            }
          
        }
      
        return true
    }
    
    //MARK:- Get total price of rooms
    static func getTotalPriceOfRooms()-> Float{
        var total_price:Float = 0
        
        if HotelDetailsViewModel.selectedRoomIndexes.count != 0{
            for index in HotelDetailsViewModel.selectedRoomIndexes{
                
                let data = HotelDetailsViewModel.hotelRoomsList[index]
                total_price = total_price+data.roomPrice
            }
        }
        
        return total_price
    }
    
    
}


extension HotelDetailsViewModel{
    
    //MARK:- API's
    
    static func getHotelDetailsAPIConnection(handler:@escaping UniqueHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        let data = HotelSearchViewModel.filter_searchResults[HotelSearchViewModel.selectedIndex]
        var params = [String: String]()
        params["HotelCode"] = data.hotel_code
        params["op"] = "get_details"
        params["search_id"] = HotelSearchViewModel.search_id
        params["booking_source"] = HotelSearchViewModel.booking_source
        params["ResultIndex"] = data.result_token
        params["search_hash"] = data.search_hash
        params["user_type"] = UserViewModel.userData?.user_type
        params["user_id"] = UserViewModel.userData?.user_id
        
        var finalParams:[String: String] = [:]
        finalParams["hotel_details"] = VKAPIs.getJSONString(object: params)
        
        // calling apis...
        VKAPIs.shared.getRequestXwwwform(params: finalParams, file: kHotelDetails, httpMethod: .POST)
        { (resultObj, success, error,data) in
            
            // success status...
            if success == true {
                print("Get Hotel Details response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        
                        self.createDetailsModel(result_dict: result)
                        handler()
                        
                    }else{
                        appDelegate.window?.makeToast(message: model.message)
                    }
                    
                } else {
                    print("Get Hotel Details formate : \(String(describing: resultObj))")
                }
            } else {
                
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
                
            }
            
            DialougeUtils.removeActivityView(view: appDelegate.window!)
            
        }
    }
    
    static func getRoomsListAPIConnection(handler:@escaping UniqueHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        self.clearRoomsData()
        
        var params = [String: String]()
        params["HotelCode"] = HotelDetailsViewModel.hotelDetails?.hotel_code
        params["op"] = "get_room_details"
        params["search_id"] = HotelSearchViewModel.search_id
        params["booking_source"] = HotelSearchViewModel.booking_source
        params["ResultIndex"] = HotelDetailsViewModel.hotelDetails?.hotel_code
        params["TraceId"] = HotelDetailsViewModel.hotelDetails?.hotel_code
        params["search_hash"] = HotelDetailsViewModel.search_hash

        let paramString: [String: String] = ["room_list": VKAPIs.getJSONString(object: params),"user_type":UserViewModel.userData?.user_type ?? "","user_id":UserViewModel.userData?.user_id ?? ""]
        
        // calling apis...
        VKAPIs.shared.getRequestXwwwform(params: paramString, file: kGetHotelRoomList, httpMethod: .POST)
        { (resultObj, success, error,data) in
            
            // success status...
            if success == true {
                print("Get Rooms list response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                       
                        self.createRoomsModel(result_dict: result)
                        
                        handler()
                        
                    }else{
                        appDelegate.window?.makeToast(message: model.message)
                    }
                    
                } else {
                    print("Get Hotel Details formate : \(String(describing: resultObj))")
                }
            } else {
                
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
                
            }
            
            DialougeUtils.removeActivityView(view: appDelegate.window!)
            
        }
    }
    static func getRoomPriceAPIConnection(handler:@escaping UniqueHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        let data = self.hotelRoomsList[self.selectedRoomIndex]
        
        var params = [String: Any]()
        params["hotel_id"] = data.hotel_code
        params["Check_in"] = data.selected_check_in
        params["Check_out"] = data.selected_check_out
        params["room_id"] = data.roomId
        params["Meal_plan"] = data.selected_meal_plan_id
        
        var token = data.token
        token = token.replacingOccurrences(of: "+", with: "|||")
        
        params["token"] = token
        params["token_key"] = data.token_key
        params["search_id"] = data.search_id
        
        let parameters: [String: String] = ["room_list": VKAPIs.getJSONString(object: params),"user_type":UserViewModel.userData?.user_type ?? "","user_id":UserViewModel.userData?.user_id ?? ""]
        
        // calling apis...
        VKAPIs.shared.getRequestXwwwform(params: parameters, file: kGetRoomPrice, httpMethod: .POST)
        { (resultObj, success, error,data) in
            
            // success status...
            if success == true {
                print("Get Rooms price response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        self.updateRoomData(result: result)
                        handler()
                        
                    }else{
                        appDelegate.window?.makeToast(message: model.message)
                    }
                    
                } else {
                    print("Get Hotel Details formate : \(String(describing: resultObj))")
                }
            } else {
                
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
                
            }
            
            DialougeUtils.removeActivityView(view: appDelegate.window!)
            
        }
    }
    
}
