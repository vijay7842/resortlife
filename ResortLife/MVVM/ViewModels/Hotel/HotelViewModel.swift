//
//  HotelViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 19/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

// MARK:- HotelViewModel
class HotelViewModel {

    //MARK:- Variables
    static var details = HotelModel()

    static var checkInOrOutDate:Int = 0 // 1-CheckIn, 2- CheckOut

    static var totalAdults:Int = 1
    static var totalChildren:Int = 0
   
    //MARK:- selected city
    static func selectedCity_info(cityInfo: [String : String]){
        
        HotelViewModel.details.cityId = cityInfo["city_id"] ?? ""
        HotelViewModel.details.cityName = cityInfo["city_name"] ?? ""
    }
    
    //MARK:- Setup initial data
    static func updateInitialData(){
        
        if HotelSearchViewModel.guestRoomsList.count == 0{
            HotelSearchViewModel.createModels()
        }
        
        let tomorrowDate = HomeViewModel.currentDate.getDate(dayDifference: 1)
        
        HotelViewModel.details.checkInDate = HomeViewModel.currentDate
        HotelViewModel.details.checkIn_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: HomeViewModel.currentDate)
        
        HotelViewModel.details.checkOutDate = tomorrowDate
        HotelViewModel.details.checkOut_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: tomorrowDate)
    }
    
    //MARK:- Setup CheckIn and Checkout dates
    
    static func updateSelectedDate() -> String{
        
        if HotelViewModel.checkInOrOutDate == 1 {
            HotelViewModel.details.checkInDate = HomeViewModel.calSelectedDate
            HotelViewModel.details.checkIn_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: HomeViewModel.calSelectedDate)

                let nextDay = HomeViewModel.calSelectedDate.getDate(dayDifference: 1)
                HotelViewModel.details.checkOutDate = nextDay
                HotelViewModel.details.checkOut_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: nextDay)
            
        }
        else if HotelViewModel.checkInOrOutDate == 2{
            
            let loDepartDate = HotelViewModel.details.checkInDate
            let loCalDate = HomeViewModel.calSelectedDate
            
            if (loDepartDate.compare(loCalDate) == .orderedDescending)  {
                return "Please select return date greater than depart date !"
            } else {
                
                let days = DateFormatter.getDaysBetweenTwoDates(startDate: HotelViewModel.details.checkInDate, endDate: HomeViewModel.calSelectedDate)
                HotelViewModel.details.noOf_nights = days
                
                HotelViewModel.details.checkOutDate = HomeViewModel.calSelectedDate
                HotelViewModel.details.checkOut_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: HomeViewModel.calSelectedDate)
            }
        }
        
        return ""
    }
    //MARK:- Get Guest rooms
    static func getGuestRoomsString() -> String{
        
        var adults:Int = 0
        var children:Int = 0
        
        for model in HotelSearchViewModel.guestRoomsList{
            
            adults = adults+model.adultsCount
            children = children+model.childrenCount
        }
        
        let adultsString = (adults == 1) ? "Adult" : "Adults"
        let childString = (children == 0 || children == 1) ? "Child" : "Children"
        
        let guestRooms = "\(adults) \(adultsString), \(children) \(childString) "
        
        totalAdults = adults
        totalChildren = children

        return guestRooms
    }
    
    //MARK:- Get Total guests

    static func getTotalGuests()->String{
        var adults:Int = 0
        var children:Int = 0
        
        for model in HotelSearchViewModel.guestRoomsList{
            
            adults = adults+model.adultsCount
            children = children+model.childrenCount
        }
        totalAdults = adults
        totalChildren = children
        
        let totalGuests = adults+children
       
        let guests = "\(totalGuests)"
        
        return guests

    }
    
    //MARK:- Get Header date
    static func getHeaderDate() -> String{
        
        let checkIn = DateFormatter.getDateString(formate: "dd MMM", date: HotelViewModel.details.checkInDate)
       
        let checkOut = DateFormatter.getDateString(formate: "dd MMM", date: HotelViewModel.details.checkOutDate)
        
        let dateString = "\(checkIn) - \(checkOut)"
        
        return dateString
    }
    
    //MARK:- Get Header date
    static func getSubTitle() -> String{
        
        let dateString = HotelViewModel.getHeaderDate()
        let guestsString = HotelViewModel.getGuestRoomsString()
        let roomString = (HotelSearchViewModel.guestRoomsList.count == 1) ? "Room" : "Rooms"

        let roomsString = "\(HotelSearchViewModel.guestRoomsList.count) \(roomString)"
        let subTitle = "\(dateString), \(guestsString), \(roomsString)"
        
        return subTitle
    }
    
}
