//
//  HotelSearchViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation


class HotelSearchViewModel {
    
    //MARK:- Variables

    static var is_domestic = true

    static var guestRoomsList: [GuestsRoomsModel] = []
   
    static var searchResults: [HotelSearchResultModel] = []
    static var filter_searchResults: [HotelSearchResultModel] = []

    static var selectedIndex:Int = -1

    static var booking_source:String = ""
    static var search_id:String = ""
    
    //MARK:- Clear search data
    static func clearSearchData(){
        searchResults.removeAll()
        filter_searchResults.removeAll()
    }
    
    //MARK:- Create child Models
    static func createModels(){
        var model = GuestsRoomsModel()
        model.adultSelIndex = 0
        model.childSelIndex = 0
        HotelSearchViewModel.guestRoomsList.append(model)
    }
    
    //MARK:- Update model
    static func updateModel(index:Int,roomIndex:Int){
        
        var model = HotelSearchViewModel.guestRoomsList[roomIndex]
       
        if index != model.childsList.count{
            model.childsList.removeAll()
            let childModel = ChildsModel()
            for _ in 0 ..< index{
                model.childsList.append(childModel)
            }
            HotelSearchViewModel.guestRoomsList[roomIndex] = model
        }
      
    }
    
    //MARK:- Create Hotel Models
    static func createHotelModels(result_dict:[String:Any]){
        
        if let dict1 = result_dict["data"] as? [String:Any],let dict2 = dict1["HotelSearchResult"] as? [String:Any]{
                       
            if let hotelsArray = dict2["HotelResults"] as? [[String:Any]],hotelsArray.count != 0 {
                
                let array = HotelSearchResultModel.createModels(array:hotelsArray)
                HotelSearchViewModel.searchResults = array
                HotelSearchViewModel.filter_searchResults = HotelSearchViewModel.searchResults
                
            }
        }
    }

}

extension HotelSearchViewModel{
   // MARK:-  API's
    
    static func getHotelSearchList(handler:@escaping UniqueHandler) {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        var params = [String:Any]()
        params["NoOfNights"] = "\(HotelViewModel.details.noOf_nights)"
        params["NoOfRooms"] = "\(HotelSearchViewModel.guestRoomsList.count)"
        params["CityId"] = "\(HotelViewModel.details.cityId)"
        params["CheckInDate"] = "\(HotelViewModel.details.checkIn_date)"
        params["CheckOutDate"] = "\(HotelViewModel.details.checkOut_date)"
        
        var roomList = [[String:Any]]()
        
        for model in HotelSearchViewModel.guestRoomsList{
            
            var rooms = [String:Any]()
            rooms["NoOfAdults"] = "\(model.adultsCount)"
            rooms["NoOfChild"] = "\(model.childrenCount)"
            
            var agesList = [String]()
            if model.childrenCount>0{
                for childModel in model.childsList{
                    agesList.append(childModel.age)
                }
            }
            rooms["ChildAge_1"] = agesList
            roomList.append(rooms)
        }
        
        params["RoomGuests"] = roomList
        
        print("params: \(params)")
        
        var finalParams: [String: String] = [:]
        finalParams["hotel_search"] = VKAPIs.getJSONString(object: params)
        finalParams["user_type"] = UserViewModel.userData?.user_type
        finalParams["user_id"] = UserViewModel.userData?.user_id
        
        // calling apis...
        VKAPIs.shared.getRequestXwwwform(params: finalParams, file: kHotelSearch, httpMethod: .POST)
        { (resultObj, success, error,data) in
            
            // success status...
            if success == true {
                print("Hotel search success: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        
                        HotelSearchViewModel.booking_source = "\(result["booking_source"] ?? "")"
                        HotelSearchViewModel.search_id = "\(result["search_id"] ?? "")"
                        
                        self.createHotelModels(result_dict: result)
                        
                        handler()
                        
                    }else{
                        appDelegate.window?.makeToast(message: model.message)
                    }
                    
                } else {
                    print("Hotel search formate : \(String(describing: resultObj))")
                }
            } else {
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
        }
    }
  
    
}
