//
//  HotelReviewViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 07/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HotelReviewViewModel {
    
    //MARK:- Variables
    static var pre_booking_details:HotelPrebookingModel?

    static var flight_arrival = FlightInfoModel()
    static var flight_departure = FlightInfoModel()
    static var notes = ""
    static var address = ""
    static var mobile = ""
    static var email = ""
    
    
    //MARK:- Clear data
    static func clearData(){
        flight_arrival = FlightInfoModel()
        flight_departure = FlightInfoModel()
        notes = ""
        address = ""
        mobile = ""
        email = ""
    }
    
    //MARK:- get Amounts
    static func getPriceAmounts() -> (Float,Float,Float){
        
        var totalPriceToDisplay:Float = 0
        let totalPrice:Float = HotelReviewViewModel.pre_booking_details?.total_price ?? 0
        let convenienceFee:Float = HotelReviewViewModel.pre_booking_details?.convenience_fees ?? 0
        totalPriceToDisplay = totalPrice+convenienceFee
        return(totalPrice,convenienceFee,totalPriceToDisplay)
        
    }
    
    //MARK:- Show Total Amount
    static func showTotalAmount()->String{
        let currency = HomeViewModel.getCurrencySymbol(currency_code:HotelReviewViewModel.pre_booking_details?.default_currency ?? "")
        let amountString = String(format:"%@ %.2f",currency,self.getPriceAmounts().2)
        return amountString
    }
    
    //MARK:- Get price details
    static func getPriceDetails()->([String],[Float]){
        
        var totalPrice:Float = 0
        var basePrice:Float = 0

        totalPrice = HotelReviewViewModel.pre_booking_details?.total_price ?? 0
        let convenienceFee:Float = HotelReviewViewModel.pre_booking_details?.convenience_fees ?? 0
        let taxes:Float = HotelReviewViewModel.pre_booking_details?.tax_service_sum
            ?? 0
        
        basePrice = totalPrice-taxes
        
        var titlesList = [String]()
        var amountsList = [Float]()
        
        titlesList.append("Base fare")
        titlesList.append("Taxes")
        titlesList.append("Conveniece fee")
        
        amountsList.append(basePrice)
        amountsList.append(taxes)
        amountsList.append(convenienceFee)
        
        return (titlesList,amountsList)
        
    }
    
    //MARK:- Get Room Block parameters
    static func getRoomBlockParameters() -> [String:String]{
        
        var details = [String: Any] ()
        
        details["search_id"] = HotelSearchViewModel.search_id
        details["PropertyTypeId"] = HotelDetailsViewModel.hotelDetails?.property_type_id
        //  details["CancellationPolicy"] = HotelDetailsViewModel.hotelDetails?.cancellation_policy
        
        let hotel_data = HotelSearchViewModel.filter_searchResults[HotelSearchViewModel.selectedIndex]
        details["HotelCode"] = hotel_data.hotel_code
        details["ResultIndex"] = hotel_data.hotel_code
        details["TraceId"] =  hotel_data.hotel_code
        details["HotelName"] = hotel_data.hotel_name
        details["StarRating"] = hotel_data.star_rating
        details["HotelAddress"] = hotel_data.address
        details["HotelImage"] = "\(Server.image_url)\(hotel_data.hotel_picture)"
        details["search_hash"] = hotel_data.search_hash
        
        
        var tokenArray = [String]()
        var tokenKeyArray = [String]()
        var transferTokenArray = [String]()
        var transferTokenKeyArray = [String]()
        var roomIdArray = [String]()
        var checkInArray = [String]()
        var checkOutArray = [String]()
        var mealPlanArray = [String]()
        
        if HotelDetailsViewModel.selectedRoomIndexes.count != 0{
            for index in HotelDetailsViewModel.selectedRoomIndexes{
                
                let data = HotelDetailsViewModel.hotelRoomsList[index]
              
                var token = data.token
                token = token.replacingOccurrences(of: "+", with: "|||")
                
                tokenArray.append(token)
                tokenKeyArray.append(data.token_key)
                roomIdArray.append(data.roomId)
                checkInArray.append(data.selected_check_in)
                checkOutArray.append(data.selected_check_out)
              mealPlanArray.append(data.selected_meal_plan_id)
            }
        }
        
        if HotelDetailsViewModel.selectedTransfers.count != 0{
            for model in HotelDetailsViewModel.hotelTransfers {
                if HotelDetailsViewModel.selectedTransfers.contains(model.id) {
                    
                    var transfer_token = model.token
                    transfer_token = transfer_token.replacingOccurrences(of: "+", with: "|||")
                    
                    transferTokenArray.append(transfer_token)
                    transferTokenKeyArray.append(model.token_key)
                }
            }
        }
        
        var params = [String: String] ()
        params["details"] = VKAPIs.getJSONString(object: details)
        params["user_type"] = UserViewModel.userData?.user_type
        params["user_id"] = UserViewModel.userData?.user_id
        params["token"] = VKAPIs.getJSONString(object: tokenArray)
        params["TokenId"] = VKAPIs.getJSONString(object: tokenKeyArray)
        params["transfer_token"] = VKAPIs.getJSONString(object: transferTokenArray)
        params["transfer_token_key"] = VKAPIs.getJSONString(object: transferTokenKeyArray)
        params["room_id"] = VKAPIs.getJSONString(object: roomIdArray)
        params["check_in_date"] = VKAPIs.getJSONString(object: checkInArray)
        params["check_out_date"] = VKAPIs.getJSONString(object: checkOutArray)
        params["meal_plan"] = VKAPIs.getJSONString(object: mealPlanArray)
        
        return params
    }
    
}

extension HotelReviewViewModel{
    //MARK:- API's
    
    static func hotelRoomBlockAPICall(handler:@escaping UniqueHandler) {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
       
        self.clearData()
        
        let parameters: [String: String] = self.getRoomBlockParameters()

        // calling apis...
        VKAPIs.shared.getRequestXwwwform(params: parameters, file: kHotelRoomBlock, httpMethod: .POST)
        { (resultObj, success, error,data) in
            
            // success status...
            if success == true {
                print("Hotel Room Block success: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        
                        if let dataDict = result["data"] as? [String:Any]{
                            self.pre_booking_details = HotelPrebookingModel.init(details: dataDict)
                        }
                        
                        handler()
                    }else{
                        appDelegate.window?.makeToast(message: model.message)
                    }
                    
                    
                } else {
                    print("Flight search formate : \(String(describing: resultObj))")
                }
            } else {
                
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
                
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
            
        }
    }
    
}
