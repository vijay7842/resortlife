//
//  FilterSortViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 03/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

class FilterSortViewModel{
    
    //MARK:- Variables
    
    static var price_default: (Float, Float) = (0, 1)
    static var price_selection: (Float, Float) = (0, 1)
    
    static var amenities = [false,false,false,false]

    static var selectedRating:Int = 0
   
    static var selectedLocations = [String]()
    static var locationNames = [String]()
    
    static var sort_number = -1

    //MARK:- Clear fitlers
    static func clearAllFilters(){
        
        //Filters
        price_default = (0, 1)
        price_selection = (0, 1)
        
        selectedRating = 0
        
        amenities = [false,false,false,false]
        selectedLocations.removeAll()
        
        //Sorting
        sort_number = -1

    }
    
    //MARK:- Get Locations
    static func removeDuplicates(array: [String]) -> [String] {
        
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                result.append(value)
            }
        }
        return result
    }
    
    //MARK:- Get Min and Max values
    static func getMinAndMaxValue(price_array: [Float]) -> (Float, Float) {
        
        var minValue: Float = 0.0
        var maxValue: Float = 1.0
        
        // first element
        if price_array.count != 0 {
            minValue = price_array[0]
            maxValue = price_array[0]
        }
        
        // final loops...
        for i in 0 ..< price_array.count {
            
            let price = price_array[i]
            if minValue > price {
                minValue = price
            }
            
            if maxValue < price {
                maxValue = price
            }
        }
        return (minValue, maxValue)
    }
    
    //MARK:- Get Locations
    static func getHotelLocations(){
       
        var locations =  HotelSearchViewModel.searchResults.map({$0.location})
         locations = self.removeDuplicates(array: locations)
        FilterSortViewModel.locationNames = locations
        
        getMinMaxPrice()
        
    }
    
    //MARK:- Get Min and max price
    static func getMinMaxPrice(){
        
        let priceList =  HotelSearchViewModel.searchResults.map({$0.room_price})

        FilterSortViewModel.price_default = self.getMinAndMaxValue(price_array: priceList)
        FilterSortViewModel.price_selection = self.getMinAndMaxValue(price_array: priceList)
        
    }
    
    //MARK:- Apply Filters
    static func applyAllFiltersAndSorting(hotelList: [HotelSearchResultModel]) -> [HotelSearchResultModel] {
        
        print("hotelList count \(hotelList.count)")

        // price filters...
        let priceFilter = hotelList.filter { ($0.room_price >= FilterSortViewModel.price_selection.0 && $0.room_price <= FilterSortViewModel.price_selection.1)}
        print("priceFilter count \(priceFilter.count)")
        
        //Rating filter
        let ratingFilter = rating_Filter(hotelList: priceFilter)
        
        print("ratingFilter count \(ratingFilter.count)")

        //Location filter
        let locationFilter = location_Filter(hotelList: ratingFilter)

        print("locationFilter count \(locationFilter.count)")

        //Sorting
        let sortingFilter = sorting_hotels(hotelList: locationFilter)
       
        print("sortingFilter count \(sortingFilter.count)")

        return sortingFilter
    }
    
    //MARK:- Rating Filter
    static func rating_Filter(hotelList: [HotelSearchResultModel]) -> [HotelSearchResultModel]{
        
        var hotel_array = hotelList
        
        if FilterSortViewModel.selectedRating != 0{
            let rating = "\(FilterSortViewModel.selectedRating)"
            hotel_array = hotel_array.filter({$0.star_rating == rating})
        }
        
        return hotel_array
    }
    
    //MARK:- Location Fitler
    static func location_Filter(hotelList: [HotelSearchResultModel]) -> [HotelSearchResultModel]{
        
        var hotel_array: [HotelSearchResultModel] = []
        
        if self.selectedLocations.count != 0{
            for model in hotelList {
                if self.selectedLocations.contains(model.location) {
                    hotel_array.append(model)
                }
            }
        }else{
            hotel_array = hotelList
        }
        
        return hotel_array
    }
    
    // MARK:- Sorting
    static func sorting_hotels(hotelList: [HotelSearchResultModel]) -> [HotelSearchResultModel] {
        
        var hotel_array = hotelList
        
        // Price sorting...
        if FilterSortViewModel.sort_number == 0 {
            
            let results_1 = hotel_array.sorted(by: { $0.room_price < $1.room_price })
            hotel_array = results_1
            
        }
        else if FilterSortViewModel.sort_number == 1 {
            
            let results_1 = hotel_array.sorted(by: { $0.room_price > $1.room_price })
            hotel_array = results_1
          
        }
         
        // Rating sorting...

        else if FilterSortViewModel.sort_number == 2 {
            
            let results_1 = hotel_array.sorted(by: { $0.rating < $1.rating })
            hotel_array = results_1
          
        }
        else if FilterSortViewModel.sort_number == 3 {
            
            let results_1 = hotel_array.sorted(by: { $0.rating > $1.rating })
            hotel_array = results_1
          
        }
           
        // Name sorting...

        else if FilterSortViewModel.sort_number == 4 {
       
            let results_1 = hotel_array.sorted { $0.hotel_name.localizedCaseInsensitiveCompare($1.hotel_name) == .orderedAscending }
            hotel_array = results_1

        }
        else if FilterSortViewModel.sort_number == 5 {
         
            let results_1 = hotel_array.sorted { $0.hotel_name.localizedCaseInsensitiveCompare($1.hotel_name) == .orderedDescending }
            hotel_array = results_1
        }
          
        else {
            print("There is no Hotel sorting")
        }
        
        return hotel_array
    }
    
    
}
