//
//  HotelBookingViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 08/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HotelBookingViewModel{
    
    //MARK:- Variables
    static var booking_url:String = ""
    
    
    //MARK:- Get booking parameters
    static func getBookingParameters() -> [String:String]{
        
        var hotel_params = [String: Any] ()
        hotel_params["user_type"] = UserViewModel.userData?.user_type
        hotel_params["user_id"] = UserViewModel.userData?.user_id
        hotel_params["PinCode"] = "560100"
        hotel_params["AddressLine1"] = HotelReviewViewModel.address
        hotel_params["countryname"] = "India"
        hotel_params["countrycode"] = "91"
      
        hotel_params["Email"] = HotelReviewViewModel.email
        hotel_params["ContactNo"] = HotelReviewViewModel.mobile
        hotel_params["payment_method"] = HotelReviewViewModel.pre_booking_details?.payment_method
        hotel_params["search_id"] = HotelSearchViewModel.search_id
        hotel_params["currency_symbol"] = "USD"
        hotel_params["currency"] = "USD"
        hotel_params["City"] = "Bengaluru"
        hotel_params["PropertyTypeId"] = HotelDetailsViewModel.hotelDetails?.property_type_id

        var passengersList = [[String:Any]]()
        
        for model in PassengersViewModel.passengersList{
            var passengers = [String:Any]()
            passengers["FirstName"] = model.firstName
            passengers["LastName"] = model.lastName
            passengers["Title"] = model.title
            passengers["PassportNumber"] = model.passportNumber
            passengers["PassportExpiry"] = model.passportExpDate
            passengers["DateOfBirth"] = model.dateOfBirth
            passengers["PassportIssueCountry"] = model.passportCountryName
            
            if model.passengerType == "1"{
                passengers["passenger_type"] = "Adult"
                passengers["lead_passenger"] = "true"
            }else if model.passengerType == "2"{
                passengers["passenger_type"] = "Child"
                passengers["lead_passenger"] = "false"
            }else{
                passengers["passenger_type"] = "Infant"
                passengers["lead_passenger"] = "false"
            }
            
            if model.title == "1"{
                passengers["Gender"] = "1"
            }else if model.title == "2"{
                passengers["Gender"] = "1"
            }else{
                passengers["Gender"] = "2"
            }
            
            passengersList.append(passengers)
        }
        
        hotel_params["Passengers"] = passengersList
        
        hotel_params["promo_code"] = ""
        hotel_params["promo_code_discount_val"] = "0"
      
        hotel_params["final_fare"] = "\(HotelReviewViewModel.getPriceAmounts().0)"
        hotel_params["convenience_fee"] = "\(HotelReviewViewModel.getPriceAmounts().1)"

        hotel_params["onward_flight_name"] = HotelReviewViewModel.flight_departure.flight_name
        hotel_params["onward_flight_no"] = HotelReviewViewModel.flight_departure.flight_number
        hotel_params["onward_flight_date"] = HotelReviewViewModel.flight_departure.flight_date
        hotel_params["onward_flight_time"] = HotelReviewViewModel.flight_departure.flight_time
        hotel_params["return_flight_name"] = HotelReviewViewModel.flight_arrival.flight_name
        hotel_params["return_flight_no"] = HotelReviewViewModel.flight_arrival.flight_number
        hotel_params["return_flight_date"] = HotelReviewViewModel.flight_arrival.flight_date
        hotel_params["return_flight_time"] = HotelReviewViewModel.flight_arrival.flight_time
        hotel_params["notes"] = HotelReviewViewModel.notes

        var params = [String:String]()
        
        var token = HotelReviewViewModel.pre_booking_details?.pre_booking_params_token
        token = token?.replacingOccurrences(of: "+", with: "|||")
        
        params["Token"] = token
        params["Token_key"] = HotelReviewViewModel.pre_booking_details?.pre_booking_params_token_key
        params["hotel_params"] = VKAPIs.getJSONString(object: hotel_params)
        params["user_type"] = UserViewModel.userData?.user_type
        params["user_id"] = UserViewModel.userData?.user_id

        return params
        
    }
    
}
extension HotelBookingViewModel{
    
    //MARK:- API's

    static func hotelBookingAPICall(handler:@escaping UniqueHandler) {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        let paramString: [String: String] = self.getBookingParameters()

        print("paramString: \(paramString)")
        
        // calling apis...
        VKAPIs.shared.getRequestXwwwform(params: paramString, file: kHotelPreBooking, httpMethod: .POST)
        { (resultObj, success, error,data) in
            
            // success status...
            if success == true {
                print("Hotel booking success: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    let model = ResponseCheckModel.init(dict: result)
                    if model.status {
                        if let dataDict = result["data"] as? [String:Any] {
                            if let return_url = dataDict["return_url"] as? String, return_url != ""{
                                
                                self.booking_url = return_url
                            }
                        }
                        
                        handler()
                    }else{
                        print("model.message : \(String(describing: model.message))")

                        appDelegate.window?.makeToast(message: model.message)
                    }
                    
                } else {
                    print("Hotel search formate : \(String(describing: resultObj))")
                }
            } else {
                
                print("error : \(error?.localizedDescription ?? "")")

                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
                
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
            
        }
    }
    
    
}
