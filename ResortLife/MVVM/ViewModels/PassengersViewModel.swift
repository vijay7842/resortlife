//
//  PassengersViewModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 02/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

// MARK:- PassengersViewModel
class PassengersViewModel {
    
   static var passengersList: [PassengersModel] = []
   
    //MARK:- Clear All
    static func clearAllModels(){
        PassengersViewModel.passengersList.removeAll()
    }
  
    //MARK:- Create Passenger Models

    static func createPassengerModels(){
        var model = PassengersModel()
        model.title = "1"
        model.passportCountryCode = "92"
        model.passportCountryName = "India"

        let expDate = Date().getDate(dayDifference: 1825) // 5 years from now
        let dateString = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: expDate)
      
        for i in 0 ..< HotelViewModel.totalAdults{
            model.passengerType = "1"
            model.displayTitle = "Adult \(i+1)"
          //  model.passportNumber = "\(randomNumberWith(digits:10))"
            model.passportNumber = generateRandomDigits(10)
            model.passportExpDate = dateString
            PassengersViewModel.passengersList.append(model)
        }
        for i in 0 ..< HotelViewModel.totalChildren{
            model.passengerType = "2"
            model.displayTitle = "Child \(i+1)"
         //   model.passportNumber = "\(randomNumberWith(digits:10))"
            model.passportNumber = generateRandomDigits(10)
            model.passportExpDate = dateString
            PassengersViewModel.passengersList.append(model)
        }
        
    }
    
    //MARK:- Create Random Passport numbers
    static func randomNumberWith(digits:Int) -> Int {
        let min = Int(pow(Double(10), Double(digits-1))) - 1
        let max = Int(pow(Double(10), Double(digits))) - 1
        return Int(Range(uncheckedBounds: (min, max)))
    }
    
    static func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }

    
    //MARK:- Validation
    
    static func validateHotelPassengerDetails()->Bool{
        
        for model in PassengersViewModel.passengersList{
            if model.firstName.count == 0 || model.lastName.count == 0{
                return false
            }
            else if !HotelSearchViewModel.is_domestic{
                if model.dateOfBirth.count == 0 || model.passportExpDate.count == 0 || model.passportNumber.count == 0 || model.passportCountryCode.count == 0{
                    return false
                }
            }
            
        }
        return true
    }
}

