//
//  HomeViewModel.swift
//  ResortLife
//
//  Created by Apple retina on 18/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HomeViewModel{
    
    //MARK:- Variables
    static var currentDate = Date()
    static var calSelectedDate = Date()

    static let menuList:[(String,String)] = [("Profile","close_white"),("My Bookings","close_white"),("Change Password","close_white"),("Logout","close_white")]

    static var selectedMenuIndex:Int = -1
    static var menuToggle:Bool = false

    static var balance:String?
    static var credit_limit:String?
    static var due_amount:String?

    //MARK:- Get current date
    static func getCurrentDate(){
        HomeViewModel.currentDate = Date()
    }
    
    
    //MARK:- Get currency symbol
    static func getCurrencySymbol(currency_code:String) -> String{
        
        var currency_Symbol = ""
        
        if currency_code == "USD"{
            currency_Symbol = "$"
        }else if currency_code == "INR"{
            currency_Symbol = "₹"
        }else{
            currency_Symbol = ""
        }
        
        return currency_Symbol
    }
    
}

extension HomeViewModel{
    
    //MARK:- Filters
    
    static func clearAllFilters(){
        
        FilterSortViewModel.clearAllFilters()
        FilterSortViewModel.getHotelLocations()
        
    }
    //Rating
    static func upateRating(rating:Int){
        
        FilterSortViewModel.selectedRating = rating
        
    }
    static func getSelectedRating() -> Int{
        var selectedRating:Int = 0
        
        selectedRating = FilterSortViewModel.selectedRating
        
        return selectedRating
    }
    
    //Amenities
    static func checkAmenities(index:Int) -> Bool{
        
        var checkAmenity:Bool = false
        
        checkAmenity = FilterSortViewModel.amenities[index]
        
        return checkAmenity
        
    }
    static func updateAmenities(index:Int){
        
        FilterSortViewModel.amenities[index] = !FilterSortViewModel.amenities[index]
        
    }
    
    // Check Location
    static func checkLocation(location:String) -> Bool{
        
        var contains:Bool = false
        
            if FilterSortViewModel.selectedLocations.contains(location){
                contains = true
            }
            
        
        return contains
    }
    
    
    // Update location
    static func updateLocation(location:String){
        
            if FilterSortViewModel.selectedLocations.contains(location){
                if let index = FilterSortViewModel.selectedLocations.firstIndex(of: location){
                    FilterSortViewModel.selectedLocations.remove(at: index)
                }
            }else{
                FilterSortViewModel.selectedLocations.append(location)
            }
        
    }
    
    // Get Min Max values
    static func getMinMaxValues()->(Float,Float){
        var minValue:Float = 0
        var maxValue:Float = 1
        
        
        minValue = FilterSortViewModel.price_default.0
        maxValue = FilterSortViewModel.price_default.1
        
        
        return (minValue,maxValue)
        
    }
    
    // Get Selected Min Max values
    static func getSelectedMinMaxValues()->(Float,Float){
        var minValue:Float = 0
        var maxValue:Float = 1
        
        
        minValue = FilterSortViewModel.price_selection.0
        maxValue = FilterSortViewModel.price_selection.1
        
        return (minValue,maxValue)
        
    }
    
    
    // Update Min Max values
    static func updateMinMaxValues(minValue:Float,maxValue:Float){
        
        
        FilterSortViewModel.price_selection.0 = minValue
        FilterSortViewModel.price_selection.1 = maxValue
        
    }
    
    //MARK:- Sorting
    static func updateSortNumber(sortNumber:Int){
        
        FilterSortViewModel.sort_number = sortNumber
        
    }
    static func getSelectedSortNumber()->Int{
        
        var sortNumber:Int = 0
        
        sortNumber = FilterSortViewModel.sort_number
        
        
        return sortNumber
    }
    
    
}


extension HomeViewModel{
    
    //MARK:- API'S
    
    //Get balance
    static func getBalanceAPICall(params: [String: String], handler: @escaping UniqueHandler) -> Void {
        
        DialougeUtils.addActivityView(view: appDelegate.window!)
        
        // calling api...
        VKAPIs.shared.getRequestXwwwform(params: params, file: kGetBalance, httpMethod: .POST)
        { (resultObj, success, error, data) in
            
            // success status...
            if success == true {
                print("Get balance response: \(String(describing: resultObj))")
                
                if let result = resultObj as? [String: Any] {
                    
                    if let status = result["status"] as? Bool,status == true{
                        
                        if let data = result["data"] as? [String:Any]{
                            self.balance = data["balance"] as? String
                            self.credit_limit = data["credit_limit"] as? String
                            self.due_amount = data["due_amount"] as? String
                        }
                        
                        handler()
                        
                    } else {
                        
                        // error message...
                        appDelegate.window?.makeToast(message: result["message"] as? String ?? "")
                    }
                }
                
            }
            else {
                
                print("Login Personal/Professional error : \(String(describing: error?.localizedDescription))")
                appDelegate.window?.makeToast(message: error?.localizedDescription ?? "")
            }
            DialougeUtils.removeActivityView(view: appDelegate.window!)
        }
        
    }
    
}
