//
//  SplashScreenVC.swift
//  ResortLife
//
//  Created by Apple retina on 21/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class SplashScreenVC: UIViewController {
 
    //MARK:- IBOutlets
    @IBOutlet weak var videoContentView:VideoView!

    //MARK:- Variables
    var timer: Timer?

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playVideo()
        runTimer()
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    //MARK:- Play Video
    func playVideo(){
        videoContentView.layoutIfNeeded()
        videoContentView.playVideoFromFilePath(fileName: "app_video1")
        videoContentView.isLoop = true
        videoContentView.play()
    }
    
    
    //MARK:- Run Timer
    func runTimer(){
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false) { timer in
               
                UserViewModel.loadUserDataFromUserDefaults()
                
                if let userId = UserViewModel.userData?.user_id, userId != "" {
                    HandleNavigationScreens.showHomeScreen()
                }else{
                    HandleNavigationScreens.showLoginScreen()
                }
               
            }
        }
        
    }

}
