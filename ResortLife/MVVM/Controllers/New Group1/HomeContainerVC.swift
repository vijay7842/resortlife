//
//  HomeContainerVC.swift
//  ResortLife
//
//  Created by Apple retina on 20/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class HomeContainerVC: UIViewController {

    //MARK:- IBOutelts
    @IBOutlet weak var sideMenuConstraint:NSLayoutConstraint!
    @IBOutlet weak var sideMenuWidthConstraint:NSLayoutConstraint!

    //MARK:- Variables
    var sideMenuWidth:CGFloat = 0
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

      addSideMenu()
        
    }
    
    //MARK:- Add side menu
    func addSideMenu(){
        sideMenuWidth = self.view.frame.width-100
        sideMenuWidthConstraint.constant = sideMenuWidth
        sideMenuConstraint.constant = -(sideMenuWidth)
        
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: kToggleSideMenu, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sideMenuClicked), name: kMenuClicked, object: nil)

    }
    
    //MARK:- Toggle Side Menu
    @objc func toggleSideMenu(){
        
        if HomeViewModel.menuToggle{
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                self.sideMenuConstraint.constant = -self.sideMenuWidth
                self.view.layoutIfNeeded()
            }, completion:nil)
   
            
        }else{

            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                self.sideMenuConstraint.constant = 0
                self.view.layoutIfNeeded()
            }, completion:nil)
        }
        HomeViewModel.menuToggle = !HomeViewModel.menuToggle
        
    }
    
    //MARK:- Show Alert view
    func showLogoutAlertView(){
        
        let alertView = UIAlertController.init(title: "Alert", message: "Are you sure want to logout?", preferredStyle: .alert)
        alertView.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { _ in
            HandleNavigationScreens.logOut()
        }))
        alertView.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
        
    }

}

extension HomeContainerVC{
    //MARK:- IBActions
    @objc func sideMenuClicked(){
        
        toggleSideMenu()

        switch HomeViewModel.selectedMenuIndex {
        case 0:
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "MyBookingsVC") as! MyBookingsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 2:
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            showLogoutAlertView()
        default:
            break
        }
    }
}
