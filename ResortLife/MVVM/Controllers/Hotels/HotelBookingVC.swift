//
//  HotelBookingVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 30/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit
import WebKit

class HotelBookingVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var webView:WKWebView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadUrl()
        
    }
    
    //MARK:- Load url
    func loadUrl(){
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
        
        if let url = URL.init(string: HotelBookingViewModel.booking_url){
            DialougeUtils.addActivityView(view: appDelegate.window!)

            let urlRequest = URLRequest.init(url: url )
            self.webView.load(urlRequest)
        }
        
    }
    
}

extension HotelBookingVC{
  
    //MARK:- IBActions
   
    @IBAction func homeClicked(_ sender:UIButton){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension HotelBookingVC:WKNavigationDelegate,WKUIDelegate{
  
    //MARK:- WebView delegates
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
      
        guard let url = navigationAction.request.url, let urlScheme = url.scheme, let urlHost = url.host else {
            //if we can't convert the URL, deny the action
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        print("url \(url),urlScheme \(urlScheme), urlHost \(urlHost) ")
       
        decisionHandler(WKNavigationActionPolicy.allow)
        
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation
        navigation: WKNavigation!) {
        DialougeUtils.addActivityView(view: appDelegate.window!)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
          DialougeUtils.addActivityView(view: appDelegate.window!)
    }
    func webView(_ webView: WKWebView,
                 didFinish navigation: WKNavigation!){
        print("Loaded: \(String(describing: webView.url))")

        DialougeUtils.removeActivityView(view: appDelegate.window!)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("didFail")
        
        DialougeUtils.removeActivityView(view: appDelegate.window!)
        
    }
    
}

