//
//  HotelDetailsVC.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 12/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class HotelDetailsVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var coll_images:UICollectionView!
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_subTitle:UILabel!
    @IBOutlet weak var lbl_hotelName:UILabel!
    @IBOutlet weak var lbl_location:UILabel!
    @IBOutlet weak var lbl_currency:UILabel!
    @IBOutlet weak var lbl_price:UILabel!
    @IBOutlet weak var lbl_checkInTime:UILabel!
    @IBOutlet weak var lbl_checkInDate:UILabel!
    @IBOutlet weak var lbl_checkOutTime:UILabel!
    @IBOutlet weak var lbl_checkOutDate:UILabel!
    @IBOutlet weak var lbl_nightsCount:UILabel!
    @IBOutlet weak var img_hotel:UIImageView!
    @IBOutlet weak var txv_description:UITextView!
    @IBOutlet weak var txv_complements:UITextView!
    @IBOutlet weak var txv_exclusions:UITextView!

    @IBOutlet var img_stars:[UIImageView]!
    @IBOutlet var img_amenities:[UIImageView]!

    @IBOutlet weak var hei_tblImages: NSLayoutConstraint!
    @IBOutlet weak var hei_descriptionView: NSLayoutConstraint!
    @IBOutlet weak var hei_complementsView: NSLayoutConstraint!
    @IBOutlet weak var hei_exclusionsView: NSLayoutConstraint!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.registerCells()
        self.showInitialData()
        
    }
    
    override func viewDidLayoutSubviews() {

    }
    
    //MARK:- Show Initial data
    func showInitialData(){
        
        lbl_title.text = HotelViewModel.details.cityName
        lbl_subTitle.text = HotelViewModel.getSubTitle()
        lbl_nightsCount.text = "\(HotelViewModel.details.noOf_nights)"

        //Checkin & Checkout dates
        if let checkIn = HotelDetailsViewModel.hotelDetails?.checkIn,checkIn != ""{
            let checkInDate = DateFormatter.changeDateFormatString(fromFormat: "dd-MM-yyyy", date: checkIn, toFormat: "EEE, dd MMM")
            lbl_checkInDate.text = checkInDate
            
            let checkInTime = DateFormatter.changeDateFormatString(fromFormat: "dd-MM-yyyy", date: checkIn, toFormat: "hh:mm a")
            lbl_checkInTime.text = checkInTime
            
        }
        
        if let checkOut = HotelDetailsViewModel.hotelDetails?.checkOut,checkOut != ""{
            let checkOutDate = DateFormatter.changeDateFormatString(fromFormat: "dd-MM-yyyy", date: checkOut, toFormat: "EEE, dd MMM")
            lbl_checkOutDate.text = checkOutDate
            
            let checkOutTime = DateFormatter.changeDateFormatString(fromFormat: "dd-MM-yyyy", date: checkOut, toFormat: "hh:mm a")
            lbl_checkOutTime.text = checkOutTime
            
        }
        
        let data = HotelSearchViewModel.filter_searchResults[HotelSearchViewModel.selectedIndex]
      
        lbl_hotelName.text = data.hotel_name
        lbl_location.text = data.address
        lbl_currency.text = kCurrency
        lbl_price.text = String(format:"%.2f",data.room_price)
        
        if let description = HotelDetailsViewModel.hotelDetails?.description,description.count != 0{
//            txv_description.text = CommonUtilities.convertHtmlToString(htmlString: description)
            txv_description.attributedText = CommonUtilities.convertHtmlStringToAttributedString(htmlString: description)
            txv_description.layoutIfNeeded()
            hei_descriptionView.constant = txv_description.contentSize.height+45
        }else{
            hei_descriptionView.constant = 0
        }
        
        if let complements = HotelDetailsViewModel.hotelDetails?.complements,complements.count != 0{
            txv_complements.attributedText = CommonUtilities.convertHtmlStringToAttributedString(htmlString: complements)
            txv_complements.layoutIfNeeded()
            hei_complementsView.constant = txv_complements.contentSize.height+45
        }else{
            hei_complementsView.constant = 0
        }
        
        if let exclusions = HotelDetailsViewModel.hotelDetails?.exclusion,exclusions.count != 0{
            txv_exclusions.attributedText = CommonUtilities.convertHtmlStringToAttributedString(htmlString: exclusions)
            txv_exclusions.layoutIfNeeded()
            hei_exclusionsView.constant = txv_exclusions.contentSize.height+45
        }else{
            hei_exclusionsView.constant = 0
        }

        //Hotel Image
        let imageUrl = "\(Server.image_url)\(data.hotel_picture)"

        img_hotel.sd_setShowActivityIndicatorView(true)
        img_hotel.sd_setIndicatorStyle(.gray)
        img_hotel.sd_setImage(with: URL.init(string: imageUrl)) { (image, error, cache, urls) in
            self.img_hotel.image = (error != nil) ? hotelPlaceHolder : image
        }

        //Hotel images
        hei_tblImages.constant = (HotelDetailsViewModel.hotelDetails?.images.count == 0) ? 0 : 100
        
        //rating
        let rating = data.star_rating
        
        for imageView in img_stars{
            let tag = "\(imageView.tag)"
            imageView.image = #imageLiteral(resourceName: "starSelected")
            if tag == rating{
                break
            }
        }
        
        //Amenities
        let amenities = data.hotel_amenitites.joined(separator: ",")
        
        for imageView in img_amenities{
            
            imageView.alpha = 0.2
            
            if imageView.tag == 1 && amenities.localizedCaseInsensitiveContains("parking"){
                imageView.alpha = 1.0
            }
            if imageView.tag == 2 && amenities.localizedCaseInsensitiveContains("Wi-fi"){
                imageView.alpha = 1.0
            }
            if imageView.tag == 3 && amenities.localizedCaseInsensitiveContains("breakfast"){
                imageView.alpha = 1.0
            }
            if imageView.tag == 4 && amenities.localizedCaseInsensitiveContains("pool"){
                imageView.alpha = 1.0
            }
        }
        
    }
    
}

extension HotelDetailsVC{
    
    //MARK:- API's
    
    func getRoomsListAPICall(){
        HotelDetailsViewModel.getRoomsListAPIConnection(handler: {
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "SelectRoomVC") as! SelectRoomVC
            self.navigationController?.pushViewController(vc, animated: true)
        })
        
    }
    
    
}

extension HotelDetailsVC{
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func locationClicked(_ sender:UIButton){
        
//       let vc = MainStoryboard.instantiateViewController(withIdentifier: "HotelLocationVC") as! HotelLocationVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func selectRoomClicked(_ sender:UIButton){
       
        getRoomsListAPICall()
        
    }
}


extension HotelDetailsVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func registerCells(){
        coll_images.register(UINib.init(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier:
            "ImagesCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return HotelDetailsViewModel.hotelDetails?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
     
        cell.configureCell(index:indexPath.row)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: 100, height: collectionView.frame.height)
        return size
    }
    
    
}
