//
//  HotelSearchCititesVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 19/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

// protocol
@objc protocol searchHotelCitiesDelegate {
    
    @objc optional func searchCity_info(cityInfo: [String: String])
}

class HotelSearchCititesVC: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var tf_search: UITextField!
    @IBOutlet weak var tbl_search: UITableView!
    
    //MARK:- variables
    var searchMainArray: [[String: String]] = []
    var searchDisplayArray: [[String: String]] = []
    
    var delegate:searchHotelCitiesDelegate?
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.registerCells()
         tf_search.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
         self.gettingDisplayInformation()

    }
       
    // MARK:- Helpers
    func gettingDisplayInformation() {
        
        if HotelCitiesModel.hotelCitiesArray.count == 0 {
            HotelCitiesModel.getHotelCitiesList()
        }
        searchMainArray = HotelCitiesModel.hotelCitiesArray
        searchDisplayArray = searchMainArray
        tbl_search.reloadData()
    }
    
   

}

extension HotelSearchCititesVC{
    
    // MARK:- IBActions
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension HotelSearchCititesVC: UITextFieldDelegate {
    
    // MARK:- UITextField delegates
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField.text?.count != 0 {
            
            // search predicate...
            let predicate: NSPredicate = NSPredicate(format: "city_name contains[c] %@", textField.text!, textField.text!)
            let loArray = (searchMainArray as NSArray).filtered(using: predicate) as NSArray
            searchDisplayArray = loArray as! [[String: String]]
            
        }
        else {
            searchDisplayArray = searchMainArray
        }
        tbl_search.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension HotelSearchCititesVC: UITableViewDataSource, UITableViewDelegate {
    
    func registerCells(){
        tbl_search.register(UINib.init(nibName: "SearchCitiesCell", bundle: nil), forCellReuseIdentifier: "SearchCitiesCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDisplayArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // cell creation...
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCitiesCell") as! SearchCitiesCell
        
        cell.lbl_citiesName.text = searchDisplayArray[indexPath.row]["city_name"] ?? ""
       
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.searchCity_info!(cityInfo: searchDisplayArray[indexPath.row])
        
        self.navigationController?.popViewController(animated: false)
    }
    
}
