//
//  SelectGuestsRoomsVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

protocol GuestRoomsDelegate {
    func guestRoomsSelected()
}

class SelectGuestsRoomsVC: UIViewController {

    // MARK:- IBOutlets
    @IBOutlet weak var tbl_guestRooms:UITableView!

    // MARK:- Variables
    var delegate:GuestRoomsDelegate?
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.registerCells()
        self.showInitialData()
    
        NotificationCenter.default.addObserver(self, selector: #selector(reloadtableView),name: kGuestTblReload, object: nil)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kGuestTblReload, object: nil)
    }
    
    //MARK:- Show data
    func showInitialData(){
        if HotelSearchViewModel.guestRoomsList.count == 0{
            HotelSearchViewModel.createModels()
            tbl_guestRooms.reloadData()
        }
    }
    
    //MARK:- Validate fields
    func validateData()->Bool{
        for model in HotelSearchViewModel.guestRoomsList{
            
            if model.childrenCount>0{
               
                for childModel in model.childsList{
                    if childModel.selectedIndex<0{
                        self.view.makeToast(message: "Please select Child age")
                        return false
                    }
                }
            }
           
        }
        return true
    }

    //MARK:- Reload Tableview
    @objc func reloadtableView() {
        
        print("reloadtableView")
        tbl_guestRooms.reloadData()
        
    }
    
}
extension SelectGuestsRoomsVC{
    
    // MARK:- IBActions
    @IBAction func backButtonClicked(_ sender: UIButton) {
        delegate?.guestRoomsSelected()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addRoomClicked(_ sender: UIButton) {
        
        if HotelSearchViewModel.guestRoomsList.count <= 4{
            HotelSearchViewModel.createModels()
            tbl_guestRooms.reloadData()
            tbl_guestRooms.scrollToRow(at: IndexPath.init(row: HotelSearchViewModel.guestRoomsList.count-1, section: 0), at: .bottom, animated: true)
        }else{
            self.view.makeToast(message: "Reached maximum limit")
        }
        

    }
    @objc func deleteRoomClicked(_ sender:UIButton){
        HotelSearchViewModel.guestRoomsList.remove(at: sender.tag)
        tbl_guestRooms.reloadData()

    }
    
    @IBAction func doneClicked(_ sender: UIButton) {
        if !validateData(){return}
        
        delegate?.guestRoomsSelected()
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SelectGuestsRoomsVC:UITableViewDataSource,UITableViewDelegate{
    
    func registerCells(){
        tbl_guestRooms.register(UINib.init(nibName: "SelectGuestRoomsCell", bundle: nil), forCellReuseIdentifier: "SelectGuestRoomsCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HotelSearchViewModel.guestRoomsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectGuestRoomsCell", for: indexPath) as! SelectGuestRoomsCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        cell.cornerRadius = 10
        
        cell.setUpCellData()
        
        cell.btn_delete.addTarget(self, action: #selector(self.deleteRoomClicked(_:)), for: .touchUpInside)
        
        return cell
    }
   
    
}
