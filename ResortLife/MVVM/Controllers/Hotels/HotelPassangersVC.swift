//
//  HotelPassangersVC.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 12/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class HotelPassangersVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tbl_travellerInfo:UITableView!
    @IBOutlet weak var tbl_flightDetails:UITableView!
    @IBOutlet weak var tbl_passengers:UITableView!
    @IBOutlet weak var tbl_paymentDetails:UITableView!
    
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_subTitle:UILabel!
    @IBOutlet weak var lbl_hotelName:UILabel!
    @IBOutlet weak var lbl_location:UILabel!
    @IBOutlet weak var lbl_description:UILabel!
    @IBOutlet weak var lbl_checkInTime:UILabel!
    @IBOutlet weak var lbl_checkInDate:UILabel!
    @IBOutlet weak var lbl_checkOutTime:UILabel!
    @IBOutlet weak var lbl_checkOutDate:UILabel!
    @IBOutlet weak var lbl_nightsCount:UILabel!
    @IBOutlet weak var img_hotel:UIImageView!
    @IBOutlet weak var txf_mobile:UITextField!
    @IBOutlet weak var txf_email:UITextField!
    @IBOutlet weak var txv_address:UITextView!
    @IBOutlet weak var txv_notes:UITextView!
    @IBOutlet weak var btn_proceedPay:UIButton!
    @IBOutlet weak var lbl_totalAmount:UILabel!

    @IBOutlet weak var reviewDetailsView:UIView!
    @IBOutlet weak var reviewContentView:UIView!
    
    @IBOutlet weak var hei_tblTravellerInfo:NSLayoutConstraint!
    @IBOutlet weak var hei_tblFlightDetails:NSLayoutConstraint!
    @IBOutlet weak var hei_tblPassengers:NSLayoutConstraint!
    @IBOutlet weak var hei_tblPaymentDetails:NSLayoutConstraint!
   
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCells()
        addReviewInfoView()
        displayInformation()

    }
    
    override func viewDidLayoutSubviews() {
       
        hei_tblFlightDetails.constant = tbl_flightDetails.contentSize.height
        hei_tblTravellerInfo.constant = tbl_travellerInfo.contentSize.height
        hei_tblPassengers.constant = tbl_passengers.contentSize.height
        hei_tblPaymentDetails.constant = tbl_paymentDetails.contentSize.height
        reviewContentView.setOnlyTopCornerRadiusWithShadow(30)
    }
    
    //MARK:- Display information
    func displayInformation(){
        
        PassengersViewModel.clearAllModels()
        PassengersViewModel.createPassengerModels()

        tbl_travellerInfo.reloadData()
        
        hei_tblTravellerInfo.constant = tbl_travellerInfo.contentSize.height
        
        lbl_title.text = HotelViewModel.details.cityName
        lbl_subTitle.text = HotelViewModel.getSubTitle()
        lbl_totalAmount.text = HotelReviewViewModel.showTotalAmount()

        lbl_checkInDate.text = DateFormatter.getDateString(formate: "EEE, dd MMM", date: HotelViewModel.details.checkInDate)
        lbl_checkOutDate.text = DateFormatter.getDateString(formate: "EEE, dd MMM", date: HotelViewModel.details.checkOutDate)
        lbl_checkInTime.text = DateFormatter.getDateString(formate: "hh:mm a", date: HotelViewModel.details.checkInDate)
        lbl_checkOutTime.text = DateFormatter.getDateString(formate: "hh:mm a", date: HotelViewModel.details.checkOutDate)
        lbl_nightsCount.text = "\(HotelViewModel.details.noOf_nights)"
        
        let data = HotelSearchViewModel.searchResults[HotelSearchViewModel.selectedIndex]
        lbl_hotelName.text = data.hotel_name
        lbl_description.text = data.hotel_promotion
        lbl_location.text = data.address
        
        let imageUrl = "\(Server.image_url)\(data.hotel_picture)"

        img_hotel.sd_setShowActivityIndicatorView(true)
        img_hotel.sd_setIndicatorStyle(.gray)
        img_hotel.sd_setImage(with: URL.init(string: imageUrl)) { (image, error, cache, urls) in
            self.img_hotel.image = (error != nil) ? hotelPlaceHolder : image
        }
        
        if let address = UserViewModel.userData?.address,address.count != 0{
            txv_address.text = address
            HotelReviewViewModel.address = address
        }
        
    }
   
    //MARK:- Add ReviewInfo View
    
    func addReviewInfoView(){
        
        reviewDetailsView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(reviewDetailsView)
        
        reviewContentView.layoutIfNeeded()
        
    }
    
    
    //MARK:- Validate fields
    func validateFields()->Bool{
        if !PassengersViewModel.validateHotelPassengerDetails(){
            appDelegate.window?.makeToast(message: "Please fill all Passenger details")
            return false
        }
        if txf_mobile.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Mobile number")
            return false
        }
        if !CommonUtilities.validateMobile(value: txf_mobile.text!){
            appDelegate.window?.makeToast(message: "Please enter Valid mobile number")
            return false
        }
        if txf_email.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Email Id")
            return false
        }
        if CommonUtilities.validateEmail(with: txf_email.text!){
            appDelegate.window?.makeToast(message: "Please enter Valid Email Id")
            return false
        }
        
        return true
    }

}
extension HotelPassangersVC{
  
    //MARK:- API's
    func hotelBookingAPICall(){
        HotelBookingViewModel.hotelBookingAPICall(handler: {
            if HotelBookingViewModel.booking_url != ""{
                
                let vc = MainStoryboard.instantiateViewController(withIdentifier: "HotelBookingVC") as! HotelBookingVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        })
    }
    
}

extension HotelPassangersVC:UITextViewDelegate{

    //MARK:- UITextViewDelegates
    func textViewDidChange(_ textView: UITextView) {
        if textView == txv_address && textView.text.count != 0{
                HotelReviewViewModel.address = textView.text
        }
        if textView == txv_notes && textView.text.count != 0{
                HotelReviewViewModel.notes = textView.text
        }
    }
    
}

extension HotelPassangersVC{
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func mobileTextChanges(_ sender:UITextField){
        if sender.text?.count != 0{
            HotelReviewViewModel.mobile = sender.text ?? ""
        }
    }
    @IBAction func emailTextChanges(_ sender:UITextField){
        if sender.text?.count != 0{
            HotelReviewViewModel.email = sender.text ?? ""
        }
    }
   
    @IBAction func continueClicked(_ sender:UIButton){
        
        if !validateFields(){return}
        
        self.view.endEditing(true)
        tbl_passengers.reloadData()
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            
            self.reviewDetailsView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
        
    }
    
    
    @IBAction func payNowClicked(_ sender:UIButton){
        
        hotelBookingAPICall()
        
    }
    
    @IBAction func hideReviewDetailsClicked(_ sender:UIButton){
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            
            self.reviewDetailsView.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
        
    }
    
}


extension HotelPassangersVC:UITableViewDataSource,UITableViewDelegate{
    
    func registerCells(){
        tbl_travellerInfo.register(UINib.init(nibName: "PassengersCell", bundle: nil), forCellReuseIdentifier: "PassengersCell")
        tbl_flightDetails.register(UINib.init(nibName: "FlightDetailsCell", bundle: nil), forCellReuseIdentifier: "FlightDetailsCell")
        tbl_passengers.register(UINib.init(nibName: "PassengerDetailsCell", bundle: nil), forCellReuseIdentifier: "PassengerDetailsCell")
        tbl_paymentDetails.register(UINib.init(nibName: "PaymentDetailsCell", bundle: nil), forCellReuseIdentifier: "PaymentDetailsCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_travellerInfo{
            return PassengersViewModel.passengersList.count
        }else if tableView == tbl_flightDetails{
            return 2
        }else if tableView == tbl_passengers{
            return PassengersViewModel.passengersList.count
        }else{
            return HotelReviewViewModel.getPriceDetails().0.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tbl_travellerInfo{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PassengersCell", for: indexPath) as! PassengersCell
            cell.selectionStyle = .none
            cell.tag = indexPath.row
        cell.loadCellData(data:PassengersViewModel.passengersList[indexPath.row])

            return cell
        }else if tableView == tbl_flightDetails{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FlightDetailsCell", for: indexPath) as! FlightDetailsCell
            cell.selectionStyle = .none
            
            cell.loadCellData(index: indexPath.row)

            return cell
        }else if tableView == tbl_passengers{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PassengerDetailsCell", for: indexPath) as! PassengerDetailsCell
            cell.selectionStyle = .none
            cell.tag = indexPath.row
            cell.loadCellData(data:PassengersViewModel.passengersList[indexPath.row])
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentDetailsCell", for: indexPath) as! PaymentDetailsCell
            cell.selectionStyle = .none
            
            cell.showHotelPriceDetails(index:indexPath.row)
            
            return cell
            
        }
        
    }
    
   
}
