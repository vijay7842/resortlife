//
//  HotelListingVC.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 05/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class HotelListingVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tbl_hotels:UITableView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_subTitle:UILabel!

    //MARK:- Variables
    var filtersVC = FiltersVC()
    var sortingVC = SortingVC()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.registerCells()
        self.showInitialData()

    }
    override func viewDidLayoutSubviews() {
       
        if !Constants.isLoaded{
            Constants.isLoaded = true
            bottomView.setOnlyTopCornerRadiusWithShadow(30)
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Constants.isLoaded = false
    }
    
    //MARK:- Show Initial data
    func showInitialData(){
        lbl_title.text = HotelViewModel.details.cityName
        lbl_subTitle.text = HotelViewModel.getHeaderDate()
        
        FilterSortViewModel.clearAllFilters()
        FilterSortViewModel.getHotelLocations()
        
    }
    
    //MARK:- Add filter view
    func addFilterView(){
        if let vc = MainStoryboard.instantiateViewController(withIdentifier: "FiltersVC") as? FiltersVC{
            filtersVC = vc
        }
        
        filtersVC.view.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        filtersVC.delegate = self
        filtersVC.willMove(toParent: self)
        self.view.addSubview(filtersVC.view)
        self.addChild(filtersVC)
        filtersVC.didMove(toParent: self)
        
        let topPadding = appDelegate.window?.safeAreaInsets.top ?? 0

        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.filtersVC.view.frame = CGRect.init(x: 0, y: topPadding, width: self.view.frame.width, height: self.view.frame.height-topPadding)
            
        }, completion: nil)
        
    }
    
    //MARK:- Add Sort view
    func addSortView(){
        if let vc = MainStoryboard.instantiateViewController(withIdentifier: "SortingVC") as? SortingVC{
            sortingVC = vc
        }
        
        sortingVC.view.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        sortingVC.delegate = self
        sortingVC.willMove(toParent: self)
        self.view.addSubview(sortingVC.view)
        self.addChild(sortingVC)
        sortingVC.didMove(toParent: self)
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.sortingVC.view.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
        
    }

}

extension HotelListingVC{
    //MARK:- API's
    
    func getHotelDetailsAPICall(){
       HotelDetailsViewModel.getHotelDetailsAPIConnection(handler: {
        
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "HotelDetailsVC") as! HotelDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
    }
    
}

extension HotelListingVC:FilterDelegate,SortingDelegate{
   
    //MARK:- Filters delegate
    func applyFilters() {
        
        let array = FilterSortViewModel.applyAllFiltersAndSorting(hotelList: HotelSearchViewModel.searchResults)
        HotelSearchViewModel.filter_searchResults = array
        tbl_hotels.reloadData()
        
    }
    
    //MARK:- Sorting Delegate
    func applySorting() {
        
        let array = FilterSortViewModel.sorting_hotels(hotelList: HotelSearchViewModel.searchResults)
        HotelSearchViewModel.filter_searchResults = array
        tbl_hotels.reloadData()
    }
    
}

extension HotelListingVC{
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterClicked(_ sender:UIButton){
        
       self.addFilterView()
        
    }
    
    @IBAction func sortClicked(_ sender:UIButton){
        
      self.addSortView()
        
    }
    
}

extension HotelListingVC:UITableViewDataSource,UITableViewDelegate{
   
    func registerCells(){
        tbl_hotels.register(UINib.init(nibName: "HotelListingCell", bundle: nil), forCellReuseIdentifier: "HotelListingCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HotelSearchViewModel.filter_searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HotelListingCell", for: indexPath) as! HotelListingCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        cell.setUpCellData(index:indexPath.row)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        HotelSearchViewModel.selectedIndex = indexPath.row
        
        self.getHotelDetailsAPICall()
        
    }
    
}
