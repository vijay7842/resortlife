//
//  SelectRoomVC.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 12/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class SelectRoomVC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var optionsView:UIView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var lbl_title:UILabel!
    @IBOutlet weak var lbl_subTitle:UILabel!
    @IBOutlet weak var lbl_hotelName:UILabel!
    @IBOutlet weak var img_hotel:UIImageView!
    @IBOutlet weak var lbl_location:UILabel!
    @IBOutlet weak var lbl_currency:UILabel!
    @IBOutlet weak var lbl_price:UILabel!
    @IBOutlet weak var lbl_bottom_currency:UILabel!
    @IBOutlet weak var lbl_bottom_price:UILabel!
    @IBOutlet weak var lbl_optionTitle:UILabel!

    @IBOutlet weak var coll_images:UICollectionView!
    @IBOutlet weak var tbl_rooms:UITableView!
    @IBOutlet weak var tbl_transfer:UITableView!
    @IBOutlet weak var tbl_roomOptions:UITableView!

    @IBOutlet var img_stars:[UIImageView]!

    @IBOutlet weak var hei_tblImages: NSLayoutConstraint!
    @IBOutlet weak var hei_tblRooms: NSLayoutConstraint!
    @IBOutlet weak var hei_tblTransfer: NSLayoutConstraint!
    @IBOutlet weak var hei_tblRoomOptions: NSLayoutConstraint!

    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCells()
        showInitialData()
        addRoomOptionsView()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        hei_tblRooms.constant = tbl_rooms.contentSize.height+50
        hei_tblRoomOptions.constant = tbl_roomOptions.contentSize.height

        if !Constants.isLoaded{
            Constants.isLoaded = true
            bottomView.setOnlyTopCornerRadiusWithShadow(30)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Constants.isLoaded = false
    }
    
    
    //MARK:- Register Cells
    func registerCells(){
        coll_images.register(UINib.init(nibName: "ImagesCell", bundle: nil), forCellWithReuseIdentifier:
            "ImagesCell")
        tbl_rooms.register(UINib.init(nibName: "HotelRoomDetailsCell", bundle: nil), forCellReuseIdentifier: "HotelRoomDetailsCell")
        tbl_transfer.register(UINib.init(nibName: "TransferSelectionCell", bundle: nil), forCellReuseIdentifier: "TransferSelectionCell")
        tbl_roomOptions.register(UINib.init(nibName: "RoomOptionsCell", bundle: nil), forCellReuseIdentifier: "RoomOptionsCell")

    }
    //MARK:- Show initial data
    func showInitialData(){
        
        lbl_title.text = HotelViewModel.details.cityName
        lbl_subTitle.text = HotelViewModel.getSubTitle()
        
        lbl_currency.text = kCurrency
        lbl_bottom_currency.text = kCurrency
        
        let data = HotelSearchViewModel.searchResults[HotelSearchViewModel.selectedIndex]
        lbl_hotelName.text = data.hotel_name
        lbl_location.text = data.address
       
        let imageUrl = "\(Server.image_url)\(data.hotel_picture)"

        img_hotel.sd_setShowActivityIndicatorView(true)
        img_hotel.sd_setIndicatorStyle(.gray)
        img_hotel.sd_setImage(with: URL.init(string: imageUrl)) { (image, error, cache, urls) in
            self.img_hotel.image = (error != nil) ? hotelPlaceHolder : image
        }
        
        let rating = data.star_rating
        
        for imageView in img_stars{
            let tag = "\(imageView.tag)"
            imageView.image = #imageLiteral(resourceName: "starSelected")
            if tag == rating{
                break
            }
        }
        
        self.showTotalRoomsPrice()
        
        hei_tblRooms.constant = tbl_rooms.contentSize.height+50
        hei_tblImages.constant = (HotelDetailsViewModel.hotelDetails?.images.count == 0) ? 0 : 100
        
        tbl_transfer.reloadData()
        hei_tblTransfer.constant = tbl_transfer.contentSize.height+50
        
    }
    
    //MARK:- Show Room details
    func showTotalRoomsPrice(){
        
        lbl_price.text = String(format:"%.2f",HotelDetailsViewModel.getTotalPriceOfRooms())
        lbl_bottom_price.text = String(format:"%.2f",HotelDetailsViewModel.getTotalPriceOfRooms())
        
    }
    
    //MARK:- Add Options view
    func addRoomOptionsView(){
        optionsView.frame = self.view.bounds
        optionsView.isHidden = true
        self.view.addSubview(optionsView)
    }
    
    //MARK:- Show Alert view
    func showAlertViewPolicy(policy:String){
        
        let alertView = UIAlertController.init(title: "Cancellation Policy", message: policy, preferredStyle: .alert)
        alertView.addAction(UIAlertAction.init(title: "OK", style: .default, handler: { _ in
            print("ok clicked")
        }))
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    //MARK:- Validate room price
    func validateAndGetRoomPrice(){
     
        let data = HotelDetailsViewModel.hotelRoomsList[HotelDetailsViewModel.selectedRoomIndex]

        if data.selected_check_in.count != 0 && data.selected_check_out.count != 0{
            getRoomPriceAPICall()
        }
        
    }
    
    //MARK:- Show Options View
    func showOptionsView(option:SelectOption){
        
        switch option {
        case .CheckIn:
            HotelDetailsViewModel.selectedOption = .CheckIn
            lbl_optionTitle.text = "Select CheckIn Date"

        case .CheckOut:
            HotelDetailsViewModel.selectedOption = .CheckOut
            lbl_optionTitle.text = "Select CheckOut Date"

        case .MealType:
            HotelDetailsViewModel.selectedOption = .MealType
            lbl_optionTitle.text = "Select Meal Type"

        }
        
        tbl_roomOptions.reloadData()
        hei_tblRoomOptions.constant = tbl_roomOptions.contentSize.height
        tbl_roomOptions.layoutIfNeeded()
        optionsView.layoutIfNeeded()
        self.viewDidLayoutSubviews()

        optionsView.isHidden = false
        
    }
    
    //MARK:- Room Option selected
    func roomOptionSeleted(index:Int){
        
        if HotelDetailsViewModel.validateDate(index: index){
            HotelDetailsViewModel.updateRoomSelectionData(index: index)
            tbl_rooms.reloadData()
            optionsView.isHidden = true
            
            if  !HotelDetailsViewModel.selectedRoomIndexes.contains(HotelDetailsViewModel.selectedRoomIndex){
                HotelDetailsViewModel.selectedRoomIndexes.append(HotelDetailsViewModel.selectedRoomIndex)
            }
            
            self.validateAndGetRoomPrice()
        }else{
           // appDelegate.window?.makeToast(message: "Please select valid date")
        }
        
    }
    
    //MARK:- Check for selected data
    func validateSelectedRoomData(index:Int) -> Bool{
        
        let data = HotelDetailsViewModel.hotelRoomsList[index]
        
        if data.selected_check_out.count != 0 && data.selected_check_out.count != 0{
            appDelegate.window?.makeToast(message: "Room data already selected")
            return false
        }
        
        return true
    }
    
}

extension SelectRoomVC{
    //MARK:- API's
    
    func getRoomPriceAPICall(){
        HotelDetailsViewModel.getRoomPriceAPIConnection(handler: {
            
            self.tbl_rooms.reloadData()
            self.showTotalRoomsPrice()
        })
    }
    
    func bookHotelRoomAPICall(){
        
        HotelReviewViewModel.hotelRoomBlockAPICall(handler: {
           
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "HotelPassangersVC") as! HotelPassangersVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
    }
    
}

extension SelectRoomVC{
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueClicked(_ sender:UIButton){
       
        if HotelDetailsViewModel.validateDetailsForRoomBlock(){
            bookHotelRoomAPICall()
        }
        
    }
    
    @IBAction func resetClicked(_ sender:UIButton){

        HotelDetailsViewModel.resetRoomsData()
        tbl_rooms.reloadData()
        tbl_roomOptions.reloadData()

    }
    
    @IBAction func removeOptionsViewClicked(_ sender:UIButton){

        optionsView.isHidden = true
    }
    
    @objc func selectCheckInDateClicked(_ sender:UIButton){
        
        if self.validateSelectedRoomData(index: sender.tag){
            HotelDetailsViewModel.selectedRoomIndex = sender.tag
            showOptionsView(option: .CheckIn)
        }
      
    }
    
    @objc func selectCheckOutDateClicked(_ sender:UIButton){
        
        if self.validateSelectedRoomData(index: sender.tag){
            
            let data = HotelDetailsViewModel.hotelRoomsList[sender.tag]
            
            if data.selected_check_in.count == 0 {
                appDelegate.window?.makeToast(message: "Please select Check In date")
            }else{
                HotelDetailsViewModel.selectedRoomIndex = sender.tag
                showOptionsView(option: .CheckOut)
            }
        }
       
    }
    
    @objc func selectFoodTypeClicked(_ sender:UIButton){
        
        HotelDetailsViewModel.selectedRoomIndex = sender.tag
        showOptionsView(option: .MealType)
        
    }
    
    @objc func showPolicyClicked(_ sender:UIButton){
        
        let data = HotelDetailsViewModel.hotelRoomsList[sender.tag]
        self.showAlertViewPolicy(policy: data.cancellation_policy.convertHtmlToString())
        
    }
    
    @objc func selectRoomClicked(_ sender:UIButton){
        
        HotelDetailsViewModel.selectedRoomIndex = sender.tag
        tbl_rooms.reloadData()
        tbl_rooms.layoutIfNeeded()
        hei_tblRooms.constant = tbl_rooms.contentSize.height+50
        self.showTotalRoomsPrice()
        
    }
    
}

extension SelectRoomVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_rooms{
            return HotelDetailsViewModel.hotelRoomsList.count
        }else if tableView == tbl_transfer{
            return HotelDetailsViewModel.hotelTransfers.count
        }else{
            return HotelDetailsViewModel.getRoomOptionsCount()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbl_rooms{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HotelRoomDetailsCell", for: indexPath) as! HotelRoomDetailsCell
            cell.selectionStyle = .none
            cell.loadCellData(index: indexPath.row)
            
         //   cell.btn_select.addTarget(self, action: #selector(self.selectRoomClicked(_:)), for: .touchUpInside)
            cell.btn_policy.addTarget(self, action: #selector(self.showPolicyClicked(_:)), for: .touchUpInside)
            
            cell.btn_checkInDate.addTarget(self, action: #selector(self.selectCheckInDateClicked(_:)), for: .touchUpInside)
            cell.btn_checkOutDate.addTarget(self, action: #selector(self.selectCheckOutDateClicked(_:)), for: .touchUpInside)
            cell.btn_foodType.addTarget(self, action: #selector(self.selectFoodTypeClicked(_:)), for: .touchUpInside)
            
            return cell
            
        }else if tableView == tbl_transfer{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransferSelectionCell", for: indexPath) as! TransferSelectionCell
            cell.selectionStyle = .none
           
            cell.configureCellWithData(index: indexPath.row)
            
            return cell

        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RoomOptionsCell", for: indexPath) as! RoomOptionsCell
            cell.selectionStyle = .none
           
            cell.configureCellWithData(index: indexPath.row)

            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tbl_transfer{
            HotelDetailsViewModel.selectTransfers(index: indexPath.row)
            tableView.reloadData()
        }
        if tableView == tbl_roomOptions{
            roomOptionSeleted(index:indexPath.row)
        }
        
    }
    
}

extension SelectRoomVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return HotelDetailsViewModel.hotelDetails?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as! ImagesCell
        
        cell.configureCell(index:indexPath.row)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: 100, height: collectionView.frame.height)
        return size
    }
    
}


