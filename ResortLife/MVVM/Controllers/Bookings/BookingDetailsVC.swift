//
//  BookingDetailsVC.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 06/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class BookingDetailsVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tbl_passengers:UITableView!
    @IBOutlet weak var tbl_paymentDetails:UITableView!

    @IBOutlet weak var lbl_hotelName:UILabel!
    @IBOutlet weak var lbl_hotelLocation:UILabel!
    @IBOutlet weak var img_hotel:UIImageView!
    @IBOutlet weak var lbl_checkIn:UILabel!
    @IBOutlet weak var lbl_checkOut:UILabel!
    @IBOutlet weak var lbl_orderNo:UILabel!
    @IBOutlet weak var lbl_bookingStatus:UILabel!
    @IBOutlet weak var lbl_ticketStatus:UILabel!
    @IBOutlet weak var lbl_pnr:UILabel!
    @IBOutlet weak var lbl_totalAmount:UILabel!

    
    @IBOutlet weak var hei_tblPassengers:NSLayoutConstraint!
    @IBOutlet weak var hei_tblPayment:NSLayoutConstraint!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCells()
        showDetails()
    }
    
    override func viewDidLayoutSubviews() {
        
        hei_tblPassengers.constant = tbl_passengers.contentSize.height
        hei_tblPayment.constant = tbl_paymentDetails.contentSize.height

    }
    
    //MARK:- Show details
    func showDetails(){
        
        let data = BookingViewModel.hotelBookings[BookingViewModel.selectedIndex]
       
        lbl_orderNo.text = "Order No: \(data.app_reference )"
        lbl_bookingStatus.text = "\(data.status )"
        
        let email = data.email
        let mobile = data.phone_number
        lbl_ticketStatus.text = "Ticket is emailed to \(email) and SMS sent to \(mobile). Have a safe trip!"
        
        lbl_pnr.text = "PNR - \(data.booking_reference)"
        lbl_totalAmount.text = String.init(format: "%@ %.02f",kCurrency, data.grand_total ?? 0)
        lbl_hotelName.text = data.hotel_name
        lbl_hotelLocation.text = data.hotel_location
        
        let imageUrl = "\(Server.image_url)\(data.hotel_image)"
        
        img_hotel.sd_setShowActivityIndicatorView(true)
        img_hotel.sd_setIndicatorStyle(.gray)
        img_hotel.sd_setImage(with: URL.init(string: imageUrl)) { (image, error, cache, urls) in
            self.img_hotel.image = (error != nil) ? hotelPlaceHolder : image
        }
        lbl_checkIn.text = "Check In: \(DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: data.hotel_check_in, toFormat: "dd MMM yyyy"))"
        lbl_checkOut.text = "Check Out: \(DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: data.hotel_check_out, toFormat: "dd MMM yyyy"))"

        
    }
    
}

extension BookingDetailsVC{
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BookingDetailsVC:UITableViewDataSource,UITableViewDelegate{
   
    func registerCells(){
        tbl_passengers.register(UINib.init(nibName: "PassengerStatusCell", bundle: nil), forCellReuseIdentifier: "PassengerStatusCell")
        tbl_paymentDetails.register(UINib.init(nibName: "PaymentDetailsCell", bundle: nil), forCellReuseIdentifier: "PaymentDetailsCell")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_passengers{
            return BookingViewModel.hotelBookings[BookingViewModel.selectedIndex].customers?.count ?? 0
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == tbl_passengers{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PassengerStatusCell", for: indexPath) as! PassengerStatusCell
            cell.selectionStyle = .none
            cell.configureCell(index:indexPath.row)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentDetailsCell", for: indexPath) as! PaymentDetailsCell
            cell.selectionStyle = .none
            cell.showPaymentDetails(index:indexPath.row)
            return cell
        }
        
        
    }
    
}
