//
//  MyBookingsVC.swift
//  ElamantTravel
//
//  Created by PTBLR-1206 on 05/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class MyBookingsVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var lbl_noData:UILabel!
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var tbl_bookings:UITableView!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.getBookings()
        
    }
    
    //MARK:- Initial setup
    func initialSetup(){
        lbl_noData.isHidden = true
        containerView.isHidden = true
    }
    
    //MARK:- Show/Hide container view
    func showHideContainerView(){
       
        self.tbl_bookings.reloadData()

        lbl_noData.isHidden = true
        containerView.isHidden = false

        if BookingViewModel.hotelBookings.count == 0{
         
            lbl_noData.isHidden = false
            containerView.isHidden = true
            
        }
        
    }

}

extension MyBookingsVC{
    //MARK:- API's
    
    func getBookings(){
        
        BookingViewModel.getBookingsApiCall(handler: {
            self.showHideContainerView()
        })
    }
    
}


extension MyBookingsVC{
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension MyBookingsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return BookingViewModel.hotelBookings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingsCell", for: indexPath) as! BookingsCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        cell.setUpCellData(index:indexPath.row)

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        BookingViewModel.selectedIndex = indexPath.row
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailsVC") as! BookingDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

class BookingsCell:UITableViewCell{
   
    @IBOutlet weak var lbl_orderNo:UILabel!
    @IBOutlet weak var lbl_price:UILabel!
    @IBOutlet weak var lbl_currency:UILabel!
    @IBOutlet weak var lbl_bookingDate:UILabel!
    @IBOutlet weak var lbl_bookingStatus:UILabel!
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var lbl_date_location:UILabel!

    
    //MARK:- Setup cell data
    func setUpCellData(index:Int){
        
        self.tag = index
        
        let model = BookingViewModel.hotelBookings[self.tag]
        
        lbl_name.text = "\(UserViewModel.userData?.first_name ?? "") \(UserViewModel.userData?.last_name ?? "")"
        
        lbl_orderNo.text = "Order No: \(model.app_reference )"
        lbl_bookingStatus.text = "\(model.status )"
        
        lbl_currency.text = kCurrency
        lbl_price.text = String.init(format: "%.02f", model.grand_total ?? 0)
        
        let bookingDate = model.booked_date
        lbl_bookingDate.text = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd HH:mm:ss", date: bookingDate, toFormat: "hh:mm a, dd MMM yyyy")
        
        let jouneyDate = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd HH:mm:ss", date: model.hotel_check_in , toFormat: "dd MMM yyyy")
        lbl_date_location.text = "\(model.hotel_location) | \(jouneyDate)"
       
    }
    
    
}
