//
//  ForgotPasswordVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 24/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var txf_email:UITextField!
    @IBOutlet weak var txf_mobile:UITextField!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- Validate fields
    func validateFields()->Bool{
        if txf_email.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Email")
            return false
        }
        else if txf_mobile.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Mobile Number")
            return false
        }
        return true
    }    
    
}

extension ForgotPasswordVC{
    //MARK:- API'S
    func forgotPasswordAPICall(){
        
        var parameters = [String: String]()
        parameters["user_type"] = kUserType
        let details:[String: String] = ["email":txf_email.text ?? "","phone":txf_mobile.text ?? ""]
        parameters["forgot_details"] = VKAPIs.getJSONString(object: details)
        
        LoginViewModel.forgotPasswordAPICall(params: parameters, handler: { message in
            
            self.view.makeToast(message: message)
            self.txf_email.text = ""
            self.txf_mobile.text = ""

        })
    }
}

extension ForgotPasswordVC{
    //MARK:- IBActions
    
    @IBAction func backClicked(_ sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func doneClicked(_ sender:UIButton){
        
        if !validateFields(){return}

        self.view.endEditing(true)
        self.forgotPasswordAPICall()
        
    }
}

