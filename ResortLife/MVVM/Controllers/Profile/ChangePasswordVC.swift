//
//  ChangePasswordVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 28/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var txf_oldPassword:UITextField!
    @IBOutlet weak var txf_newPassword:UITextField!
    @IBOutlet weak var txf_confirmPassword:UITextField!

    @IBOutlet weak var hei_oldPassword:NSLayoutConstraint!
    
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- Validate fields
    func validateFields()->Bool{
        
        if txf_oldPassword.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Old password")
            return false
        }
        if txf_newPassword.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter New password")
            return false
        }
        if txf_confirmPassword.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Confirm password")
            return false
        }
        if txf_newPassword.text != txf_confirmPassword.text{
            appDelegate.window?.makeToast(message: "New password and Confirm password are not equal")
            return false
        }
        return true
    }

    //MARK:- Reset fields
    func resetFields(){
        txf_oldPassword.text = ""
        txf_newPassword.text = ""
        txf_confirmPassword.text = ""
    }

}
extension ChangePasswordVC{
    
    //MARK:- API's
    func changePasswordAPICall(){
        
        var parameters = [String: String]()
        parameters["current_password"] = txf_oldPassword.text ?? ""
        parameters["new_password"] = txf_newPassword.text ?? ""
        parameters["user_type"] = UserViewModel.userData?.user_type
        parameters["user_id"] = UserViewModel.userData?.user_id
        
        LoginViewModel.changePasswordAPICall(params: parameters, handler: { message in
            self.view.makeToast(message: message)
            self.resetFields()
        })
        
    }
    
}

extension ChangePasswordVC{
    
    //MARK:- IBActions
    @IBAction func backClicked(_ sender:UIButton){
      
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func doneClicked(_ sender:UIButton){
       
        if !validateFields(){return}
        
        self.view.endEditing(true)
        
        self.changePasswordAPICall()
        
    }
}
