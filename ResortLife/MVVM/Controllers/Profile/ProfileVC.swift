//
//  ProfileVC.swift
//  ResortLife
//
//  Created by Apple retina on 18/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
   //MARK:- IBOutlets
    @IBOutlet weak var txf_title:UITextField!
    @IBOutlet weak var txf_firstName:UITextField!
    @IBOutlet weak var txf_lastName:UITextField!
    @IBOutlet weak var txf_country:UITextField!
    @IBOutlet weak var txf_dateOfBirth:UITextField!
    @IBOutlet weak var txf_panNumber:UITextField!
    @IBOutlet weak var txf_officeNumber:UITextField!
    @IBOutlet weak var txf_mobile:UITextField!
    @IBOutlet weak var txf_email:UITextField!
    @IBOutlet weak var txtView_address:UITextView!
    @IBOutlet weak var img_user:UIImageView!
    
    @IBOutlet weak var tbl_titles:UITableView!
    @IBOutlet weak var titlesView:UIView!

    @IBOutlet weak var hei_tblTitles: NSLayoutConstraint!

    
    //MARK:- Variables
    var selected_dob = ""
    var selected_title = ""
    var selected_country_code = ""
    var countryView:CountrySelectionView?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
        getProfileDetailsAPICall()
        addDOBDatePicker()
        addCountryView()
        addTitlesView()
    }
    
    override func viewDidLayoutSubviews() {
        hei_tblTitles.constant = tbl_titles.contentSize.height
    }
    
    //MARK:- Show Profile data
    func showProfileData(){
        
        if let dateOfBirth = UserViewModel.userData?.date_of_birth{
            selected_dob = dateOfBirth
            txf_dateOfBirth.text = DateFormatter.changeDateFormatString(fromFormat: "yyyy-MM-dd", date: dateOfBirth, toFormat: "dd MMM yyyy")
        }
        
        selected_title = UserViewModel.userData?.title ?? ""
        selected_country_code = UserViewModel.userData?.country_code ?? ""

        txf_title.text = UserViewModel.getTitleFromId(title_id: selected_title)
        txf_firstName.text = UserViewModel.userData?.first_name
        txf_lastName.text = UserViewModel.userData?.last_name
        txf_country.text = UserViewModel.userData?.country_name
        txf_mobile.text = UserViewModel.userData?.phone
        txf_email.text = UserViewModel.userData?.email
        txf_officeNumber.text = UserViewModel.userData?.office_phone
        txf_panNumber.text = UserViewModel.userData?.pan_number
        txtView_address.text = UserViewModel.userData?.address
        
        if let user_image = UserViewModel.userData?.image,user_image != ""{
            let image_url = "\(Server.image_url)\(user_image)"
            img_user.sd_setShowActivityIndicatorView(true)
            img_user.sd_setIndicatorStyle(.gray)
            img_user.sd_setImage(with: URL.init(string: image_url)) { (image, error, cache, urls) in
                self.img_user.image = (error != nil) ? userPlaceHolder : image
            }
        }
        
    }
    
    //MARK:- Add Options view
    func addTitlesView(){
        titlesView.frame = self.view.bounds
        titlesView.isHidden = true
        self.view.addSubview(titlesView)
    }
    
    //MARK:- Add country View
    func addCountryView(){
        getCountryCodes()

        guard let customView = CountrySelectionView().loadNib() as? CountrySelectionView else {return}
        
        countryView = customView
        countryView?.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        countryView?.delegate = self
        self.view.addSubview(countryView!)
       
    }
    
    //MARK:- Get Country codes
    func getCountryCodes(){
        if HotelCitiesModel.countryList.count == 0{
            HotelCitiesModel.getCountriesList()
        }
        print("count \(HotelCitiesModel.countryList.count)")
    }
    
    //MARK:- Add date picker
    func addDOBDatePicker(){
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(selectDateOfBirthFromDatePicker(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        txf_dateOfBirth.inputView = datePicker
    }
   
    //MARK:- Validate fields
    func validateFields()->Bool{
        if txf_firstName.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter First Name")
            return false
        }
        if txf_lastName.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Last Name")
            return false
        }
        if txf_mobile.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Mobile number")
            return false
        }
        if !CommonUtilities.validateMobile(value: txf_mobile.text!){
            appDelegate.window?.makeToast(message: "Please enter Valid mobile number")
            return false
        }
        if txf_dateOfBirth.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Date Of Birth")
            return false
        }
        if txf_officeNumber.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Office Number")
            return false
        }
        if txf_panNumber.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter PAN Number")
            return false
        }
        if !CommonUtilities.validatePANCardNumber(txf_panNumber.text!){
            appDelegate.window?.makeToast(message: "Please enter Valid PAN Number")
            return false
        }
        if txtView_address.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please enter Address")
            return false
        }
        
        if img_user.image == nil || img_user.image == userPlaceHolder{
            appDelegate.window?.makeToast(message: "Please select Image")
            return false
        }
        return true
    }

}

extension ProfileVC:DImagePickerDelegate{
    
    //MARK:- Image Picker delegates
    func imagePickerDidSelected(image: UIImage) {
        img_user.image = image
    }
    
    func imagePickerDidCancel() {
        
    }
    
}

extension ProfileVC:SelectCountryDelegate{
   
    //MARK:- Select Country delegate
    func selectedCountryInfo(data: CountryModel) {
        txf_country.text = data.country_name
        selected_country_code = data.country_code
    }
    
    func removeView(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.countryView?.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
        }, completion:nil)
    }
    
}

extension ProfileVC{
    
    //MARK:- API's
    
    func getProfileDetailsAPICall(){
        var params = [String:String]()
        params["user_id"] = UserViewModel.userData?.user_id ?? ""
        UserViewModel.getProfileDetails(params: params, handler: {
            self.showProfileData()
        })
    }
    
    func updateProfileAPICall(){
        
        var profile_info = [String:String]()
        profile_info["title"] = selected_title
        profile_info["first_name"] = txf_firstName.text
        profile_info["last_name"] = txf_lastName.text
        profile_info["phone"] = txf_mobile.text
        profile_info["address"] = txtView_address.text
        profile_info["country_code"] = selected_country_code
        profile_info["date_of_birth"] = selected_dob
        profile_info["pan_number"] = txf_panNumber.text
        profile_info["office_phone"] = txf_officeNumber.text
        
        var params = [String:String]()
        params["update_profile"] = VKAPIs.getJSONString(object: profile_info)
        params["user_id"] = UserViewModel.userData?.user_id

        var imgParams = [String:UIImage]()
        imgParams["profile_image"] = img_user.image
        
        UserViewModel.updateUserProfile(params: params, imageParms: imgParams, handler: {_ in
            
            self.view.makeToast(message: "Profile updated successfully")
        })
        
    }
    
}

extension ProfileVC{
    
    //MARK:- IBActions
    @IBAction func backAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectTitleClicked(_ sender:UIButton){

        tbl_titles.reloadData()
        hei_tblTitles.constant = tbl_titles.contentSize.height
        tbl_titles.layoutIfNeeded()
        titlesView.layoutIfNeeded()
        self.viewDidLayoutSubviews()

        titlesView.isHidden = false
        
    }
    
    @IBAction func removeTitleViewClicked(_ sender:UIButton){
        
        titlesView.isHidden = true
    }
   
    @IBAction func selectCountryClicked(_ sender:UIButton){
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.countryView?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion:nil)
        
    }
    @IBAction func changePasswordClicked(_ sender:UIButton){
        let vc = MainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func selectDateOfBirthFromDatePicker(_ sender:UIDatePicker){
        
        txf_dateOfBirth.text = DateFormatter.getDateString(formate: "dd MMM yyyy", date: sender.date)
        let selected_date = DateFormatter.getDateString(formate: "yyyy-MM-dd", date: sender.date)
        selected_dob = selected_date
    }
    
    @IBAction func cameraClicked(_ sender:Any){
        
        DImagePicker.shared.delegate = self
        DImagePicker.shared.view_ctrl = self
        DImagePicker.shared.addAlertViewCtrl(isEdit: false)
        
    }
    
    @IBAction func updateClicked(_ sender:UIButton){
        
        if !validateFields(){return}
        
        self.view.endEditing(true)
        self.updateProfileAPICall()
        
    }
    
}

extension ProfileVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return UserViewModel.titlesArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitlesCell", for: indexPath) as! TitlesCell
            cell.selectionStyle = .none
            
            cell.configureCell(index: indexPath.row)
            
            return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selected_title = UserViewModel.titlesArray[indexPath.row].1
        txf_title.text = UserViewModel.titlesArray[indexPath.row].0
        titlesView.isHidden = true

    }
    
}



class TitlesCell:UITableViewCell{
    
    //MARK:- IBOutlets
    @IBOutlet weak var lbl_title:UILabel!
    
    
    //MARK:- Configure cell
    func configureCell(index:Int){
        lbl_title.text = UserViewModel.titlesArray[index].0
    }
    
    
}
