//
//  SideMenuVC.swift
//  ResortLife
//
//  Created by Apple retina on 20/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tbl_menu:UITableView!
    @IBOutlet weak var lbl_userName:UILabel!
    @IBOutlet weak var img_user:UIImageView!

    //MARK:- Variables
    private var cellIdentifier = "SideMenuCell"
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCells()
        showInitialData()
        
    }
        
    //MARK:- Show initial data
    func showInitialData(){
       
        lbl_userName.text = UserViewModel.userData?.first_name
        
        if let user_image = UserViewModel.userData?.image,user_image != ""{
            let image_url = "\(Server.image_url)\(user_image)"
            img_user.sd_setShowActivityIndicatorView(true)
            img_user.sd_setIndicatorStyle(.gray)
            img_user.sd_setImage(with: URL.init(string: image_url)) { (image, error, cache, urls) in
                self.img_user.image = (error != nil) ? userPlaceHolder : image
            }
        }
        
    }
    
}

extension SideMenuVC:UITableViewDelegate,UITableViewDataSource{
    
    func registerCells(){
        tbl_menu.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HomeViewModel.menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SideMenuCell
        cell.selectionStyle = .none
        
        cell.configureCell(index:indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        HomeViewModel.selectedMenuIndex = indexPath.row
        NotificationCenter.default.post(name: kMenuClicked, object: nil)
        
    }
    
    
}
