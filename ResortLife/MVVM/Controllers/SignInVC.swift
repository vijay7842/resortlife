//
//  SignInVC.swift
//  ResortLife
//
//  Created by Apple retina on 15/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class SignInVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var txf_userName:UITextField!
    @IBOutlet weak var txf_password:UITextField!
    @IBOutlet weak var loginView:UIView!
    @IBOutlet weak var videoContentView:VideoView!
    
    //MARK:- Variables
    var showView:Bool = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        loginView.isHidden = (showView == true) ?false :true
        playVideo()
        
    }
    
    //MARK:- Play Video
    func playVideo(){
        videoContentView.layoutIfNeeded()
        videoContentView.playVideoFromFilePath(fileName: "app_video1")
        videoContentView.isLoop = true
        videoContentView.play()
    }
    
    //MARK:- Validate fields
    func validateFields()->Bool{
        if txf_userName.text?.count == 0{
            self.view.makeToast(message: "Please enter User name")
            return false
        }
        if txf_password.text?.count == 0{
            self.view.makeToast(message: "Please enter password")
            return false
        }
        return true
    }

   
}

extension SignInVC{
    
    //MARK:- API Calls
    func doSignInAPICall(){
        
        var params = [String: String]()
        params["username"] = txf_userName.text ?? ""
        params["password"] = txf_password.text ?? ""
        
        LoginViewModel.loginAPICall(params: params, handler: {_ in
            
            HandleNavigationScreens.showHomeScreen()
            
        })
  }

}

extension SignInVC{
    
    //MARK:- IBActions
    
    @IBAction func upArrowClicked(_ sender:UIButton){

        showView = !showView
        loginView.isHidden = (showView == true) ?false :true

    }
    
    @IBAction func forgotPasswordClicked(_ sender:UIButton){
        
        let vc = MainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func loginClicked(_ sender:UIButton){
        
        if !showView{
            showView = true
            loginView.isHidden = (showView == true) ?false :true
        }else{
            if !validateFields(){return}
            
            self.view.endEditing(true)
            self.doSignInAPICall()
        }
        
    }
    
}
