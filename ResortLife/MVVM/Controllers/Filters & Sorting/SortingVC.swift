//
//  SortingVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 03/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

protocol SortingDelegate {
    func applySorting()
}

class SortingVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet var img_price:[UIImageView]!
    @IBOutlet var img_rating:[UIImageView]!
    @IBOutlet var img_name:[UIImageView]!

    //MARK:- Variables
    var delegate:SortingDelegate?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadInitialData()
        
    }
    
    //MARK:- Show initial values
    func loadInitialData(){
        
        self.showSelectedSort()
        
    }

    //MARK:- Remove self.view
    func removeMainView(){
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            
            self.view.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: {(_ finished: Bool) -> Void in
            self.willMove(toParent: nil)
            self.removeFromParent()
            self.view.removeFromSuperview()
        })
    }
    
    //MARK:- show selected Sort
    func showSelectedSort(){
        
        let sortNumber = HomeViewModel.getSelectedSortNumber()
        
        for imageView in img_price{
            
            imageView.image = (sortNumber == imageView.tag) ?  #imageLiteral(resourceName: "radio_on"): #imageLiteral(resourceName: "radio_off")
        }
        
        for imageView in img_rating{
            imageView.image = (sortNumber == imageView.tag) ?  #imageLiteral(resourceName: "radio_on"): #imageLiteral(resourceName: "radio_off")
        }
        
        for imageView in img_name{
            imageView.image = (sortNumber == imageView.tag) ?  #imageLiteral(resourceName: "radio_on"): #imageLiteral(resourceName: "radio_off")
        }
      
    }

}

extension SortingVC{
    
    //MARK:- IBActions
    @IBAction func cancelClicked(_ sender:UIButton){
       
        self.removeMainView()
        
    }
    @IBAction func saveClicked(_ sender:UIButton){
      
        self.removeMainView()
        delegate?.applySorting()
        
    }
    
    @IBAction func sortByPriceClicked(_ sender:UIButton){

        HomeViewModel.updateSortNumber(sortNumber:sender.tag)
        self.showSelectedSort()

    }
    
    @IBAction func sortByRatingClicked(_ sender:UIButton){
       
        HomeViewModel.updateSortNumber(sortNumber:sender.tag)
        self.showSelectedSort()

    }
    
    @IBAction func sortByNameClicked(_ sender:UIButton){
      
        HomeViewModel.updateSortNumber(sortNumber:sender.tag)
        self.showSelectedSort()

    }
}
