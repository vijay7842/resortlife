//
//  FiltersVC.swift
//  ElamantTravel
//
//  Created by Apple retina on 03/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit

protocol FilterDelegate {
    func applyFilters()
}

class FiltersVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tbl_locations:UITableView!

    @IBOutlet weak var sliderContainerView:UIView!
    @IBOutlet weak var priceFilterView:UIView!

    @IBOutlet weak var lbl_minPrice:UILabel!
    @IBOutlet weak var lbl_maxPrice:UILabel!

    @IBOutlet var img_stars:[UIImageView]!
    @IBOutlet var starViews:[UIView]!
    @IBOutlet var img_checkBox:[UIImageView]!

    @IBOutlet weak var hei_locationView:NSLayoutConstraint!
    @IBOutlet weak var hei_amenitiesView:NSLayoutConstraint!
    
    //MARK:- Variables
    var delegate:FilterDelegate?
    var price_slider = MARKRangeSlider()
    var locations = [String]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.addPriceSlider()
        self.registerCells()
        self.loadInitialData()
        
    }
    
    //MARK:- Add slider
    func addPriceSlider(){
      
        //Price Slider
        sliderContainerView.layoutIfNeeded()
        
        price_slider.frame = CGRect.init(x: 15, y: sliderContainerView.frame.origin.y, width: sliderContainerView.frame.width, height: sliderContainerView.frame.height)
        price_slider.minimumDistance = 0.01
        price_slider.addTarget(self, action: #selector(sliderValueChanged(_:)), for: .valueChanged)
        priceFilterView.addSubview(price_slider)
        
       let values = HomeViewModel.getMinMaxValues()
       price_slider.setMinValue(CGFloat(values.0), maxValue: CGFloat(values.1))
        
    }
    
    //MARK:- Initial setup
    func loadInitialData(){
        
        locations = FilterSortViewModel.locationNames

       
        let values = HomeViewModel.getSelectedMinMaxValues()
        price_slider.setLeftValue(CGFloat(values.0), rightValue: CGFloat(values.1))
        
        lbl_minPrice.text = String(format: "%@ %.2f",kCurrency,values.0)
        lbl_maxPrice.text = String(format: "%@ %.2f",kCurrency,values.1)
        
        tbl_locations.reloadData()
        self.view.layoutIfNeeded()
        
        hei_locationView.constant = (locations.count == 0) ? 0 : tbl_locations.contentSize.height+55
        
        self.showSelectedFilter()
        
    }

    //MARK:- Remove self.view
    func removeMainView(){
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            
            self.view.frame = CGRect.init(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: {(_ finished: Bool) -> Void in
            self.willMove(toParent: nil)
            self.removeFromParent()
            self.view.removeFromSuperview()
        })
    }
    
    //MARK:- show selected Sort
    func showSelectedFilter(){
        
        //Amenities
        
        for imageView in img_checkBox{
            
            let checkAmenity = HomeViewModel.checkAmenities(index:imageView.tag-1)
            imageView.image = checkAmenity ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "unchecked")
            
        }
        
        //Rating
        let selectedRating = HomeViewModel.getSelectedRating()
        for imageView in img_stars{

            imageView.image = (selectedRating == imageView.tag) ?  #imageLiteral(resourceName: "starSelected"): #imageLiteral(resourceName: "starUnselected")
        }
        
        for view in starViews{
            view.layer.borderColor = AppColors.green.cgColor
            view.layer.borderWidth = (selectedRating == view.tag) ? 1 : 0
        }
        
    }
    
}

extension FiltersVC{
    //MARK:- IBActions
    @IBAction func backClicked(_ sender:UIButton){
      
       removeMainView()
        
    }
    @IBAction func doneClicked(_ sender:UIButton){
       
        removeMainView()
        delegate?.applyFilters()
        
    }
    @IBAction func reloadClicked(_ sender:UIButton){
      
       HomeViewModel.clearAllFilters()
       self.loadInitialData()
        
    }
    @IBAction func starRatingSelected(_ sender:UIButton){
      
        HomeViewModel.upateRating(rating:sender.tag)
        showSelectedFilter()
        
    }
    @IBAction func amenitiesSelected(_ sender:UIButton){
        
        HomeViewModel.updateAmenities(index:sender.tag-1)
        showSelectedFilter()
        
    }
    
    @objc func sliderValueChanged(_ sender:MARKRangeSlider){
    HomeViewModel.updateMinMaxValues(minValue:Float(sender.leftValue),maxValue:Float(sender.rightValue))
        
        lbl_minPrice.text = String(format: "%@ %.2f",kCurrency,sender.leftValue)
        lbl_maxPrice.text = String(format: "%@ %.2f",kCurrency,sender.rightValue)

    }
}


extension FiltersVC:UITableViewDataSource,UITableViewDelegate{
    
    func registerCells(){
        tbl_locations.register(UINib.init(nibName: "MultiSelectionCell", bundle: nil), forCellReuseIdentifier: "MultiSelectionCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultiSelectionCell", for: indexPath) as! MultiSelectionCell
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        
        let locationName = locations[indexPath.row]
        cell.lbl_name.text = locationName
      
        let contains:Bool = HomeViewModel.checkLocation(location: locationName)
        
        cell.img_checkBox.image = contains ?  #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "unchecked")
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        HomeViewModel.updateLocation(location: locations[indexPath.row])
      
        tableView.reloadData()
        
    }
    
}
