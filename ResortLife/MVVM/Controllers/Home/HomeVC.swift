//
//  HomeVC.swift
//  ResortLife
//
//  Created by Apple retina on 15/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation

class HomeVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var videoContentView:VideoView!

    @IBOutlet weak var walletView:UIView!
    @IBOutlet weak var walletContentView:UIView!

    @IBOutlet weak var lbl_currentBalance:UILabel!
    @IBOutlet weak var lbl_creditBalance:UILabel!
    @IBOutlet weak var lbl_dueBalance:UILabel!
    @IBOutlet weak var img_user:UIImageView!

    @IBOutlet weak var txf_hotelCity:UITextField!
    @IBOutlet weak var txf_noOfNights:UITextField!
    @IBOutlet weak var txf_noOfRooms:UITextField!
    @IBOutlet weak var txf_noOfGuests:UITextField!

    @IBOutlet weak var lbl_checkInDate:UILabel!
    @IBOutlet weak var lbl_checkInDay:UILabel!
    @IBOutlet weak var lbl_checkInMonthYear:UILabel!
    @IBOutlet weak var lbl_checkOutDate:UILabel!
    @IBOutlet weak var lbl_checkOutDay:UILabel!
    @IBOutlet weak var lbl_checkOutMonthYear:UILabel!
    
    @IBOutlet weak var view_calendarPop: UIView!
    @IBOutlet weak var lbl_calendarTitle: UILabel!
    @IBOutlet weak var view_JBCalendar: JTHorizontalCalendarView!
    
    @IBOutlet weak var hei_calendarView:NSLayoutConstraint!
    
    //MARK:- Variables
    var calendarManager = JTCalendarManager()
    var controllerObj = SideMenuVC()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
        getBalanceInfoAPICall()
        getProfileDetailsAPICall()
        addWalletView()
        addingCalendarPop()
        showInitialData()
        playVideo()
        
    }
    
    //MARK:- Play Video
    func playVideo(){
        videoContentView.layoutIfNeeded()
      //  videoContentView.playVideoFromUrl(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")
        videoContentView.playVideoFromFilePath(fileName: "app_video1")
        videoContentView.isLoop = true
        videoContentView.play()
    }
    
    
    //MARK:- Add wallet view
    func addWalletView(){
        walletView.frame = self.view.bounds
        walletView.isHidden = true
        self.view.addSubview(walletView)
        walletContentView.alpha = 0.1
    }
    
    // MARK:- Add Calendar Popup
    func addingCalendarPop() {
        
        // calendar pop adding to window...
        for views in (UIApplication.shared.keyWindow?.subviews)! {
            
            if views.tag == 100 || view.tag == 101 || view.tag == 102 || view.tag == 103 {
                views.removeFromSuperview()
            }
        }
        self.view_calendarPop.isHidden = true
        self.view_calendarPop.tag = 100
        self.view_calendarPop.frame = self.view.frame
        UIApplication.shared.keyWindow?.addSubview(self.view_calendarPop)
        
        
//        calendarManager.settings.weekModeEnabled = true
//        hei_calendarView.constant = 180

        calendarManager.delegate = self
        calendarManager.contentView = self.view_JBCalendar
        calendarManager.setDate(Date())
        calendarTitleDisplay(sDate: Date())
        calendarManager.reload()
                
    }
    
    //MARK:- Show initial data
    func showInitialData(){
        
        HotelViewModel.updateInitialData()
        
        lbl_checkInDay.text = DateFormatter.getDateString(formate: "EEEE", date: HotelViewModel.details.checkInDate)
        lbl_checkInMonthYear.text = DateFormatter.getDateString(formate: "MMMM", date: HotelViewModel.details.checkInDate)
        lbl_checkInDate.text = DateFormatter.getDateString(formate: "dd", date: HotelViewModel.details.checkInDate)
        
        lbl_checkOutDay.text = DateFormatter.getDateString(formate: "EEEE", date: HotelViewModel.details.checkOutDate)
        lbl_checkOutMonthYear.text = DateFormatter.getDateString(formate: "MMMM", date: HotelViewModel.details.checkOutDate)
        lbl_checkOutDate.text = DateFormatter.getDateString(formate: "dd", date: HotelViewModel.details.checkOutDate)
        
        txf_noOfNights.text = "1"
        txf_noOfGuests.text = "1"
        txf_noOfRooms.text = "1"

    }
    //MARK:- show balance details
    func showBalanceDetails(){
        lbl_currentBalance.text = String(format:"%@ %@",kCurrency,HomeViewModel.balance ?? "")
        lbl_creditBalance.text = String(format:"%@ %.@",kCurrency,HomeViewModel.credit_limit ?? "")
        lbl_dueBalance.text = String(format:"%@ %.2f",kCurrency,HomeViewModel.due_amount ?? "")
    }
    
    //MARK:- Show Profile data
    func showProfileData(){
        
        if let user_image = UserViewModel.userData?.image,user_image != ""{
            let image_url = "\(Server.image_url)\(user_image)"
            img_user.sd_setShowActivityIndicatorView(true)
            img_user.sd_setIndicatorStyle(.gray)
            img_user.sd_setImage(with: URL.init(string: image_url)) { (image, error, cache, urls) in
                self.img_user.image = (error != nil) ? userPlaceHolder : image
            }
        }
        
    }
    
    //MARK:- Show Hotel selected date
    func showHotelSelectedDate(){
        
        if HotelViewModel.details.checkIn_date.count != 0{
            
            lbl_checkInDay.text = DateFormatter.getDateString(formate: "EEEE", date: HotelViewModel.details.checkInDate)
            
            lbl_checkInMonthYear.text = DateFormatter.getDateString(formate: "MMMM", date: HotelViewModel.details.checkInDate)
            
            lbl_checkInDate.text = DateFormatter.getDateString(formate: "dd", date: HotelViewModel.details.checkInDate)
            
        }
        if HotelViewModel.details.checkOut_date.count != 0{
            
            lbl_checkOutDay.text = DateFormatter.getDateString(formate: "EEEE", date: HotelViewModel.details.checkOutDate)
            
            lbl_checkOutMonthYear.text = DateFormatter.getDateString(formate: "MMMM", date: HotelViewModel.details.checkOutDate)
            
            lbl_checkOutDate.text = DateFormatter.getDateString(formate: "dd", date: HotelViewModel.details.checkOutDate)
            
            txf_noOfNights.text = "\(HotelViewModel.details.noOf_nights)"
            
        }
        
    }

}

extension HomeVC{
    
    //MARK:- IBActions
    
    @IBAction func menuClicked(_ sender:UIButton){
        
        NotificationCenter.default.post(name: kToggleSideMenu, object: nil)
        
    }
    
    @IBAction func walletClicked(_ sender:UIButton){
    
        walletView.isHidden = false

        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.walletContentView.alpha = 1.0

        }, completion:nil)
        
    }
    
    @IBAction func profileClicked(_ sender:UIButton){
        
        let vc = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func closeButtonClicked(_ sender:UIButton){
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.walletContentView.alpha = 0.5
            self.walletView.isHidden = true

        }, completion:nil)
        
    }
    @IBAction func searchHotelTextFieldClicked(_ sender:UIButton){
      
        let searchObj = MainStoryboard.instantiateViewController(withIdentifier: "HotelSearchCititesVC") as! HotelSearchCititesVC
        searchObj.delegate = self
        self.navigationController?.pushViewController(searchObj, animated: true)
        
    }
    
    @IBAction func checkInCheckOutClicked(_ sender:UIButton){
        HotelViewModel.checkInOrOutDate = sender.tag
        
        self.view_calendarPop.isHidden = false
        UIApplication.shared.keyWindow?.bringSubviewToFront(self.view_calendarPop)
    }
    
    @IBAction func roomsAndGuestsClicked(_ sender:UIButton){
        
        let vc = MainStoryboard.instantiateViewController(withIdentifier: "SelectGuestsRoomsVC") as! SelectGuestsRoomsVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func calendarDateSetClicked(_ sender: UIButton) {

        let message = HotelViewModel.updateSelectedDate()
        if message.count != 0{
            appDelegate.window?.makeToast(message: message)
            return
        }
        
        view_calendarPop.isHidden = true
        self.showHotelSelectedDate()
        
    }
    
    @IBAction func searchClicked(_ sender:UIButton){
     
        if txf_hotelCity.text?.count == 0{
            appDelegate.window?.makeToast(message: "Please select City")
            return
        }
        
        self.getHotelsListAPICall()
        
    }
    
}

extension HomeVC:searchHotelCitiesDelegate,GuestRoomsDelegate{
    
    // MARK:- Hotel Search Cities Delegate
    func searchCity_info(cityInfo: [String: String]){
        HotelViewModel.selectedCity_info(cityInfo: cityInfo)
        txf_hotelCity.text = HotelViewModel.details.cityName
    }
    
    func guestRoomsSelected() {
        
        txf_noOfRooms.text = "\(HotelSearchViewModel.guestRoomsList.count)"
        txf_noOfGuests.text = HotelViewModel.getTotalGuests()
    }
    
}

extension HomeVC: JTCalendarDelegate {
    
    ////MARK:- JTCalendarDelegate
    
    func calendar(_ calendar: JTCalendarManager!, prepareDayView dayView: (UIView & JTCalendarDay)!) {
        
        // JTCalenarDayView type casting...
        let loDayView = dayView as! JTCalendarDayView
        if calendarManager.dateHelper.date(Date(), isTheSameDayThan: loDayView.date) {

            // Today...
            loDayView.circleView.isHidden = false
            loDayView.circleView.backgroundColor = UIColor.red //UIColor.init(named: "#E3E3E3")
            loDayView.dotView.backgroundColor = UIColor.white
            loDayView.textLabel.textColor = UIColor.white
        }
        else if calendarManager.dateHelper.date(HomeViewModel.calSelectedDate, isTheSameDayThan: loDayView.date) {
            
            // Selected date...
            loDayView.circleView.isHidden = false
            loDayView.circleView.backgroundColor = AppColors.green //UIColor.init(named: "#2097D9")
            loDayView.dotView.backgroundColor = UIColor.white
            loDayView.textLabel.textColor = UIColor.white
        }
        else if !calendarManager.dateHelper.date(self.view_JBCalendar.date, isTheSameMonthThan: loDayView.date) {
            
            // Other month
            loDayView.circleView.isHidden = true
            loDayView.dotView.backgroundColor = UIColor.red
            loDayView.textLabel.textColor = UIColor.lightGray
        }
        else if calendarManager.dateHelper.date(Date(), isEqualOrAfter: loDayView.date) {
            
            // same month passed dates...
            loDayView.circleView.isHidden = true
            loDayView.dotView.backgroundColor = UIColor.red
            loDayView.textLabel.textColor = UIColor.lightGray
        }
        else {
            
            // Another day of the current month
            loDayView.circleView.isHidden = true
            loDayView.dotView.backgroundColor = UIColor.red
            loDayView.textLabel.textColor = UIColor.black
        }
                
    }
    
    func calendar(_ calendar: JTCalendarManager!, didTouchDayView dayView: (UIView & JTCalendarDay)!) {
        
        // JTCalenarDayView type casting...
        let loDayView = dayView as! JTCalendarDayView
        
        // today date getting with formate...
        let todayDateStr = DateFormatter.getDateString(formate: "dd-MM-yyyy", date: Date())
        let todayDate = DateFormatter.getDate(formate: "dd-MM-yyyy", date: todayDateStr)
        
        // select date gettign with formate...
        let dateSelectStr = DateFormatter.getDateString(formate: "dd-MM-yyyy", date: loDayView.date)
        let selectDate = DateFormatter.getDate(formate: "dd-MM-yyyy", date: dateSelectStr)
        
        
        // user selected past date...
        if todayDate.compare(selectDate) == .orderedDescending {
            appDelegate.window?.makeToast(message: "Date already past")
        }
        else {
            
            // choose selection date....
            HomeViewModel.calSelectedDate = loDayView.date
            
            // Animation for the circleView
            loDayView.circleView.transform = CGAffineTransform.init(scaleX: 0.1, y: 0.1)
            UIView.transition(with: dayView, duration: 0.3, options: [], animations: {
                loDayView.circleView.transform = .identity
                self.calendarManager.reload()
            })
            
            // Don't change page in week mode because block the selection of days in first and last weeks of the month...
            if calendarManager.settings.weekModeEnabled {
                return
            }
            
            // Load the previous or next page if touch a day from another month...
            if !calendarManager.dateHelper.date(self.view_JBCalendar.date, isTheSameMonthThan: loDayView.date) {
                
                if self.view_JBCalendar.date.compare(loDayView.date) == .orderedAscending {
                    self.view_JBCalendar.loadNextPageWithAnimation()
                } else {
                    self.view_JBCalendar.loadPreviousPageWithAnimation()
                }
            }
        }
    }
    
    func calendar(_ calendar: JTCalendarManager!, canDisplayPageWith date: Date!) -> Bool {
        
        // Min date will be 2 month before today
        let minCalDate = calendarManager.dateHelper.add(to: Date(), months: 0)
        
        // Max date will be 2 month after today
        let maxCalDate = calendarManager.dateHelper.add(to: Date(), months: 24)
        
        return self.calendarManager.dateHelper.date(date, isEqualOrAfter: minCalDate, andEqualOrBefore: maxCalDate)
    }
    
    func calendarDidLoadNextPage(_ calendar: JTCalendarManager!) {
        // next month...
        calendarTitleDisplay(sDate: calendar.date())
    }
    
    func calendarDidLoadPreviousPage(_ calendar: JTCalendarManager!) {
        // previous month...
        calendarTitleDisplay(sDate: calendar.date())
    }
    
    func calendarTitleDisplay(sDate: Date) {
        // calender month setup...
        lbl_calendarTitle.text = DateFormatter.getDateString(formate: "MMM yyyy", date: sDate)
    }
    
}

extension HomeVC{
    
    //MARK:- API'S
    func getBalanceInfoAPICall(){
        let params = ["user_id":UserViewModel.userData?.user_id ?? ""]
        
        HomeViewModel.getBalanceAPICall(params: params, handler: {
            self.showBalanceDetails()
        })
    }
    
    func getProfileDetailsAPICall(){
        var params = [String:String]()
        params["user_id"] = UserViewModel.userData?.user_id ?? ""
        UserViewModel.getProfileDetails(params: params, handler: {
           self.showProfileData()
        })
    }
    
    func getHotelsListAPICall() {
        
        HotelSearchViewModel.clearSearchData()
        
        HotelSearchViewModel.getHotelSearchList(handler: {
            
            if HotelSearchViewModel.searchResults.count != 0{
                let vc = MainStoryboard.instantiateViewController(withIdentifier: "HotelListingVC") as! HotelListingVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
}
