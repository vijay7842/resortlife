//
//  HotelCitiesModel.swift
//  ResortLife
//
//  Created by Apple retina on 18/05/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HotelCitiesModel {
    
    // MARK:- Variables
    static var hotelCitiesArray: [[String: String]] = []
    static var countryList: [CountryModel] = []

    // MARK:- API's
    
    static func getHotelCitiesList() {
        
        if self.hotelCitiesArray.count == 0 {
            
            var hotelList: [[String: String]] = []
            let csvFilePath = Bundle.main.path(forResource: "api_hotels_city", ofType: "csv")
            do {
                
                let csvFileDataStr = try String.init(contentsOfFile: csvFilePath!, encoding: .ascii)
                let citysArray = csvFileDataStr.components(separatedBy: "\n")
                print("cities count before: \(citysArray.count)")
                
                for csvString in citysArray {
                    
                    let finalString = csvString.replacingOccurrences(of: "\"", with: "")
                    let citysFinalArray = finalString.components(separatedBy: ",")
                    
                    if citysFinalArray.count >= 4 {
                        
                        let cityName = "\(citysFinalArray[1]) (\(citysFinalArray[2]))"
                        let hCityDict:[String: String] = ["city_id": citysFinalArray[0],
                                                          "city_name": cityName,
                                                          "country": citysFinalArray[3]]
                        hotelList.append(hCityDict)
                    }
                    else {
                        print("Count over lap : \(citysFinalArray)")
                    }
                }
                print("hotels count after: \(hotelList.count)")
                self.hotelCitiesArray = hotelList
            }
            catch {
                print("File Read Error for file \(String(describing: csvFilePath))")
            }
        }
    }
    
    static func getCountriesList() {
        
        if self.countryList.count == 0 {
            
            var countryList: [CountryModel] = []
            let csvFilePath = Bundle.main.path(forResource: "api_country_list", ofType: "csv")
            do {
                
                let csvFileDataStr = try String.init(contentsOfFile: csvFilePath!, encoding: .ascii)
                let citysArray = csvFileDataStr.components(separatedBy: "\n")
                print("countries count before: \(citysArray.count)")
                
                for csvString in citysArray {
                    
                    let finalString = csvString.replacingOccurrences(of: "\"", with: "")
                    let countriesFinalArray = finalString.components(separatedBy: ",")
                    
                    if countriesFinalArray.count >= 4 {
                        
                        var model = CountryModel()
                        model.country_code = countriesFinalArray[0]
                        model.country_name = countriesFinalArray[2]
                        model.dial_code = countriesFinalArray[3]
                        model.country_id = countriesFinalArray[4]
                      
                        countryList.append(model)
                    }
                    else {
                        print("Count over lap : \(countriesFinalArray)")
                    }
                }
                print("countries count after: \(countryList.count)")
                self.countryList = countryList
            }
            catch {
                print("File Read Error for file \(String(describing: csvFilePath))")
            }
        }
    }
    
}

struct CountryModel{
    
    var country_code = ""
    var country_name = ""
    var dial_code = ""
    var country_id = ""
    
}
