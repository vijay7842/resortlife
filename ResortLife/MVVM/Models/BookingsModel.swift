//
//  BookingsModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 26/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

//MARK:- Hotel

struct HotelBookingsModel {
    
    // elements...
    var customers:[CustomersModel]?

    var adult_count = ""
    var app_reference = ""
    var booked_date = ""
    var booking_id = ""
    var booking_reference = ""
    var booking_source = ""
    var child_count = ""
    var currency = ""
    var currency_conversion_rate = ""
    var domain_origin = ""
    var email = ""
    var hotel_check_in = ""
    var hotel_check_out = ""
    var hotel_image = ""
    var hotel_location = ""
    var hotel_name = ""
    var lead_pax_email = ""
    var lead_pax_name = ""
    var lead_pax_phone_number = ""
    var phone_number = ""
    var star_rating  = ""
    var status = ""
    var total_nights = ""
    var total_rooms = ""
    var transfer_total = ""
    
    var grand_total:Float?
    var tax_total:Float?
    
    init(details: [String: Any]) {
        
        if let array = details["customer_details"] as? [[String:Any]], array.count != 0{
            customers = CustomersModel.createModels(array: array)
        }
        
        adult_count = "\(details["adult_count"] ?? "")"
        app_reference = "\(details["app_reference"] ?? "")"
        booked_date = "\(details["booked_date"] ?? "")"
        booking_id = "\(details["booking_id"] ?? "")"
        booking_reference = "\(details["booking_reference"] ?? "")"
        booking_source = "\(details["booking_source"] ?? "")"
        child_count = "\(details["child_count"] ?? "")"
        currency = "\(details["currency"] ?? "")"
        currency_conversion_rate = "\(details["currency_conversion_rate"] ?? "")"
        domain_origin = "\(details["domain_origin"] ?? "")"
        email = "\(details["email"] ?? "")"
        hotel_check_in = "\(details["hotel_check_in"] ?? "")"
        hotel_check_out = "\(details["hotel_check_out"] ?? "")"
        hotel_image = "\(details["hotel_image"] ?? "")"
        hotel_image = hotel_image.replacingOccurrences(of: " ", with: "%20")
        hotel_location  = "\(details["hotel_location"] ?? "")"
        hotel_name = "\(details["hotel_name"] ?? "")"
        lead_pax_email = "\(details["lead_pax_email"] ?? "")"
        lead_pax_name = "\(details["lead_pax_name"] ?? "")"
        lead_pax_phone_number = "\(details["lead_pax_phone_number"] ?? "")"
        phone_number = "\(details["phone_number"] ?? "")"
        star_rating = "\(details["star_rating"] ?? "")"
        status = "\(details["status"] ?? "")"
        total_nights = "\(details["total_nights"] ?? "")"
        total_rooms = "\(details["total_rooms"] ?? "")"
        transfer_total = "\(details["transfer_total"] ?? "")"
        
        grand_total = Float(String.init(describing: details["grand_total"] ?? "")) ?? 0
        tax_total = Float(String.init(describing: details["tax_total"] ?? "")) ?? 0
        
    }
    
    //MARK:- Create Models
    
    static func createModels(array:[[String:Any]]) -> [HotelBookingsModel]
    {
        var models:[HotelBookingsModel] = []
        for item in array{
            models.append(HotelBookingsModel(details: item))
        }
        return models
    }
    
}

struct CustomersModel{
   
    var date_of_birth = ""
    var email = ""
    var first_name = ""
    var last_name = ""
    var middle_name = ""
    var pax_type = ""
    var phone = ""
    var title = ""
   
    init(details: [String: Any]) {
        date_of_birth = "\(details["date_of_birth"] ?? "")"
        email = "\(details["email"] ?? "")"
        first_name = "\(details["first_name"] ?? "")"
        last_name = "\(details["last_name"] ?? "")"
        middle_name = "\(details["middle_name"] ?? "")"
        pax_type = "\(details["pax_type"] ?? "")"
        phone = "\(details["phone"] ?? "")"
        title = "\(details["title"] ?? "")"
    }
    
    //MARK:- Create Models
    
    static func createModels(array:[[String:Any]]) -> [CustomersModel]
    {
        var models:[CustomersModel] = []
        for item in array{
            models.append(CustomersModel(details: item))
        }
        return models
    }
    
}
