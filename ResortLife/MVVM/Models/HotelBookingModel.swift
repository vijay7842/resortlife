//
//  HotelBookingModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 11/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HotelPrebookingModel {
    
    var convenience_fees:Float = 0
    var to_currency = ""
    var block_room_id = ""
    var ResultIndex = ""
    var TraceId = ""
    var default_currency = ""
    var pre_booking_token_key = ""
    var pre_booking_params_token = ""
    var pre_booking_params_token_key = ""
    var payment_method = ""
    
    var total_price:Float = 0
    var tax_service_sum:Float = 0
    
    init(details: [String: Any]) {
        
        convenience_fees = Float(String.init(describing: details["convenience_fees"] ?? "")) ?? 0
        
        if let array = details["active_payment_options"] as? [Any],array.count != 0{
            payment_method = "\(array[0])"
        }
        
        if let currencyObj = details["currency_obj"] as? [String:Any]{
            to_currency = "\(currencyObj["to_currency"] ?? "")"
        }
        
        if let preBooking = details["pre_booking_params"] as? [String:Any]{
            block_room_id = "\(preBooking["BlockRoomId"] ?? "")"
            ResultIndex = "\(preBooking["ResultIndex"] ?? "")"
            TraceId = "\(preBooking["TraceId"] ?? "")"
            default_currency = "\(preBooking["default_currency"] ?? "")"
            
            if let array = preBooking["token_key"] as? [Any],array.count != 0{
                pre_booking_token_key = "\(array[0])"
            }
            
        }
        
        pre_booking_params_token = "\(details["pre_booking_params_token"] ?? "")"
        pre_booking_params_token_key = "\(details["pre_booking_params_token_key"] ?? "")"
        tax_service_sum = Float(String.init(describing: details["tax_service_sum"] ?? "")) ?? 0
        total_price = Float(String.init(describing: details["total_price"] ?? "")) ?? 0
        
    }
    
}
