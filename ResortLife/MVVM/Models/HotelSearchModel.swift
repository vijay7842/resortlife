//
//  HotelSearchModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 25/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation


struct HotelSearchResultModel{
    
    var hotel_amenitites = [String]()
    var hotel_code:String = ""
    var hotel_original_code:String = ""
    var hotel_name:String = ""
    var address:String = ""
    var contact_number:String = ""
    var location:String = ""
    var hotel_picture:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var hotel_promotion:String = ""

    var currency_code:String = ""
    var currency_symbol:String = ""
    var room_price: Float = 0
    var room_price_wGST:String = ""
    var tax:String = ""

    var star_rating:String = ""
    var rating: Int = 0

    var trip_reviews:String = ""
    var trip_review_url:String = ""
    var web_reviews_url:String = ""

    var result_token:String = ""
    var search_hash = ""
    
    init(data:[String:Any]) {
        
        if let array:[String] = data["HotelAmenities"] as? [String]{
            hotel_amenitites = array
        }
        hotel_code = "\(data["HotelCode"] ?? "")"
        hotel_original_code = "\(data["HoteOrginalHotelCodelCode"] ?? "")"
        hotel_name = "\(data["HotelName"] ?? "")"
        address = "\(data["HotelAddress"] ?? "")"
        contact_number = "\(data["HotelContactNo"] ?? "")"
        location = "\(data["HotelLocation"] ?? "")"
        hotel_picture = "\(data["HotelPicture"] ?? "")"
        hotel_picture = hotel_picture.replacingOccurrences(of: " ", with: "%20")
        latitude = "\(data["Latitude"] ?? "")"
        longitude = "\(data["Longitude"] ?? "")"
        hotel_promotion = "\(data["HotelPromotion"] ?? "")"

        if let priceDict = data["Price"] as? [String:Any]{
            currency_code = "\(priceDict["CurrencyCode"] ?? "")"
            currency_symbol =  "\(priceDict["CurrencySymbol"] ?? "")"
            room_price_wGST = "\(priceDict["RoomPriceWoGST"] ?? "")"
            tax = "\(priceDict["Tax"] ?? "")"
            room_price = Float(String.init(describing: priceDict["RoomPrice"] ?? "")) ?? 0
        }
        
        star_rating = "\(data["StarRating"] ?? "")"
        rating = Int(String.init(describing: data["StarRating"] ?? "")) ?? 0

        trip_review_url = "\(data["trip_review_url"] ?? "")"
        trip_reviews = "\(data["trip_reviews"] ?? "")"
        web_reviews_url = "\(data["web_reviews_url"] ?? "")"

        result_token = "\(data["ResultToken"] ?? "")"
        search_hash = "\(data["search_hash"] ?? "")"

    }
    
    //MARK:- Create Models
    
    static func createModels(array:[[String:Any]]) -> [HotelSearchResultModel]
    {
        var models:[HotelSearchResultModel] = []
        for item in array{
            models.append(HotelSearchResultModel(data: item))
        }
        return models
    }
    
}


