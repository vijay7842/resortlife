//
//  UserModel.swift
//
//  Created by Vijay G on 03/03/20.
//  Copyright © 2019 Vijay G. All rights reserved.
//

import Foundation

//MARK:- Format model
struct ResponseCheckModel {
    
    var status:Bool = false
    var message:String = ""
    
    init(dict:[String:Any]) {
        status = dict["status"] as? Bool ?? false
        message = "\(dict["message"] ?? "")"
    }
    
}

// MARK:- UserModel

struct UserModel:Codable {
    
    // variables
    var address : String?
    var city: String?
    var pin_code: String?
    var country_code: String?
    var country_code_value: String?
    var country_name:String?
    var image: String?
    var language_preference: String?
    var email: String?
    var first_name: String?
    var last_name: String?
    var user_id:String?
    var phone:String?
    var user_name:String?
    var user_type:String?
    var user_profile_name:String?
    var status:String?
    var date_of_birth:String?
    var title:String?
    var agency_name:String?
    var office_phone:String?
    var terms_conditions:String?
    var pan_number:String?
    var balance: Float = 0
    var credit_limit: Float = 0
    var due_amount: Float = 0

    init(details: [String: Any]) {
        
        pan_number = "\(details["pan_number"] ?? "")"
        status = "\(details["status"] ?? "")"
        date_of_birth = "\(details["date_of_birth"] ?? "")"
        title = "\(details["title"] ?? "")"
        agency_name = "\(details["agency_name"] ?? "")"
        office_phone = "\(details["office_phone"] ?? "")"
        terms_conditions = "\(details["terms_conditions"] ?? "")"
        balance = Float(String.init(describing: details["balance"] ?? "")) ?? 0
        credit_limit = Float(String.init(describing: details["credit_limit"] ?? "")) ?? 0
        due_amount = Float(String.init(describing: details["due_amount"] ?? "")) ?? 0
        address = "\(details["address"] ?? "")"
        city = "\(details["city"] ?? "")"
        pin_code = "\(details["pin_code"] ?? "")"
        country_name = "\(details["country_name"] ?? "")"
        country_code = "\(details["country_code"] ?? "")"
        country_code_value = "\(details["country_code_value"] ?? "")"
        image = "\(details["image"] ?? "")"
        language_preference = "\(details["language_preference"] ?? "")"
        email = "\(details["email"] ?? "")"
        first_name = "\(details["first_name"] ?? "")"
        last_name = "\(details["last_name"] ?? "")"
        user_id = "\(details["user_id"] ?? "")"
        phone = "\(details["phone"] ?? "")"
        user_name = "\(details["user_name"] ?? "")"
        user_profile_name = "\(details["user_profile_name"] ?? "")"
        
        let user_typee = "\(details["user_type"] ?? "")"
        if user_typee == "3"{
            self.user_type = "b2b"
        }else if user_typee == "4"{
            self.user_type = "b2c"
        }
    }
    
}


struct PassengersModel {
    
    // elements...
    var passengerType: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var title: String = ""
    var dateOfBirth: String = ""
    var passportNumber: String = ""
    var passportCountryName: String = ""
    var passportCountryCode: String = ""
    var passportExpDate: String = ""
    
    var displayTitle: String = ""
    
}


struct NotificationModel {
    
    // elements...
    var title: String =  ""
    var id: String = ""
    var description: String = ""
    var added_on: String = ""
    var type: String = ""
    var status: String = ""
    
    init(details: [String: Any]) {
        
        self.title = "\(details["title"] ?? "")"
        self.id = "\(details["id"] ?? "")"
        self.description = "\(details["description"] ?? "")"
        self.added_on = "\(details["added_on"] ?? "")"
        self.type = "\(details["type"] ?? "")"
        self.status = "\(details["status"] ?? "")"
        
    }
    
    //MARK:- Create Models
    
    static func createModels(array:[[String:Any]]) -> [NotificationModel]{
        var models = [NotificationModel]()
        
        for item in array{
            models.append(NotificationModel.init(details: item))
        }
        return models
    }
    
}

