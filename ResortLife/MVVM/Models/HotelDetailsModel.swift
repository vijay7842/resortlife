//
//  HotelDetailsModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 08/04/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation

struct HotelDetailsModel{
    
    var images = [String]()
    var facilities = [String]()
    var amenities = [String]()
    
    var address:String = ""
    var country_name:String = ""
    var hotel_code:String = ""
    var hotel_name:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var star_rating:String = ""
    var checkIn:String = ""
    var checkOut:String = ""
    var property_type:String = ""
    var property_type_id:String = ""

    var rating: Int = 0
    
    var hotel_policy:String = ""
    var description:String = ""
    var complements:String = ""
    var exclusion:String = ""
    var cancellation_policy:String = ""

    init(data:[String:Any]) {
        
        if let array:[String] = data["Images"] as? [String]{
            images = array
        }
        if let array:[String] = data["Amenities"] as? [String]{
            amenities = array
        }
        if let array:[String] = data["HotelFacilities"] as? [String]{
            facilities = array
        }
        
        address = "\(data["Address"] ?? "")"
        country_name = "\(data["CountryName"] ?? "")"
        hotel_code = "\(data["HotelCode"] ?? "")"
        hotel_name = "\(data["HotelName"] ?? "")"
        latitude = "\(data["Latitude"] ?? "")"
        longitude = "\(data["Longitude"] ?? "")"
        checkIn = "\(data["checkin"] ?? "")"
        checkOut = "\(data["checkout"] ?? "")"
        property_type = "\(data["PropertyType"] ?? "")"
        property_type_id = "\(data["PropertyTypeId"] ?? "")"
        star_rating = "\(data["StarRating"] ?? "")"
        rating = Int(String.init(describing: data["StarRating"] ?? "")) ?? 0
        
        hotel_policy = "\(data["HotelPolicy"] ?? "")"
        description = "\(data["Description"] ?? "")"
        complements = "\(data["Complements"] ?? "")"
        exclusion = "\(data["Exclusion"] ?? "")"
        cancellation_policy = "\(data["cancellation_policy"] ?? "")"

    }
}

struct HotelTransfersModel{
    
    var from_loc = ""
    var hotel_id:String = ""
    var id:String = ""
    var market:String = ""
    var route:String = ""
    var supplier_id:String = ""
    var to_loc:String = ""
    var token:String = ""
    var token_key:String = ""
    var trans_adult:String = ""
    var trans_child:String = ""
    var trans_infant:String = ""
    var transfer:String = ""
    var transfer_name:String = ""
    var transfer_type:String = ""

    init(dict:[String:Any]) {
        
        from_loc = "\(dict["from_loc"] ?? "")"
        hotel_id = "\(dict["hotel_id"] ?? "")"
        id = "\(dict["id"] ?? "")"
        market = "\(dict["market"] ?? "")"
        route = "\(dict["route"] ?? "")"
        supplier_id = "\(dict["supplier_id"] ?? "")"
        to_loc = "\(dict["to_loc"] ?? "")"
        token = "\(dict["token"] ?? "")"
        token_key = "\(dict["token_key"] ?? "")"
        trans_adult = "\(dict["trans_adult"] ?? "")"
        trans_child = "\(dict["trans_child"] ?? "")"
        trans_infant = "\(dict["trans_infant"] ?? "")"
        transfer = "\(dict["transfer"] ?? "")"
        transfer_name = "\(dict["transfer_name"] ?? "")"
        transfer_type = "\(dict["transfer_type"] ?? "")"

    }
    
    static func createModels(array:[[String:Any]]) -> [HotelTransfersModel]
    {
        var models:[HotelTransfersModel] = []
        for item in array{
            models.append(HotelTransfersModel(dict: item))
        }
        return models
    }
    
}

struct HotelRoomsModel{
    
    var amenities = [String]()
    var datesList = [String]()
    var mealPlans:[MealPlanModel] = []
    
    var roomId:String = ""
    var roomName:String = ""
    var roomPrice:Float = 0
    var last_cancellation_date:String = ""
    var api_search_id:String = ""
    var cancellation_policy:String = ""
    var cancellation_policy_old:String = ""
    var currency:String = ""
    var currency_symbol:String = ""
    var hotel_code:String = ""
    var result_index:String = ""
    var tax:String = ""
    var booking_source:String = ""
    var search_id:String = ""
    var token:String = ""
    var token_key:String = ""
    var accessKey:String = ""

    //Selection
    var selected_check_in = ""
    var selected_check_out = ""
    var selected_meal_plan = ""
    var selected_meal_plan_id = ""
    
    init(dict:[String:Any]) {
        
        if let data = dict["room_data"] as? [String:Any]{
            if let array:[String] = data["Amenities"] as? [String],array.count != 0{
                amenities = array
            }
            if let array:[String] = data["DateList"] as? [String],array.count != 0{
                datesList = array
            }
            
            if let array = data["MealPlan"] as? [[String:Any]],array.count != 0{
                mealPlans = MealPlanModel.createModels(array: array)
            }
            
            roomId = "\(data["RoomId"] ?? "")"
            roomName = "\(data["RoomTypeName"] ?? "")"
            roomPrice = Float(String.init(describing: data["RoomPrice"] ?? "")) ?? 0

            last_cancellation_date = "\(data["LastCancellationDate"] ?? "")"
            api_search_id = "\(data["API_SEARCH_ID"] ?? "")"
            cancellation_policy = "\(data["CancellationPolicy"] ?? "")"
            cancellation_policy_old = "\(data["CancellationPolicy_old"] ?? "")"
            currency = "\(data["Currency"] ?? "")"
            currency_symbol = "\(data["Currency_symbol"] ?? "")"
            hotel_code = "\(data["HotelCode"] ?? "")"
            result_index = "\(data["ResultIndex"] ?? "")"
            tax = "\(data["Tax"] ?? "")"
            booking_source = "\(data["booking_source"] ?? "")"
            search_id = "\(data["search_id"] ?? "")"
            accessKey = "\(data["AccessKey"] ?? "")"

            if let custom = data["custom"] as? [String:Any]{
                token = "\(custom["token"] ?? "")"
                token_key = "\(custom["token_key"] ?? "")"
            }
        }
        
    }
    
    static func createModels(array:[[String:Any]]) -> [HotelRoomsModel]
    {
        var models:[HotelRoomsModel] = []
        for item in array{
            models.append(HotelRoomsModel(dict: item))
        }
        return models
    }
    
}


struct MealPlanModel{
    
    var meal_plan = ""
    var origin:String = ""
    
    init(dict:[String:Any]) {
        
        meal_plan = "\(dict["meal_plan"] ?? "")"
        origin = "\(dict["origin"] ?? "")"
       
    }
    
    static func createModels(array:[[String:Any]]) -> [MealPlanModel]
    {
        var models:[MealPlanModel] = []
        for item in array{
            models.append(MealPlanModel(dict: item))
        }
        return models
    }
    
}

struct RoomPriceModel{
    
    var dicount_code = ""
    var no_of_nights:String = ""
    var room_base_price:Float = 0
    var room_price:Float = 0
    var token:String = ""
    var token_key:String = ""

    
    init(dict:[String:Any]) {
        
        dicount_code = "\(dict["dicount_code"] ?? "")"
        no_of_nights = "\(dict["no_of_nights"] ?? "")"
        room_price = Float(String.init(describing: dict["room_price"] ?? "")) ?? 0
        room_base_price = Float(String.init(describing: dict["room_base_price"] ?? "")) ?? 0
        token = "\(dict["token"] ?? "")"
        token_key = "\(dict["token_key"] ?? "")"

    }
    
}

struct FlightInfoModel {
    
    // elements...
   
    var flight_name = ""
    var flight_number = ""
    var flight_date = ""
    var flight_time = ""
    
}
