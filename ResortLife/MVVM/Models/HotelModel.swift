//
//  HotelModel.swift
//  ElamantTravel
//
//  Created by Apple retina on 19/03/20.
//  Copyright © 2020 ProvabTechnoSoft. All rights reserved.
//

import Foundation


struct HotelModel {
    
    // elements...
    var cityId: String = ""
    var cityName: String = ""
    var cityCode: String = ""
    
    var noOf_nights: Int = 1
    var noOf_guests: String = ""
    var noOf_rooms: String = ""

    var checkIn_date: String = ""
    var checkOut_date: String = ""
    
    var checkInDate = Date()
    var checkOutDate = Date()
    
}

struct GuestsRoomsModel {
    
    // elements...

    var adultsCount: Int = 1
    var childrenCount: Int = 0
    var adultSelIndex: Int = 0
    var childSelIndex: Int = 0
    
    var childsList = [ChildsModel]()
    
}
struct ChildsModel {
    
    // elements...
    var age: String = ""
    var selectedIndex: Int = -1

}
