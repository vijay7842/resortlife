//
//  Extensions.swift
//  BaseProject
//
//  Created by PTBLR-1206 on 13/01/20.
//  Copyright © 2020 PTBLR-1206. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    class func instantiateFromStoryBoard() -> UIViewController?{
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vcName = String(describing: self)
        let vc = storyBoard.instantiateViewController(withIdentifier: vcName)
        return vc
    }
}

extension UIWindow{
    static var main:UIWindow{
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.window!
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        // return UIView()
        return value(forKey: "statusBar") as? UIView
    }
}
extension UINavigationController
{   /**
     - usage: self.navigationController(LoginVC.self)
     */
    
    func popToViewController<T: UIViewController>(controller type: T.Type,animated:Bool) {
        for viewController in self.viewControllers {
            if viewController is T {
                self.popToViewController(viewController, animated: animated)
                return
            }
        }
    }
}

extension Int {
    init(_ range: Range<Int> ) {
        let delta = range.startIndex < 0 ? abs(range.startIndex) : 0
        let min = UInt32(range.startIndex + delta)
        let max = UInt32(range.endIndex   + delta)
        self.init(Int(min + arc4random_uniform(max - min)) - delta)
    }
}

extension Date{
    
    var startOfDay: Date
    {
        return Calendar.current.startOfDay(for: self)
    }
    
    func getDate(dayDifference: Int) -> Date {
        var components = DateComponents()
        components.day = dayDifference
        return Calendar.current.date(byAdding: components, to:startOfDay)!
    }
}
extension DateFormatter {
    
    // convert date string to date...
    class func getDate(formate: String, date: String) -> Date {
        
        // date formatter...
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "en_US")
        formatter.calendar = Calendar(identifier: .gregorian)
        
        // set date formate and send date...
        formatter.dateFormat = formate
        return formatter.date(from: date) ?? Date()
    }
    
    // convert date to date string...
    class func getDateString(formate: String, date: Date) -> String {
        
        // date formatter...
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "en_US")
        formatter.calendar = Calendar(identifier: .gregorian)
        
        // set date formate and send date...
        formatter.dateFormat = formate
        return formatter.string(from: date)
    }
    
    // convert date Format...
    class func changeDateFormatString(fromFormat: String,date:String,toFormat:String) -> String {
        
        // date formatter...
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "en_US")
        formatter.calendar = Calendar(identifier: .gregorian)
        
        formatter.dateFormat = fromFormat
        let date = formatter.date(from: date) ?? Date()
        formatter.dateFormat = toFormat

        return formatter.string(from: date)
    }
    
    // days between dates...
    class func getDaysBetweenTwoDates(startDate: Date, endDate: Date) -> Int {
        
        let timeInterval = endDate.timeIntervalSince(startDate)
        let days_count = Int(timeInterval / (60 * 60 * 24 ))
        
        return days_count
    }
    
    // minutes...
    class func getHoursMinsBetweenDates(fromDate: Date, toDate: Date) -> String {
        
        let difference = Calendar.current.dateComponents([.hour, .minute], from: fromDate, to: toDate)
        let formattedString = String(format: "%02ld H %02ld M", difference.hour!, difference.minute!)
        
        print(formattedString)
        return formattedString
    }
}

extension String {
    
    func convertHtmlToString() -> String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    var htmlToAttributedString: NSAttributedString? {
        
        // getting attributed string...
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    func strikeLineText() -> NSAttributedString? {
        
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    func isValidIntergerSet() -> Bool {
        
        // filtering existed string as interger set or not...
        let cs = CharacterSet.init(charactersIn: "0123456789").inverted
        let filtered: String = self.components(separatedBy: cs).joined(separator: "")
        if self == filtered {
            return true
        }
        return false
    }
    
    func isValidEmailAddress() -> Bool {
        
        // create a regex string...
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        // create predicate with format matching your regex string...
        let predicateTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return predicateTest.evaluate(with: self)
    }
    
    func isValidFloatSet() -> Bool {
        
        // filtering existed string as float set or not...
        let cs = CharacterSet.init(charactersIn: "0123456789.").inverted
        let filtered: String = self.components(separatedBy: cs).joined(separator: "")
        if self == filtered {
            return true
        }
        return false
    }
    
    func isValidFloatSet(mainString: String) -> Bool {
        
        // check if main string already existed "."
        let searchForDecimal = "."
        let range: NSRange? = (mainString as NSString).range(of: searchForDecimal)
        if range?.location != NSNotFound {
            return false
        }
        
        // filtering existed string as float set or not...
        let cs = CharacterSet.init(charactersIn: "0123456789.").inverted
        let filtered: String = self.components(separatedBy: cs).joined(separator: "")
        if self == filtered {
            return true
        }
        return false
    }
    
    func isValidPassword() -> Bool {
        
        // if password is empty...
        if !self.isValidString() {
            return false
        }
        
        // at least one uppercase -> at least one digit -> at least one lowercase -> 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: self)
    }
    
    func isValidString() -> Bool {
        
        // string validation without space...
        let whitespace = CharacterSet.whitespacesAndNewlines
        if self.count == 0 || self.trimmingCharacters(in: whitespace).count == 0 {
            return false
        }
        return true
    }
}

extension NSAttributedString {
    
    class func attributedColorString(color: UIColor, placeString: String) -> NSAttributedString {
        
        // textfiled placeholder color...
        return NSAttributedString.init(string: placeString, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
}

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}

extension UIImage {
    func scaleImage(newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func convertImageToData() -> Data{
       
        return self.pngData()!
    }
    
}

extension UIColor {
    
    class func RGB(_ r: Float, _ g: Float, _ b: Float) -> UIColor {
        return UIColor.init(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: 1.0)
    }
    class func hex(hex: String, alpha: Double = 1.0) -> UIColor{
        
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        
        return UIColor.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(255 * alpha) / 255)
    }
}
