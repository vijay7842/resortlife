//
//  UIView+Extension.swift
//  ETeacherApp
//
//  Created by ptblr-1209 on 11/11/19.
//  Copyright © 2019 PTBLR-1206. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class DesignableView: UIView {
    
    @IBInspectable var circleRadius: Bool = false
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        // dynamic radius...
        if circleRadius == true {
            layer.cornerRadius = frame.size.height / 2
        }
    }
    
    
}

@IBDesignable class DesignableButton: UIButton {
    
    @IBInspectable var circleRadius: Bool = false
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        // dynamic radius...
        if circleRadius == true {
            layer.cornerRadius = frame.size.height / 2
        }
    }
}

@IBDesignable class DesignableLabel: UILabel {
    
    @IBInspectable var circleRadius: Bool = false
   
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        // dynamic radius...
        if circleRadius == true {
            layer.cornerRadius = frame.size.height / 2
        }
    }
    
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    @IBInspectable var masksToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
    
    func OnlyBottomCornerRadius( sender : CGFloat)
    {
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        
        shadowLayer.shadowColor = shadowColor?.cgColor
        shadowLayer.shadowOffset = shadowOffset
        shadowLayer.shadowOpacity = shadowOpacity
        shadowLayer.shadowRadius = shadowRadius
        
        self.layer.insertSublayer(shadowLayer, at: 0)
        
        layer.backgroundColor =  nil
        
    }
    
    func halfCircle() {
        let circlePath = UIBezierPath.init(arcCenter: CGPoint(x:self.bounds.size.width / 2, y: self.bounds.size.height),
                                           radius: self.bounds.size.height, startAngle: .pi, endAngle: 0, clockwise: true)
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        circleShape.shadowPath = circleShape.path
        circleShape.shadowColor = UIColor.lightGray.cgColor
        circleShape.shadowOffset = shadowOffset
        circleShape.shadowOpacity = shadowOpacity
        circleShape.shadowRadius = shadowRadius
        self.layer.mask = circleShape
    }
    
    func OnlyTopCornerRadius( sender : CGFloat)
    {
        
        //                        let rectShape = CAShapeLayer()
        //                        rectShape.bounds = self.frame
        //                        rectShape.position = self.center
        //                        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        //                        self.layer.mask = rectShape
        
        
        
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowOffset = shadowOffset
        shadowLayer.shadowOpacity = shadowOpacity
        shadowLayer.shadowRadius = shadowRadius
        
        self.layer.insertSublayer(shadowLayer, at: 0)
        
        layer.backgroundColor =  nil
        
    }
    
    func OnlyLeftCornerRadius( sender : CGFloat)
    {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft , .topLeft], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        self.layer.mask = rectShape
        
    }
    
    func OnlyRightCornerRadius( sender : CGFloat)
    {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomRight , .topRight], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        self.layer.mask = rectShape
    }
    
    func MultipleColorsAssignToView( color1 : UIColor, color2 : UIColor)
    {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setOnlyTopCornerRadiusWithShadow(_ sender: CGFloat) {
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowOffset = CGSize(width: 0, height: 0.3)
        shadowLayer.shadowOpacity = 0.3
        shadowLayer.shadowRadius = 3
        
        self.layer.insertSublayer(shadowLayer, at: 0)
        
        layer.backgroundColor =  nil
    }
    
    func setOnlyBottomRadiusWithShadow(_ sender: CGFloat) {
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft , .bottomRight], cornerRadii: CGSize(width: sender, height: sender)).cgPath
        shadowLayer.fillColor = backgroundColor?.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        
        shadowLayer.shadowColor = UIColor.lightGray.cgColor
        shadowLayer.shadowOffset = CGSize(width: 0, height: 0.3)
        shadowLayer.shadowOpacity = 0.3
        shadowLayer.shadowRadius = 3
        
        self.layer.insertSublayer(shadowLayer, at: 0)
        
        layer.backgroundColor =  nil
    }
    
    func drawDottedLine(strokeColor:UIColor) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [20, 3] // 20 is the length of dash, 3 is length of the gap.
        shapeLayer.fillColor = nil
        shapeLayer.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(shapeLayer)
        
    }
    
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
}

